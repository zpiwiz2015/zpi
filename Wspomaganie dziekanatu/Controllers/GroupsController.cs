﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wspomaganie_dziekanatu.Helpers;
using Wspomaganie_dziekanatu.Models;
using Wspomaganie_dziekanatu.Models.VM;

namespace Wspomaganie_dziekanatu.Controllers
{
    public partial class GroupsController : Controller
    {
        [Authorize]
        // GET: Groups
        public virtual ActionResult Index()
        {
            var model = new List<ListaGrupVM>();

            using (var db = new ZpiDbCtx())
            {
                var user = Helpers.User.ZalogowanaOsoba(User);
                foreach (var grupa in db.Grupa.Where(e => e.Dydaktyk.OsobaId == user.OsobaId))
                {
                    model.Add(DashboardHelpers.Grupa(grupa));
                }
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            return View(model);
        }

        [Authorize]
        // GET: Groups/{courseId}
        public virtual ActionResult ViewCourse(int courseId)
        {
            using (var db = new ZpiDbCtx())
            {
                var kurs = db.FormaPrzedmiotu.FirstOrDefault(e => e.KartaPrzedmiotuId == courseId);
                var listaGrup = new List<ListaGrupVM>();
                foreach (Grupa g in kurs.Grupa)
                {
                    listaGrup.Add(DashboardHelpers.Grupa(g));
                }

                ViewBag.CourseName = kurs.KartaPrzedmiotu.NazwaPl;

                /* breadcrumbs data */
                var user = Helpers.User.ZalogowanaOsoba(User);
                /* courses list*/
                int dydaktykId = Helpers.User.IdDydaktyka(User);
                ViewBag.Courses = RaportsHelpers.KursyDydaktyka(dydaktykId);

                ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
                return View(listaGrup);
            }
        }

        [Authorize]
        // GET: Groups/{courseId}/{groupId}
        public virtual ActionResult ViewGroup(int courseId, int groupId)
        {
            using (var db = new ZpiDbCtx())
            {
                var group = db.Grupa.First(e => e.GrupaId == groupId);

                /* breadcrumbs data */
                var user = Helpers.User.ZalogowanaOsoba(User);
                /* courses list*/
                int dydaktykId = Helpers.User.IdDydaktyka(User);
                ViewBag.Courses = RaportsHelpers.KursyDydaktyka(dydaktykId);
                /* groups list*/
                var listaGrup = new List<ListaGrupVM>();
                foreach (Grupa g in db.Grupa.Where(e => e.Dydaktyk.OsobaId == user.OsobaId && e.FormaPrzedmiotu.KartaPrzedmiotuId == courseId))
                {
                    listaGrup.Add(DashboardHelpers.Grupa(g));
                }
                ViewBag.Groups = listaGrup;

                ViewBag.CourseName = db.KartaPrzedmiotu.First(e => e.KartaPrzedmiotuId == courseId).NazwaPl;
                var terms = db.TerminZajec.Where(e => e.GrupaId == groupId);
                ViewBag.GroupName = CultureInfo.CurrentCulture.DateTimeFormat.DayNames[(int)terms.First().Data.DayOfWeek] + ", " + DashboardHelpers.GodzinyZajec(terms.First().GodzinaOd, terms.First().GodzinaDo);

                ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
                return View(group);
            }

        }
    }
}