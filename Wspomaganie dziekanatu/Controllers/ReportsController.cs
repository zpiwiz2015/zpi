﻿using Rotativa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wspomaganie_dziekanatu.Helpers;
using Wspomaganie_dziekanatu.Models;
using Wspomaganie_dziekanatu.Models.VM;

namespace Wspomaganie_dziekanatu.Controllers
{
    public partial class ReportsController : Controller
    {
        [Authorize]
        // GET: Reports
        public virtual ActionResult Index(String type, String status)
        {
            var model = new List<RaportyVM>();

            using (var db = new ZpiDbCtx())
            {
                var user = Helpers.User.ZalogowanaOsoba(User);
                foreach (var grupa in db.Grupa.Where(e => e.Dydaktyk.OsobaId == user.OsobaId))
                {
                    if (!model.Any(e => e.KursId == grupa.FormaPrzedmiotu.KartaPrzedmiotuId))
                    {
                        model.Add(RaportsHelpers.WidokRaportow(grupa.FormaPrzedmiotu.KartaPrzedmiotuId, grupa.DydaktykId));
                    }
                }
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            return View(model);
        }

        [Authorize]
        // GET: Reports/{courseId}
        public virtual ActionResult ViewCourse(int courseId)
        {
            var model = RaportsHelpers.WidokRaportow(courseId, Helpers.User.IdDydaktyka(User));

            /* breadcrumbs data */
            var user = Helpers.User.ZalogowanaOsoba(User);
            /* courses list*/
            int dydaktykId = Helpers.User.IdDydaktyka(User);
            ViewBag.Courses = RaportsHelpers.KursyDydaktyka(dydaktykId);

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            return View(model);
        }

        [Authorize]
        [HttpGet]
        // GET: Reports/{courseId}/frp/{actionType}/{formId}
        public virtual ActionResult CreateFRPReport(String actionType, int courseId, int formId)
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();

            /* breadcrumbs data */
            var user = Helpers.User.ZalogowanaOsoba(User);
            /* courses list*/
            int dydaktykId = Helpers.User.IdDydaktyka(User);
            ViewBag.Courses = RaportsHelpers.KursyDydaktyka(dydaktykId);

            var model = RaportsHelpers.PobierzRaportyFRP(dydaktykId, courseId, formId);
            if (actionType == "pdf")
            {
                String viewName = model.Zaliczenie ? "_FRPNoExamBody" : "_FRPExamBody";
                return new PartialViewAsPdf(viewName, model)
                {
                    FileName = "RaportFRP.pdf"
                };
            }
            else
            {
                return View("CreateFRPReport", model);
            }

        }

        [Authorize]
        [HttpPost]
        // POST: Reports/{courseId}/frp/{actionType}/{formId}
        public virtual ActionResult CreateFRPReport(frpVM model)
        {
            if (ModelState.IsValid)
            {
                RaportsHelpers.EdytujFRP(model);
                AktywnoscHelpers.addActivity(new AktywnoscVM()
                {
                    UzytkownikId = Helpers.User.ZalogowanyId(User),
                    URL = "/Reports/" + RouteData.Values["courseId"],
                    Data = DateTime.Now,
                    Opis = "Edytowałeś raport FRP."
                });
                return Content("true", "text/plain");
            }
            else
            {
                String message = "Wystąpił błąd.\n";
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        message += error.ErrorMessage.ToString() + "\n";
                    }
                }
                return Content(message, "text/plain");
            }
        }

        [Authorize]
        [HttpGet]
        // GET: Reports/{courseId}/srop/{actionType}
        public virtual ActionResult CreateSROPReport(String actionType, int courseId)
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();

            /* breadcrumbs data */
            var user = Helpers.User.ZalogowanaOsoba(User);
            /* courses list*/
            int dydaktykId = Helpers.User.IdDydaktyka(User);
            ViewBag.Courses = RaportsHelpers.KursyDydaktyka(dydaktykId);

            var model = RaportsHelpers.PobierzRaportSROP(dydaktykId, courseId);

            if (actionType == "pdf")
            {
                return new PartialViewAsPdf("_SROPBody", model) {
                    FileName = "RaportSROP.pdf"
                };
            }
            else {
                return View("CreateSROPReport", model);
            }
        }

        [Authorize]
        [HttpPost]
        // POST: Reports/{courseId}/srop/{actionType}
        public virtual ActionResult CreateSROPReport(SROP model)
        {
            if (ModelState.IsValid)
            {
                RaportsHelpers.EdytujSROP(model);
                AktywnoscHelpers.addActivity(new AktywnoscVM()
                {
                    UzytkownikId = Helpers.User.ZalogowanyId(User),
                    URL = "/Reports/" + RouteData.Values["courseId"],
                    Data = DateTime.Now,
                    Opis = "Edytowałeś raport SROP."
                });
                return Content("true", "text/plain");
            }
            else
            {
                String message = "Wystąpił błąd.\n";
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        message += error.ErrorMessage.ToString() + "\n";
                    }
                }
                return Content(message, "text/plain");
            }
        }

        /*
        public virtual ActionResult PrzekazSROP(int kursId)
        {
            int dydaktykId = Helpers.User.IdDydaktyka(User);
            RaportsHelpers.PrzekazSROP(dydaktykId, kursId);
            return RedirectToAction(MVC.Reports.Index("", ""));
        }

        public virtual ActionResult PrzekazFRP(int kursId)
        {
            int dydaktykId = Helpers.User.IdDydaktyka(User);
            RaportsHelpers.PrzekazFRP(dydaktykId, kursId);
            return RedirectToAction(MVC.Reports.Index("", ""));
        }
        */
    }
}