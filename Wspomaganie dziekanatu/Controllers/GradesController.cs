﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wspomaganie_dziekanatu.Helpers;
using Wspomaganie_dziekanatu.Helpers.Enums;
using Wspomaganie_dziekanatu.Models;
using Wspomaganie_dziekanatu.Models.DM;
using Wspomaganie_dziekanatu.Models.VM;

namespace Wspomaganie_dziekanatu.Controllers
{
    public partial class GradesController : Controller
    {
        [Authorize]
        // GET: Grades
        public virtual ActionResult Index()
        {
            var model = new List<ListaGrupVM>();

            using (var db = new ZpiDbCtx())
            {
                var user = Helpers.User.ZalogowanaOsoba(User);
                foreach (var grupa in db.Grupa.Where(e => e.Dydaktyk.OsobaId == user.OsobaId))
                {
                    model.Add(DashboardHelpers.Grupa(grupa));
                }
            }

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            return View(model);
        }

        [Authorize]
        // GET: Grades/{courseId}
        public virtual ActionResult GradesCourse(int courseId)
        {
            using (var db = new ZpiDbCtx())
            {
                var kurs = db.FormaPrzedmiotu.FirstOrDefault(e => e.KartaPrzedmiotuId == courseId);
                var listaGrup = new List<ListaGrupVM>();
                foreach (Grupa g in kurs.Grupa)
                {
                    listaGrup.Add(DashboardHelpers.Grupa(g));
                }

                ViewBag.CourseName = kurs.KartaPrzedmiotu.NazwaPl;

                /* breadcrumbs data */
                var user = Helpers.User.ZalogowanaOsoba(User);
                /* courses list*/
                int dydaktykId = Helpers.User.IdDydaktyka(User);
                ViewBag.Courses = RaportsHelpers.KursyDydaktyka(dydaktykId);

                ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
                return View(listaGrup);
            }
        }

        [Authorize]
        [HttpGet]
        // GET: Grades/{courseId}/{groupId}
        public virtual ActionResult GradesGroup(int courseId, int groupId)
        {

            using (var db = new ZpiDbCtx())
            {
                var model = new List<GradesVM>();
                //var grupa = db.TerminZajec.FirstOrDefault(e => e.TerminZajecId == groupId);
                var terms = db.TerminZajec.Where(e => e.GrupaId == groupId).OrderBy(e => e.Data);
                var students = db.StudentZapisanyDoGrupy.Where(e => e.GrupaId == groupId);

                foreach (var s in students)
                {
                    var obecnosci = new List<Obecnosc>();
                    var terminy = new List<String>();
                    foreach (var t in terms)
                    {
                        obecnosci.Add(s.Student.Obecnosc.First(e => e.TerminZajecId == t.TerminZajecId));
                        terminy.Add(t.Data.ToString("dd.MM.yyyy"));
                    }

                    model.Add(new GradesVM
                    {
                        Zaliczenie = new G_Zaliczenie
                        {
                            Ocena1 = s.Ocena1,
                            Ocena2 = s.Ocena2,
                            GrupaId = s.GrupaId,
                            StudentId = s.StudentId
                        },
                        Student = new G_Student
                        {
                            StudentId = s.Student.StudentId.ToString(),
                            NrAlbumu = s.Student.NrAlbumu,
                            Imie = s.Student.Osoba.Imie,
                            Nazwisko = s.Student.Osoba.Nazwisko
                        },
                        Obecnosci = obecnosci,
                        Terminy = terminy
                    });
                }

                /* breadcrumbs data */
                var user = Helpers.User.ZalogowanaOsoba(User);
                /* courses list*/
                int dydaktykId = Helpers.User.IdDydaktyka(User);
                ViewBag.Courses = RaportsHelpers.KursyDydaktyka(dydaktykId);


                /* groups list*/
                var listaGrup = new List<ListaGrupVM>();
                foreach (Grupa g in db.Grupa.Where(e => e.Dydaktyk.OsobaId == user.OsobaId && e.FormaPrzedmiotu.KartaPrzedmiotuId == courseId))
                {
                    listaGrup.Add(DashboardHelpers.Grupa(g));
                }
                ViewBag.Groups = listaGrup;

                ViewBag.CourseName = db.KartaPrzedmiotu.First(e => e.KartaPrzedmiotuId == courseId).NazwaPl;
                ViewBag.GroupName = CultureInfo.CurrentCulture.DateTimeFormat.DayNames[(int)terms.First().Data.DayOfWeek] + ", " + DashboardHelpers.GodzinyZajec(terms.First().GodzinaOd, terms.First().GodzinaDo);
                ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
                return View(model);
            }
        }

        [Authorize]
        [HttpPost]
        // POST: Grades/{courseId}/{groupId}
        public virtual ActionResult GradesGroup(List<GradesDM> model)
        {

            if (ModelState.IsValid)
            {
                using (var db = new ZpiDbCtx())
                {
                    foreach (var g in model)
                    {
                        Nullable<decimal> ocena1 = g.Oceny.Ocena1;
                        Nullable<decimal> ocena2 = g.Oceny.Ocena2;
                        int grupaId = g.Oceny.GrupaId;
                        int studentId = g.Oceny.StudentId;

                        var grades = db.StudentZapisanyDoGrupy.First(e => e.GrupaId == grupaId && e.StudentId == studentId);
                        grades.Ocena1 = ocena1;
                        grades.Ocena2 = ocena2;

                        foreach (var o in g.Obecnosci)
                        {
                            int terminZajecId = o.TerminZajecId;
                            Nullable<bool> czyObecny = o.CzyObecny;

                            var presences = db.Obecnosc.First(e => e.TerminZajecId == terminZajecId && e.StudentId == studentId);
                            presences.CzyObecny = czyObecny;
                        }
                    }


                    int groupId = int.Parse(RouteData.Values["groupId"].ToString());
                    var grupa = DashboardHelpers.Grupa(db.Grupa.First(e => e.GrupaId == groupId));

                    AktywnoscHelpers.addActivity(new AktywnoscVM()
                    {
                        UzytkownikId = Helpers.User.ZalogowanyId(User),
                        URL = "/Grades/" + RouteData.Values["courseId"] + "/" + RouteData.Values["groupId"],
                        Data = DateTime.Now,
                        Opis = "Edytowałeś dziennik dla grupy: " + grupa.NazwaKursu + ", " + grupa.DzienTygodnia + " " + grupa.Godzina + ", " + grupa.TypPrzedmiotu
                    });


                    db.SaveChanges();
                    return Content("true", "text/plain");
                }
            }
            else
            {
                String message = "Wystąpił błąd.\n";
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        message += error.ErrorMessage.ToString() + "\n";
                    }
                }
                return Content(message, "text/plain");
            }
        }
    }
    
}