﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wspomaganie_dziekanatu.Models.VM;

namespace Wspomaganie_dziekanatu.Controllers
{
    public partial class NotificationsController : Controller
    {
        [Authorize]
        // GET: Notifications
        public virtual ActionResult Index()
        {
            var model = Helpers.PowiadomienieHelpers.getUserNotifications(User);

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            return View(model);
        }

        [Authorize]
        // GET: Notifications/CountUnreaded
        public virtual ActionResult CountUnreaded()
        {
            if (Request.IsAjaxRequest())
            {
                int num = Helpers.PowiadomienieHelpers.getUserNewNotificationsNumber(User);
                return Content(num.ToString(), "text/plain");
            }
            else
            {
                return Redirect("/");
            }
        }

        [Authorize]
        // GET: Notifications/Simple
        public virtual ActionResult Simple()
        {
            if (Request.IsAjaxRequest())
            {
                var model = Helpers.PowiadomienieHelpers.getUserSimpleNotifications(User);
                return PartialView("_Simple", model);
            }
            else
            {
                return Redirect("/");
            }
        }

        [Authorize]
        [HttpPost]
        // POST: Notifications/MarkAsRead/{id}
        public virtual ActionResult MarkAsRead(int id)
        {
            Helpers.PowiadomienieHelpers.markAsRead(User, id);
            return Content("true", "text/plain");
        }

        [Authorize]
        [HttpPost]
        // POST: Notifications/MarkAllAsRead
        public virtual ActionResult MarkAllAsRead()
        {
            Helpers.PowiadomienieHelpers.markAllAsRead(User);
            return Content("true", "text/plain");
        }

        [Authorize]
        [HttpPost]
        // POST: Notifications/Add
        public virtual ActionResult Add(PowiadomienieVM notification)
        {
            Helpers.PowiadomienieHelpers.addNotification(notification);
            return Content("true", "text/plain");
        }
    }
}