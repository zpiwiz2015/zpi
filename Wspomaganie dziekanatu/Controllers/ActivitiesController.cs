﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wspomaganie_dziekanatu.Models;
using Wspomaganie_dziekanatu.Models.VM;

namespace Wspomaganie_dziekanatu.Controllers
{
    public partial class ActivitiesController : Controller
    {
        [Authorize]
        // GET: Activities
        public virtual ActionResult Index()
        {
            var model = Helpers.AktywnoscHelpers.getUserActivities(User);

            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            return View(model);
        }

        [Authorize]
        // GET: Activities/List
        public virtual ActionResult List()
        {
            if (Request.IsAjaxRequest())
            {
                var model = Helpers.AktywnoscHelpers.getUserActivities(User, false);
                return PartialView("_List", model);
            }
            else
            {
                return Redirect("/");
            }
        }
    }
}