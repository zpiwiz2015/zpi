﻿using iTextSharp.text;
using MvcRazorToPdf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Wspomaganie_dziekanatu.Models.VM;

namespace Wspomaganie_dziekanatu.Controllers
{
    public partial class TestController : Controller
    {
        [Authorize]
        // GET: Test
        public virtual ActionResult Index()
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            return View();
        }

        [Authorize]
        // GET: Test/pdf
        public virtual ActionResult Pdf()
        {
            //return View();
            return new PdfActionResult(null);
            /*{
                FileDownloadName = "DomyslnaNazwa.pdf"
            };*/
        }

        [Authorize]
        [HttpPost]
        // POST: Test/Email/{emailAddress}
        public async virtual Task<ActionResult> Email(String emailAddress)
        {
            var body = "<p>Email From: {0} ({1})</p><p>Message:</p><p>{2}</p>";
            var message = new MailMessage();
            message.To.Add(new MailAddress(emailAddress));
            message.Subject = "Your email subject";
            message.Body = string.Format(body, "From Name", "From Email", "Messageeeeeee");
            message.IsBodyHtml = true;

            using (var smtp = new SmtpClient())
            {
                await smtp.SendMailAsync(message);

                return Content("true", "text/plain");
            }
        }
    }
}