﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Wspomaganie_dziekanatu.Controllers
{
    public partial class DidacticsController : Controller
    {
        // GET: Didactics
        public virtual ActionResult Index()
        {
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            return View();
        }
    }
}