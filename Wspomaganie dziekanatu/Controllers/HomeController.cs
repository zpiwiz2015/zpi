﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wspomaganie_dziekanatu.Helpers;
using Wspomaganie_dziekanatu.Helpers.Enums;
using Wspomaganie_dziekanatu.Models;
using Wspomaganie_dziekanatu.Models.VM;

namespace Wspomaganie_dziekanatu.Controllers
{
    public partial class HomeController : Controller
    {
        [Authorize(Roles = "")]
        public virtual ActionResult Index()
        {
            var model = new DashboardVM
            {
                Obecnosci = new List<D_Obecnosci>(),
                ListaGrup = new List<ListaGrupVM>(),
                Raporty = new List<D_Raporty>()
            };
            using (var db = new ZpiDbCtx())
            {
                var user = Helpers.User.ZalogowanaOsoba(User);

                foreach (var grupa in db.Grupa.Where(e => e.Dydaktyk.OsobaId == user.OsobaId))
                {
                    var today = DateTime.Now.Date;
                    //var today = Convert.ToDateTime("2015-06-06"); //for testing
                    foreach (var zajecia in grupa.TerminZajec.Where(e => e.Data.Date == today))
                    {
                        model.Obecnosci.Add(new D_Obecnosci
                        {
                            IdGrupy = grupa.GrupaId,
                            IdKursu = grupa.FormaPrzedmiotu.KartaPrzedmiotu.KartaPrzedmiotuId,
                            Godzina = DashboardHelpers.GodzinyZajec(zajecia.GodzinaOd, zajecia.GodzinaDo),
                            NazwaKursu = zajecia.Grupa.FormaPrzedmiotu.KartaPrzedmiotu.NazwaPl,
                            TypKursu = grupa.FormaPrzedmiotu.TypPrzedmiotu.Wartosc
                        });
                    }

                    model.ListaGrup.Add(DashboardHelpers.Grupa(grupa));
                }

                model.Raporty = DashboardHelpers.Raporty(db.Dydaktyk.First(e => e.OsobaId == user.OsobaId).DydaktykId);

            }
            
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            return View(model);
            
        }

        [Authorize]
        public virtual ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            return View();
        }

        [Authorize]
        public virtual ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            ViewBag.IsAjaxRequest = Request.IsAjaxRequest();
            return View();
        }
    }
}