﻿using System.Web;
using System.Web.Optimization;

namespace Wspomaganie_dziekanatu
{
    public class BundleConfig
    {
        // Więcej informacji dotyczących tworzenia pakietów można znaleźć na stronie http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryunobtrusiveajax").Include(
                        "~/Scripts/jquery.unobtrusive-ajax*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryhistory").Include(
                        "~/Scripts/history.js/bundled/html4+html5/jquery.history.js"));

            // Użyj wersji deweloperskiej biblioteki Modernizr do nauki i opracowywania rozwiązań. Następnie, kiedy wszystko będzie
            // gotowe do produkcji, wybierz tylko potrzebne testy za pomocą narzędzia kompilacji z witryny http://modernizr.com.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/silviomoreto-bootstrap-select/bootstrap-select.js"
                      ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/silviomoreto-bootstrap-select/bootstrap-select.css",
                      "~/Content/ekp.css",
                      "~/Content/PagedList.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                       "~/Scripts/app.js"));

            // Wybierz wartość false dla ustawienia EnableOptimizations na potrzeby debugowania. Aby uzyskać więcej informacji,
            // odwiedź stronę http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = true;
        }
    }
}
