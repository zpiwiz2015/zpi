﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Wspomaganie_dziekanatu
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            routes.MapRoute(
                name: "GroupsCourse",
                url: "Groups/{courseId}",
                defaults: new { controller = "Groups", action = "ViewCourse" }
            );

            routes.MapRoute(
                name: "GroupsGroup",
                url: "Groups/{courseId}/{groupId}",
                defaults: new { controller = "Groups", action = "ViewGroup" }
            );


            routes.MapRoute(
                name: "GradesCourse",
                url: "Grades/{courseId}",
                defaults: new { controller = "Grades", action = "GradesCourse" }
            );

            routes.MapRoute(
                name: "GradesGroup",
                url: "Grades/{courseId}/{groupId}",
                defaults: new { controller = "Grades", action = "GradesGroup" }
            );

            routes.MapRoute(
                name: "CourseCards",
                url: "CourseCards/{courseId}/{actionType}",
                defaults: new { controller = "CourseCards", action="Create"}
            );


            routes.MapRoute(
                name: "ReportsCreateSROP",
                url: "Reports/{courseId}/srop/{actionType}",
                defaults: new { controller = "Reports", action = "CreateSROPReport" }
            );

            routes.MapRoute(
                name: "ReportsCreateFRP",
                url: "Reports/{courseId}/frp/{actionType}/{formId}",
                defaults: new { controller = "Reports", action = "CreateFRPReport" }
            );

            routes.MapRoute(
                name: "ReportsCourse",
                url: "Reports/{courseId}",
                defaults: new { controller = "Reports", action = "ViewCourse" }
            );


            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
