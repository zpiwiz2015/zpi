﻿using System.Web;
using System.Web.Mvc;

namespace Wspomaganie_dziekanatu
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
