﻿$(document)
    .on("click", "a[href^='/']:not([target])", function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        var title = $(this).data('title');
        History.pushState(null, title, url);
    })
    .on("click", ".nav-panel-toggle", function () {
        $('#nav-panel').toggleClass('collapsed');
    })
    .on("click", "#notifications-dropdown-toggle", function () {
        updateNotifications();
    });

$("#content")
    .on("mouseup", "tr[data-href^='/']", function (e) {
        var url = $(this).attr('data-href');
        if (e.which < 2) {
            var title = $(this).data('title');
            History.pushState(null, title, url);
        } else {
            window.open(url, '_blank');
        }
    })
    .on("click", "#activities-panel-toggle", function () {
        $("#nav-panel").toggleClass('with-activities-panel');
        $("#activities-panel").toggle();
    });
    
$("#nav-panel nav.navigation > ul > li:has(ul) > a").click(function () {
    if ($('#nav-panel').hasClass('collapsed') == false || $(window).width() < 768) {
        $("#nav-panel nav.navigation > ul > li > ul").slideUp(300);
        $("#nav-panel nav.navigation > ul > li").removeClass('active');
        if (!$(this).next().is(":visible")) {
            $(this).next().slideToggle(300, function () {
                //console.log($(this));//.removeAttr("style");
                //$("#nav-panel:not(.collapsed)").getNiceScroll().resize();
            });
            $(this).closest('li').addClass('active');
        }
        return false;
    }
});

History.Adapter.bind(window, 'statechange', function () {
    //console.log('state changed');
    var State = History.getState();
    if (State.url === '') {
        return;
    }
    ajaxViewChange(State.url, State.title);
});

var ajaxViewChange = function (url, title) {
    //console.log(url);
    //console.log(title);
    $("#change-page-overlay").remove();
    $('#content').prepend('<div id="change-page-overlay"><span class="glyphicon glyphicon-refresh glyphicon-spin"></span></div>');
    $.ajax({
        type: "GET",
        url: url,
        dataType: "html",
        cache: false,
        success: function (data, status, xhr) {
            var JSONheader = JSON.parse(xhr.getResponseHeader("X-Responded-JSON"));
            //user not logged in
            if (JSONheader !== null && JSONheader.status == 401 && JSONheader.headers.location) {
                window.location = JSONheader.headers.location;
            } else {
                $('#body').css({ 'opacity': 0 });
                $('#content').html(data);
                fitToHeaderHeight();
                bindGUIActions();
                updateMenuState();
                $('#body').css({ 'opacity': 0 });
                $('#body').animate({ 'opacity': 1 }, 1000);
            }
        },
        error: function (xhr, status, error) {
            History.back();
            $("#change-page-overlay").remove();
        }
    });
};

var fitToHeaderHeight = function () {
    var headerHeight = $('#content > header').outerHeight();
    $('#body').css({ "padding-top": (headerHeight + 10) + "px" });
    $('#activities-panel').css({ "margin-top": headerHeight + "px" });
};

var bindGUIActions = function () {
    $('.selectpicker').selectpicker();
    $('[data-toggle="tooltip"]').tooltip();

    $('header .breadcrumb .selectpicker').change(function () {
        var url = $(this).val();
        var title = "TODO: tile";
        History.pushState(null, title, url);
    });

    $('header .breadcrumb .middle-select + .bootstrap-select .dropdown-menu .selected').click(function () {
        var index = $(this).attr('data-original-index');
        var select = $('select.middle-select', $(this).parents('li'));
        select[0].selectedIndex = index;
        select.change();
    });

    $('.filter-input').keyup(function () {
        var regex = new RegExp($(this).val(), 'i');

        var elem;
        var tableId = $(this).attr('data-table-id');
        if (tableId) {
            elem = $('#' + tableId + ' tr:has(td)');
        } else {
            var listId = $(this).attr('data-list-id');
            elem = $('#' + listId + ' > li');
        }

        elem.hide()
            .filter(function () {
                return regex.test($(this).text())
            })
            .show();
    });

    $('.table-list-filter-input').keyup(function () {
        var regex = new RegExp($(this).val(), 'i');
        var tableId = $(this).attr('data-table-id');
        $('#' + tableId + ' tr:has(td)').hide();
        var indexes = [];
        var filtered = $('#' + tableId + ' .table-course-header').filter(function () {
            var result = regex.test($(this).text());
            if (result) {
                indexes.push($(this).parent().index());
            }
            return result;
        });
        
        for (var i = 0; i < indexes.length; i++) {
            var startElem = $('#' + tableId + ' tr').eq(indexes[i]);
            startElem.show();
            startElem.nextUntil('tr:has(.table-course-header)').show();
        }
    });
};

var updateMenuState = function () {
    var controllerName,
        menuLink,
        parent;

    if (window.location.pathname !== "/") {
        controllerName = window.location.pathname.split('/')[1];
        menuLink = $("#nav-panel nav.navigation a[href^='/" + controllerName + "']");
    } else {
        menuLink = $("#nav-panel nav.navigation > ul > li:first-child > a");
    }

    parent = (menuLink.length > 1) ? menuLink.eq(0).closest('.has-submenu') : menuLink.parent();

    if (!parent.hasClass("active")) {
        $("#nav-panel nav.navigation > ul > li.has-submenu > ul").slideUp(300, function () {});
        $("#nav-panel nav.navigation > ul > li").removeClass("active");
        parent.addClass("active");
    }
    
};

var submitForm = function(formId) {
    var form = $('#' + formId);

    $.ajax({
        url: form[0].action,
        type: form[0].method,
        data: form.serialize(),
        success: function (result) {
            if (result === "true") {
                updateActivities();
            } else {
                alert(result);
            }
        },
        error: function () {
            alert("Wystąpił błąd.");
        }
    });
}

var checkNotifications = function () {
    //console.log('checking notifications....');
    $.ajax({
        url: "/Notifications/CountUnreaded",
        type: "POST",
        success: function (result) {
            var num = "";
            if (result > 0) {
                num = result;
            }
            $("#notifications-badge").text(num);
        },
        error: function () {
            //alert("error");
        }
    });
}

var updateNotifications = function () {
    $('#notifications-list li:first-child').after('<li class="notifications-spiner"><span class="glyphicon glyphicon-refresh glyphicon-spin"></span></li>');
    $.ajax({
        url: "/Notifications/Simple",
        type: "POST",
        success: function(data) {
            $('#notifications-list li:not(:first-child):not(:last-child)').remove();
            $('#notifications-list li:first-child').after(data);
        },
        error: function() {
            $('#notifications-list .notifications-spiner').remove();
            //alert("error");
        }
    });
}

var markNotificationAsRead = function(id) {
    $.ajax({
        url: "/Notifications/MarkAsRead",
        type: "POST",
        data: "id=" + id,
        success: function (result) {
            $(".list-group-item-warning[data-notification-id='" + id + "']").removeClass("list-group-item-warning");
            checkNotifications();
            //alert(result);
        },
        error: function () {
            //alert("error");
        }
    });
}

var markAllNotificationsAsRead = function() {
    $.ajax({
        url: "/Notifications/MarkAllAsRead",
        type: "POST",
        success: function (result) {
            $(".list-group-item-warning[data-notification-id]").removeClass("list-group-item-warning");
            checkNotifications();
            //alert(result);
        },
        error: function () {
            //alert("error");
        }
    });
}

var updateActivities = function () {
    $.ajax({
        url: "/Activities/List",
        type: "GET",
        success: function (data) {
            $('#activities-panel').html(data);
        },
        error: function () {
            //alert("error");
        }
    });
}

$(window).resize(function () {
    fitToHeaderHeight();
});


fitToHeaderHeight();
bindGUIActions();
updateMenuState();
window.setInterval(checkNotifications, 60000);