// <auto-generated />
// This file was generated by a T4 template.
// Don't change it directly as your change would get overwritten.  Instead, make changes
// to the .tt file (i.e. the T4 template) and save it to regenerate this file.

// Make sure the compiler doesn't complain about missing Xml comments and CLS compliance
// 0108: suppress "Foo hides inherited member Foo. Use the new keyword if hiding was intended." when a controller and its abstract parent are both processed
// 0114: suppress "Foo.BarController.Baz()' hides inherited member 'Qux.BarController.Baz()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword." when an action (with an argument) overrides an action in a parent controller
#pragma warning disable 1591, 3008, 3009, 0108, 0114
#region T4MVC

using System;
using System.Diagnostics;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using System.Web.Routing;
using T4MVC;
namespace Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Controllers
{
    public partial class KartyPrzedmiotowController
    {
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public KartyPrzedmiotowController() { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected KartyPrzedmiotowController(Dummy d) { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoute(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(Task<ActionResult> taskResult)
        {
            return RedirectToAction(taskResult.Result);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoutePermanent(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(Task<ActionResult> taskResult)
        {
            return RedirectToActionPermanent(taskResult.Result);
        }

        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult Index()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Index);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult Details()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Details);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult CreateRowForWymaganieWstepne()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CreateRowForWymaganieWstepne);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult CreateRowForCelPrzedmiotu()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CreateRowForCelPrzedmiotu);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult CreateRowForNarzedzieDydaktyczne()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CreateRowForNarzedzieDydaktyczne);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult CreateRowForLiteraturaP()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CreateRowForLiteraturaP);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult CreateRowForLiteraturaU()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CreateRowForLiteraturaU);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult CreateRowForAutor()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CreateRowForAutor);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult CreateRowForPekW()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CreateRowForPekW);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult CreateRowForPekU()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CreateRowForPekU);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult CreateRowForPekK()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CreateRowForPekK);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult CreateRowForTrescW()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CreateRowForTrescW);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult CreateRowForTrescC()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CreateRowForTrescC);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult CreateRowForTrescL()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CreateRowForTrescL);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult CreateRowForTrescP()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CreateRowForTrescP);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult CreateRowForTrescS()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CreateRowForTrescS);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public KartyPrzedmiotowController Actions { get { return MVC.ElektroniczneKartyPrzedmiotow.KartyPrzedmiotow; } }
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Area = "ElektroniczneKartyPrzedmiotow";
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Name = "KartyPrzedmiotow";
        [GeneratedCode("T4MVC", "2.0")]
        public const string NameConst = "KartyPrzedmiotow";

        static readonly ActionNamesClass s_actions = new ActionNamesClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionNamesClass ActionNames { get { return s_actions; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNamesClass
        {
            public readonly string Index = "Index";
            public readonly string Details = "Details";
            public readonly string Create = "Create";
            public readonly string CreateRowForWymaganieWstepne = "CreateRowForWymaganieWstepne";
            public readonly string CreateRowForCelPrzedmiotu = "CreateRowForCelPrzedmiotu";
            public readonly string CreateRowForNarzedzieDydaktyczne = "CreateRowForNarzedzieDydaktyczne";
            public readonly string CreateRowForLiteraturaP = "CreateRowForLiteraturaP";
            public readonly string CreateRowForLiteraturaU = "CreateRowForLiteraturaU";
            public readonly string CreateRowForAutor = "CreateRowForAutor";
            public readonly string CreateRowForPekW = "CreateRowForPekW";
            public readonly string CreateRowForPekU = "CreateRowForPekU";
            public readonly string CreateRowForPekK = "CreateRowForPekK";
            public readonly string CreateRowForTrescW = "CreateRowForTrescW";
            public readonly string CreateRowForTrescC = "CreateRowForTrescC";
            public readonly string CreateRowForTrescL = "CreateRowForTrescL";
            public readonly string CreateRowForTrescP = "CreateRowForTrescP";
            public readonly string CreateRowForTrescS = "CreateRowForTrescS";
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNameConstants
        {
            public const string Index = "Index";
            public const string Details = "Details";
            public const string Create = "Create";
            public const string CreateRowForWymaganieWstepne = "CreateRowForWymaganieWstepne";
            public const string CreateRowForCelPrzedmiotu = "CreateRowForCelPrzedmiotu";
            public const string CreateRowForNarzedzieDydaktyczne = "CreateRowForNarzedzieDydaktyczne";
            public const string CreateRowForLiteraturaP = "CreateRowForLiteraturaP";
            public const string CreateRowForLiteraturaU = "CreateRowForLiteraturaU";
            public const string CreateRowForAutor = "CreateRowForAutor";
            public const string CreateRowForPekW = "CreateRowForPekW";
            public const string CreateRowForPekU = "CreateRowForPekU";
            public const string CreateRowForPekK = "CreateRowForPekK";
            public const string CreateRowForTrescW = "CreateRowForTrescW";
            public const string CreateRowForTrescC = "CreateRowForTrescC";
            public const string CreateRowForTrescL = "CreateRowForTrescL";
            public const string CreateRowForTrescP = "CreateRowForTrescP";
            public const string CreateRowForTrescS = "CreateRowForTrescS";
        }


        static readonly ActionParamsClass_Index s_params_Index = new ActionParamsClass_Index();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_Index IndexParams { get { return s_params_Index; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_Index
        {
            public readonly string sortOrder = "sortOrder";
            public readonly string currentFilter = "currentFilter";
            public readonly string searchString = "searchString";
            public readonly string page = "page";
        }
        static readonly ActionParamsClass_Details s_params_Details = new ActionParamsClass_Details();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_Details DetailsParams { get { return s_params_Details; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_Details
        {
            public readonly string id = "id";
        }
        static readonly ActionParamsClass_Create s_params_Create = new ActionParamsClass_Create();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_Create CreateParams { get { return s_params_Create; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_Create
        {
            public readonly string model = "model";
        }
        static readonly ActionParamsClass_CreateRowForWymaganieWstepne s_params_CreateRowForWymaganieWstepne = new ActionParamsClass_CreateRowForWymaganieWstepne();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_CreateRowForWymaganieWstepne CreateRowForWymaganieWstepneParams { get { return s_params_CreateRowForWymaganieWstepne; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_CreateRowForWymaganieWstepne
        {
            public readonly string prefix = "prefix";
        }
        static readonly ActionParamsClass_CreateRowForCelPrzedmiotu s_params_CreateRowForCelPrzedmiotu = new ActionParamsClass_CreateRowForCelPrzedmiotu();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_CreateRowForCelPrzedmiotu CreateRowForCelPrzedmiotuParams { get { return s_params_CreateRowForCelPrzedmiotu; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_CreateRowForCelPrzedmiotu
        {
            public readonly string prefix = "prefix";
        }
        static readonly ActionParamsClass_CreateRowForNarzedzieDydaktyczne s_params_CreateRowForNarzedzieDydaktyczne = new ActionParamsClass_CreateRowForNarzedzieDydaktyczne();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_CreateRowForNarzedzieDydaktyczne CreateRowForNarzedzieDydaktyczneParams { get { return s_params_CreateRowForNarzedzieDydaktyczne; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_CreateRowForNarzedzieDydaktyczne
        {
            public readonly string prefix = "prefix";
        }
        static readonly ActionParamsClass_CreateRowForLiteraturaP s_params_CreateRowForLiteraturaP = new ActionParamsClass_CreateRowForLiteraturaP();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_CreateRowForLiteraturaP CreateRowForLiteraturaPParams { get { return s_params_CreateRowForLiteraturaP; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_CreateRowForLiteraturaP
        {
            public readonly string prefix = "prefix";
        }
        static readonly ActionParamsClass_CreateRowForLiteraturaU s_params_CreateRowForLiteraturaU = new ActionParamsClass_CreateRowForLiteraturaU();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_CreateRowForLiteraturaU CreateRowForLiteraturaUParams { get { return s_params_CreateRowForLiteraturaU; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_CreateRowForLiteraturaU
        {
            public readonly string prefix = "prefix";
        }
        static readonly ActionParamsClass_CreateRowForAutor s_params_CreateRowForAutor = new ActionParamsClass_CreateRowForAutor();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_CreateRowForAutor CreateRowForAutorParams { get { return s_params_CreateRowForAutor; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_CreateRowForAutor
        {
            public readonly string prefix = "prefix";
        }
        static readonly ActionParamsClass_CreateRowForPekW s_params_CreateRowForPekW = new ActionParamsClass_CreateRowForPekW();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_CreateRowForPekW CreateRowForPekWParams { get { return s_params_CreateRowForPekW; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_CreateRowForPekW
        {
            public readonly string prefix = "prefix";
        }
        static readonly ActionParamsClass_CreateRowForPekU s_params_CreateRowForPekU = new ActionParamsClass_CreateRowForPekU();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_CreateRowForPekU CreateRowForPekUParams { get { return s_params_CreateRowForPekU; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_CreateRowForPekU
        {
            public readonly string prefix = "prefix";
        }
        static readonly ActionParamsClass_CreateRowForPekK s_params_CreateRowForPekK = new ActionParamsClass_CreateRowForPekK();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_CreateRowForPekK CreateRowForPekKParams { get { return s_params_CreateRowForPekK; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_CreateRowForPekK
        {
            public readonly string prefix = "prefix";
        }
        static readonly ActionParamsClass_CreateRowForTrescW s_params_CreateRowForTrescW = new ActionParamsClass_CreateRowForTrescW();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_CreateRowForTrescW CreateRowForTrescWParams { get { return s_params_CreateRowForTrescW; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_CreateRowForTrescW
        {
            public readonly string prefix = "prefix";
        }
        static readonly ActionParamsClass_CreateRowForTrescC s_params_CreateRowForTrescC = new ActionParamsClass_CreateRowForTrescC();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_CreateRowForTrescC CreateRowForTrescCParams { get { return s_params_CreateRowForTrescC; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_CreateRowForTrescC
        {
            public readonly string prefix = "prefix";
        }
        static readonly ActionParamsClass_CreateRowForTrescL s_params_CreateRowForTrescL = new ActionParamsClass_CreateRowForTrescL();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_CreateRowForTrescL CreateRowForTrescLParams { get { return s_params_CreateRowForTrescL; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_CreateRowForTrescL
        {
            public readonly string prefix = "prefix";
        }
        static readonly ActionParamsClass_CreateRowForTrescP s_params_CreateRowForTrescP = new ActionParamsClass_CreateRowForTrescP();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_CreateRowForTrescP CreateRowForTrescPParams { get { return s_params_CreateRowForTrescP; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_CreateRowForTrescP
        {
            public readonly string prefix = "prefix";
        }
        static readonly ActionParamsClass_CreateRowForTrescS s_params_CreateRowForTrescS = new ActionParamsClass_CreateRowForTrescS();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_CreateRowForTrescS CreateRowForTrescSParams { get { return s_params_CreateRowForTrescS; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_CreateRowForTrescS
        {
            public readonly string prefix = "prefix";
        }
        static readonly ViewsClass s_views = new ViewsClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ViewsClass Views { get { return s_views; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ViewsClass
        {
            static readonly _ViewNamesClass s_ViewNames = new _ViewNamesClass();
            public _ViewNamesClass ViewNames { get { return s_ViewNames; } }
            public class _ViewNamesClass
            {
                public readonly string _CreateRowAutor = "_CreateRowAutor";
                public readonly string _CreateRowCelPrzedmiotu = "_CreateRowCelPrzedmiotu";
                public readonly string _CreateRowLiteraturaP = "_CreateRowLiteraturaP";
                public readonly string _CreateRowLiteraturaU = "_CreateRowLiteraturaU";
                public readonly string _CreateRowNarzedzieDydaktyczne = "_CreateRowNarzedzieDydaktyczne";
                public readonly string _CreateRowPekK = "_CreateRowPekK";
                public readonly string _CreateRowPekU = "_CreateRowPekU";
                public readonly string _CreateRowPekW = "_CreateRowPekW";
                public readonly string _CreateRowTrescC = "_CreateRowTrescC";
                public readonly string _CreateRowTrescL = "_CreateRowTrescL";
                public readonly string _CreateRowTrescP = "_CreateRowTrescP";
                public readonly string _CreateRowTrescS = "_CreateRowTrescS";
                public readonly string _CreateRowTrescW = "_CreateRowTrescW";
                public readonly string _CreateRowWymaganieWstepne = "_CreateRowWymaganieWstepne";
                public readonly string Create = "Create";
                public readonly string Details = "Details";
                public readonly string Index = "Index";
            }
            public readonly string _CreateRowAutor = "~/Areas/ElektroniczneKartyPrzedmiotow/Views/KartyPrzedmiotow/_CreateRowAutor.cshtml";
            public readonly string _CreateRowCelPrzedmiotu = "~/Areas/ElektroniczneKartyPrzedmiotow/Views/KartyPrzedmiotow/_CreateRowCelPrzedmiotu.cshtml";
            public readonly string _CreateRowLiteraturaP = "~/Areas/ElektroniczneKartyPrzedmiotow/Views/KartyPrzedmiotow/_CreateRowLiteraturaP.cshtml";
            public readonly string _CreateRowLiteraturaU = "~/Areas/ElektroniczneKartyPrzedmiotow/Views/KartyPrzedmiotow/_CreateRowLiteraturaU.cshtml";
            public readonly string _CreateRowNarzedzieDydaktyczne = "~/Areas/ElektroniczneKartyPrzedmiotow/Views/KartyPrzedmiotow/_CreateRowNarzedzieDydaktyczne.cshtml";
            public readonly string _CreateRowPekK = "~/Areas/ElektroniczneKartyPrzedmiotow/Views/KartyPrzedmiotow/_CreateRowPekK.cshtml";
            public readonly string _CreateRowPekU = "~/Areas/ElektroniczneKartyPrzedmiotow/Views/KartyPrzedmiotow/_CreateRowPekU.cshtml";
            public readonly string _CreateRowPekW = "~/Areas/ElektroniczneKartyPrzedmiotow/Views/KartyPrzedmiotow/_CreateRowPekW.cshtml";
            public readonly string _CreateRowTrescC = "~/Areas/ElektroniczneKartyPrzedmiotow/Views/KartyPrzedmiotow/_CreateRowTrescC.cshtml";
            public readonly string _CreateRowTrescL = "~/Areas/ElektroniczneKartyPrzedmiotow/Views/KartyPrzedmiotow/_CreateRowTrescL.cshtml";
            public readonly string _CreateRowTrescP = "~/Areas/ElektroniczneKartyPrzedmiotow/Views/KartyPrzedmiotow/_CreateRowTrescP.cshtml";
            public readonly string _CreateRowTrescS = "~/Areas/ElektroniczneKartyPrzedmiotow/Views/KartyPrzedmiotow/_CreateRowTrescS.cshtml";
            public readonly string _CreateRowTrescW = "~/Areas/ElektroniczneKartyPrzedmiotow/Views/KartyPrzedmiotow/_CreateRowTrescW.cshtml";
            public readonly string _CreateRowWymaganieWstepne = "~/Areas/ElektroniczneKartyPrzedmiotow/Views/KartyPrzedmiotow/_CreateRowWymaganieWstepne.cshtml";
            public readonly string Create = "~/Areas/ElektroniczneKartyPrzedmiotow/Views/KartyPrzedmiotow/Create.cshtml";
            public readonly string Details = "~/Areas/ElektroniczneKartyPrzedmiotow/Views/KartyPrzedmiotow/Details.cshtml";
            public readonly string Index = "~/Areas/ElektroniczneKartyPrzedmiotow/Views/KartyPrzedmiotow/Index.cshtml";
        }
    }

    [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
    public partial class T4MVC_KartyPrzedmiotowController : Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Controllers.KartyPrzedmiotowController
    {
        public T4MVC_KartyPrzedmiotowController() : base(Dummy.Instance) { }

        [NonAction]
        partial void IndexOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string sortOrder, string currentFilter, string searchString, int? page);

        [NonAction]
        public override System.Web.Mvc.ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Index);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "sortOrder", sortOrder);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "currentFilter", currentFilter);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "searchString", searchString);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "page", page);
            IndexOverride(callInfo, sortOrder, currentFilter, searchString, page);
            return callInfo;
        }

        [NonAction]
        partial void DetailsOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, int id);

        [NonAction]
        public override System.Web.Mvc.ActionResult Details(int id)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Details);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "id", id);
            DetailsOverride(callInfo, id);
            return callInfo;
        }

        [NonAction]
        partial void CreateOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult Create()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Create);
            CreateOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void CreateOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Models.KartaPrzedmiotuVM model);

        [NonAction]
        public override System.Web.Mvc.ActionResult Create(Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Models.KartaPrzedmiotuVM model)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Create);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "model", model);
            CreateOverride(callInfo, model);
            return callInfo;
        }

        [NonAction]
        partial void CreateRowForWymaganieWstepneOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string prefix);

        [NonAction]
        public override System.Web.Mvc.ActionResult CreateRowForWymaganieWstepne(string prefix)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CreateRowForWymaganieWstepne);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "prefix", prefix);
            CreateRowForWymaganieWstepneOverride(callInfo, prefix);
            return callInfo;
        }

        [NonAction]
        partial void CreateRowForCelPrzedmiotuOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string prefix);

        [NonAction]
        public override System.Web.Mvc.ActionResult CreateRowForCelPrzedmiotu(string prefix)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CreateRowForCelPrzedmiotu);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "prefix", prefix);
            CreateRowForCelPrzedmiotuOverride(callInfo, prefix);
            return callInfo;
        }

        [NonAction]
        partial void CreateRowForNarzedzieDydaktyczneOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string prefix);

        [NonAction]
        public override System.Web.Mvc.ActionResult CreateRowForNarzedzieDydaktyczne(string prefix)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CreateRowForNarzedzieDydaktyczne);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "prefix", prefix);
            CreateRowForNarzedzieDydaktyczneOverride(callInfo, prefix);
            return callInfo;
        }

        [NonAction]
        partial void CreateRowForLiteraturaPOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string prefix);

        [NonAction]
        public override System.Web.Mvc.ActionResult CreateRowForLiteraturaP(string prefix)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CreateRowForLiteraturaP);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "prefix", prefix);
            CreateRowForLiteraturaPOverride(callInfo, prefix);
            return callInfo;
        }

        [NonAction]
        partial void CreateRowForLiteraturaUOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string prefix);

        [NonAction]
        public override System.Web.Mvc.ActionResult CreateRowForLiteraturaU(string prefix)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CreateRowForLiteraturaU);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "prefix", prefix);
            CreateRowForLiteraturaUOverride(callInfo, prefix);
            return callInfo;
        }

        [NonAction]
        partial void CreateRowForAutorOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string prefix);

        [NonAction]
        public override System.Web.Mvc.ActionResult CreateRowForAutor(string prefix)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CreateRowForAutor);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "prefix", prefix);
            CreateRowForAutorOverride(callInfo, prefix);
            return callInfo;
        }

        [NonAction]
        partial void CreateRowForPekWOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string prefix);

        [NonAction]
        public override System.Web.Mvc.ActionResult CreateRowForPekW(string prefix)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CreateRowForPekW);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "prefix", prefix);
            CreateRowForPekWOverride(callInfo, prefix);
            return callInfo;
        }

        [NonAction]
        partial void CreateRowForPekUOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string prefix);

        [NonAction]
        public override System.Web.Mvc.ActionResult CreateRowForPekU(string prefix)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CreateRowForPekU);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "prefix", prefix);
            CreateRowForPekUOverride(callInfo, prefix);
            return callInfo;
        }

        [NonAction]
        partial void CreateRowForPekKOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string prefix);

        [NonAction]
        public override System.Web.Mvc.ActionResult CreateRowForPekK(string prefix)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CreateRowForPekK);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "prefix", prefix);
            CreateRowForPekKOverride(callInfo, prefix);
            return callInfo;
        }

        [NonAction]
        partial void CreateRowForTrescWOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string prefix);

        [NonAction]
        public override System.Web.Mvc.ActionResult CreateRowForTrescW(string prefix)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CreateRowForTrescW);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "prefix", prefix);
            CreateRowForTrescWOverride(callInfo, prefix);
            return callInfo;
        }

        [NonAction]
        partial void CreateRowForTrescCOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string prefix);

        [NonAction]
        public override System.Web.Mvc.ActionResult CreateRowForTrescC(string prefix)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CreateRowForTrescC);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "prefix", prefix);
            CreateRowForTrescCOverride(callInfo, prefix);
            return callInfo;
        }

        [NonAction]
        partial void CreateRowForTrescLOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string prefix);

        [NonAction]
        public override System.Web.Mvc.ActionResult CreateRowForTrescL(string prefix)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CreateRowForTrescL);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "prefix", prefix);
            CreateRowForTrescLOverride(callInfo, prefix);
            return callInfo;
        }

        [NonAction]
        partial void CreateRowForTrescPOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string prefix);

        [NonAction]
        public override System.Web.Mvc.ActionResult CreateRowForTrescP(string prefix)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CreateRowForTrescP);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "prefix", prefix);
            CreateRowForTrescPOverride(callInfo, prefix);
            return callInfo;
        }

        [NonAction]
        partial void CreateRowForTrescSOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string prefix);

        [NonAction]
        public override System.Web.Mvc.ActionResult CreateRowForTrescS(string prefix)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CreateRowForTrescS);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "prefix", prefix);
            CreateRowForTrescSOverride(callInfo, prefix);
            return callInfo;
        }

    }
}

#endregion T4MVC
#pragma warning restore 1591, 3008, 3009, 0108, 0114
