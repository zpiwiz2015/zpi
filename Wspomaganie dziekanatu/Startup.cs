﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Wspomaganie_dziekanatu.Startup))]
namespace Wspomaganie_dziekanatu
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
