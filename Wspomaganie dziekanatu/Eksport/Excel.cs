﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using Microsoft.Office.Interop.Excel;
//using Application = Microsoft.Office.Interop.Excel.Application;

namespace Wspomaganie_dziekanatu.Eksport
{
    //public class Excel
    //{
    //    /// <summary>
    //    /// Reprezentuje nową instancję programu Excel
    //    /// </summary>
    //    private Application app;
    //    /// <summary>
    //    /// Reprezentuje nowy workbook
    //    /// </summary>
    //    private Workbook wb;
    //    /// <summary>
    //    /// Reprezentuje aktywny worksheet
    //    /// </summary>
    //    private Worksheet ActiveWorkSheet;

    //    /// <summary>
    //    /// Konstruktor
    //    /// </summary>
    //    public Excel()
    //    {
    //        app = new Application();
    //        wb = app.Workbooks.Add(XlSheetType.xlWorksheet);
    //        ActiveWorkSheet = (Worksheet) app.ActiveSheet;
    //    }

    //#region Podstawowe metody
    //    /// <summary>
    //    /// Dodaje zawartość do komórki
    //    /// </summary>
    //    /// <param name="kolumna">Identyfikator kolumny</param>
    //    /// <param name="wiersz">Identyfikator wiersza</param>
    //    /// <param name="zawartosc">Zawartość komórki</param>
    //    private void DodajKomorke(int kolumna, int wiersz, string zawartosc)
    //    {
    //        ActiveWorkSheet.Cells[wiersz, kolumna] = zawartosc;
    //    }

    //    /// <summary>
    //    /// Wypełnia komórkę zadanym kolorem
    //    /// </summary>
    //    /// <param name="kolumna">Identyfikator kolumny</param>
    //    /// <param name="wiersz">Identyfikator wiersza</param>
    //    /// <param name="kolor">Kolor</param>
    //    private void KolorujKomorke(int kolumna, int wiersz, XlRgbColor kolor)
    //    {
    //        ActiveWorkSheet.Cells[wiersz, kolumna].Interior.Color = kolor;
    //    }

    //    /// <summary>
    //    /// Wypełnia komórki kolorem między zadanymi wierszami
    //    /// </summary>
    //    /// <param name="kolumna">Identyfikator kolumny</param>
    //    /// <param name="wierszOd">Identyfikator początkowego wiersza</param>
    //    /// <param name="wierszDo">Identyfikator końcowego wiersza</param>
    //    /// <param name="kolor">Kolor</param>
    //    private void KolorujKolumne(int kolumna, int wierszOd, int wierszDo, XlRgbColor kolor)
    //    {
    //        for (int i = wierszOd; i <= wierszDo; i++)
    //            ActiveWorkSheet.Cells[i, kolumna].Interior.Color = kolor;
    //    }

    //    /// <summary>
    //    /// Wypełnia komórki kolorem między zadanymi kolumnami
    //    /// </summary>
    //    /// <param name="kolumnaOd">Identyfikator początkowej kolumny</param>
    //    /// <param name="kolumnaDo">Identyfikator końcowej kolumny</param>
    //    /// <param name="wiersz">Identyfikator wiersza</param>
    //    /// <param name="kolor">Kolor</param>
    //    private void KolorujWiersz(int kolumnaOd, int kolumnaDo, int wiersz, XlRgbColor kolor)
    //    {
    //        for (int i = kolumnaOd; i <= kolumnaDo; i++)
    //            ActiveWorkSheet.Cells[i, wiersz].Interior.Color = kolor;
    //    }

    //    /// <summary>
    //    /// Pogrubia tekst w komórce
    //    /// </summary>
    //    /// <param name="kolumna">Identyfikator kolumny</param>
    //    /// <param name="wiersz">Identyfikator wiersza</param>
    //    private void PogrubienieKomorki(int kolumna, int wiersz)
    //    {
    //        ActiveWorkSheet.Cells[wiersz, kolumna].EntireRow.Font.Bold = true;
    //    }

    //    /// <summary>
    //    /// Dopasowuje szerokość kolumn na podstawie znajdującego się tekstu w komórkach
    //    /// </summary>
    //    private void AutoSzerokoscKolumn()
    //    {
    //        ActiveWorkSheet.Columns.AutoFit();
    //    }

    //    /// <summary>
    //    /// Uruchamia program Excel z wytworzonymi arkuszami
    //    /// </summary>
    //    private void PokazExcela()
    //    {
    //        app.Visible = true;
    //    }

    //    /// <summary>
    //    /// Dodaje nowy arkusz i ustawia go jako aktywny
    //    /// </summary>
    //    /// <param name="nazwa">Nazwa nowego arkusza</param>
    //    private void DodajNowyArkusz(string nazwa)
    //    {
    //        ActiveWorkSheet = (Worksheet) app.Worksheets.Add();
    //        ActiveWorkSheet.Name = nazwa;
    //    }

    //    /// <summary>
    //    /// Zmienia nazwę arkusza 
    //    /// </summary>
    //    /// <param name="nazwa">Nowa nazwa arkusza</param>
    //    private void ZmienNazweArkusza(string nazwa)
    //    {
    //        ActiveWorkSheet.Name = nazwa;
    //    }

    //    /// <summary>
    //    /// Ustawia aktywny arkusz na jeden z istniejących. Jeśli nie istnieje taki arkusz, to dodaje nowy
    //    /// </summary>
    //    /// <param name="nazwa"></param>
    //    private void ZmienAktywnyArkusz(string nazwa)
    //    {
    //        try
    //        {
    //            ActiveWorkSheet = (Worksheet) app.Worksheets[nazwa];
    //        }
    //        catch
    //        {
    //            DodajNowyArkusz(nazwa);
    //        }
    //    }

    //    /// <summary>
    //    /// Ustawia aktywny arkusz na jeden z istniejących. Jeśli nie istnieje taki arkusz, to dodaje nowy
    //    /// </summary>
    //    /// <param name="id">Identyfikator arkusza</param>
    //    private void ZmienAktywnyArkusz(int id)
    //    {
    //        try
    //        {
    //            ActiveWorkSheet = (Worksheet) app.Worksheets[id];
    //        }
    //        catch
    //        {
    //            DodajNowyArkusz("Nowy arkusz " + id);
    //        }
    //    }

    //#endregion Podstawowe metody

    //    public void PrzykladoweGenerowanieArkusza(List<String> listaImion)
    //    {
    //        ZmienNazweArkusza("Lista imion");
    //        DodajKomorke(1,1,"Imiona:");
    //        PogrubienieKomorki(1,1);

    //        for (int i = 0; i < listaImion.Count; i++)
    //        {
    //            DodajKomorke(1, i + 2, listaImion[i]);

    //        }
    //        AutoSzerokoscKolumn();
    //        PokazExcela();
    //    } 
    //}
}