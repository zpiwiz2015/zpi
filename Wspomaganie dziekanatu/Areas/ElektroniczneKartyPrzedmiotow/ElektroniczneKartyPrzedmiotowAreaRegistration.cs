﻿using System.Web.Mvc;

namespace Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow
{
    public class ElektroniczneKartyPrzedmiotowAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "ElektroniczneKartyPrzedmiotow";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "ElektroniczneKartyPrzedmiotow_default",
                "ElektroniczneKartyPrzedmiotow/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}