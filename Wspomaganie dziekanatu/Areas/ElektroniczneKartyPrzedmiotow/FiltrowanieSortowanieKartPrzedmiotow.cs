﻿using System.Collections.Generic;
using System.Linq;
using Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Models;

namespace Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow
{
    public class FiltrowanieSortowanieKartPrzedmiotow
    {
        public IEnumerable<KartaPrzedmiotuVM> FiltrujSortuj(IEnumerable<KartaPrzedmiotuVM> dane, string sortOrder, string searchString)
        {
            if (!string.IsNullOrEmpty(searchString))
            {
                dane = dane.Where(
                    d => d.KodPrzedmiotu.ToLower().Contains(searchString.ToLower())
                        || d.NazwaPrzedmiotuPOL.ToLower().Contains(searchString.ToLower())
                        || d.ImieOpiekuna.ToLower().Contains(searchString.ToLower())
                        || d.NazwiskoOpiekuna.ToLower().Contains(searchString.ToLower())
                        || d.Status.ToLower().Contains(searchString.ToLower())
                        || d.DataZaakceptowania.ToString().Contains(searchString)
                    );
            }// if

            switch(sortOrder)
            {
                case "opiekun":
                    dane = dane
                        .OrderBy(d => d.NazwiskoOpiekuna)
                        .ThenBy(d => d.ImieOpiekuna);
                    break;
                case "opiekun_desc":
                    dane = dane
                        .OrderByDescending(d => d.NazwiskoOpiekuna)
                        .ThenByDescending(d => d.ImieOpiekuna);
                    break;
                case "jezyk":
                    dane = dane
                        .OrderBy(d => d.WersjaJezykowa)
                        .ThenBy(d => d.WersjaJezykowa);
                    break;
                case "jezyk_desc":
                    dane = dane
                        .OrderByDescending(d => d.WersjaJezykowa)
                        .ThenByDescending(d => d.WersjaJezykowa);
                    break;
                case "dataUtworzenia":
                    dane = dane
                        .OrderBy(d => d.DataUtworzenia);
                    break;
                case "dataUtworzenia_desc":
                    dane = dane
                        .OrderByDescending(d => d.DataUtworzenia);
                    break;
                case "dataZaakceptowania":
                    dane = dane
                        .OrderBy(d => d.DataZaakceptowania);
                    break;
                case "datadataZaakceptowania_desc":
                    dane = dane
                        .OrderByDescending(d => d.DataZaakceptowania);
                    break;
                case "kod":
                    dane = dane
                        .OrderBy(d => d.KodPrzedmiotu);
                    break;
                case "kod_desc":
                    dane = dane
                        .OrderByDescending(d => d.KodPrzedmiotu);
                    break;
                case "status":
                    dane = dane
                        .OrderBy(d => d.Status);
                    break;
                case "status_desc":
                    dane = dane
                        .OrderByDescending(d => d.Status);
                    break;
                case "nazwa_desc":
                    dane = dane
                        .OrderByDescending(d => d.NazwaPrzedmiotuPOL);
                    break;
                default:
                    dane = dane
                        .OrderBy(d => d.NazwaPrzedmiotuPOL);
                    break;
            }// switch

            return dane;
        }// FiltrujSortuj()

    }// FiltrowanieSortowanieKartPrzedmiotow
}// namespace