﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Extensions
{
    // Na podstawie treści udostępnionych przez Jakuba Gutkowskiego, na blogu:
    // http://blog.gutek.pl/2011/05/12/edycja-zagniezdzonych-list-w-mvc-3-czesc-1/

    public static class DictionaryExtensions
    {
        public static IDictionary<string, object> MergeAttributes(this IDictionary<string, object> @this, IDictionary<string, object> toMerge, bool replace = false)
        {
            foreach (var pair in toMerge)
            {
                string key = Convert.ToString(pair.Key, CultureInfo.InvariantCulture);
                string val = Convert.ToString(pair.Value, CultureInfo.InvariantCulture);

                if (string.IsNullOrEmpty(key))
                {
                    throw new ArgumentException("Key cannot be null or empty", "key");
                }
                if (replace || !@this.ContainsKey(key))
                {
                    @this[key] = val;
                }
                else if (@this.ContainsKey(key))
                {
                    @this[key] = @this[key] + " " + val;
                }
            }// foreach

            return @this;
        }// MergeAttributes()

    }// DictionaryExtensions
}// namespace