﻿
namespace Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Extensions
{
    // Na podstawie treści udostępnionych przez Jakuba Gutkowskiego, na blogu:
    // http://blog.gutek.pl/2011/05/12/edycja-zagniezdzonych-list-w-mvc-3-czesc-1/

    public static class ObjectExtensions
    {
        public static TDestination Map<TDestination>(this object source) where TDestination : class
        {
            var copy = source.ToJSon();

            return copy.FromJSon<TDestination>();
        }// Map()

    }// ObjectExtensions
}// namespace