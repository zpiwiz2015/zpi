﻿using System.Web.Script.Serialization;

namespace Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Extensions
{
    // Na podstawie treści udostępnionych przez Jakuba Gutkowskiego, na blogu:
    // http://blog.gutek.pl/2011/05/12/edycja-zagniezdzonych-list-w-mvc-3-czesc-1/

    public static class StringExtensions
    {
        public static string ToJSon<T>(this T source) where T : class
        {
            return new JavaScriptSerializer().Serialize(source);
        }// ToJSon()

        public static T FromJSon<T>(this string source) where T : class
        {
            return new JavaScriptSerializer().Deserialize<T>(source);
        }// FromJSon()

    }// StringExtensions
}// namespace