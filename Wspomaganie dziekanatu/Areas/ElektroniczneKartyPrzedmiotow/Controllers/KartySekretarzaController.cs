﻿using PagedList;
using System;
using System.Linq;
using System.Web.Mvc;
using Wspomaganie_dziekanatu.Models;

namespace Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Controllers
{
    public partial class KartySekretarzaController : Controller
    {
        private ZpiDbCtx dbo = new ZpiDbCtx();
        PobieranieDanych pd = new PobieranieDanych();

        // przeglądanie listy kart przedmiotów
        [Authorize(Roles = "Sekretarz")]
        public virtual ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            FiltrowanieSortowanieKartPrzedmiotow fskp = new FiltrowanieSortowanieKartPrzedmiotow();

            ViewBag.CurrentSort = sortOrder;
            ViewBag.OpiekunSort = sortOrder == "opiekun" ? "opiekun_desc" : "opiekun";
            ViewBag.JezykSort = sortOrder == "jezyk" ? "jezyk_desc" : "jezyk";
            ViewBag.DataSort = sortOrder == "dataUtworzenia" ? "dataUtworzenia_desc" : "dataUtworzenia";
            ViewBag.KodSort = sortOrder == "kod" ? "kod_desc" : "kod";
            ViewBag.NazwaSort = String.IsNullOrEmpty(sortOrder) ? "nazwa_desc" : "";
            ViewBag.StatusSort = sortOrder == "status" ? "status_desc" : "status";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var dane = pd.PobierzDaneKartyPrzedmiotu()
                .Where(d => d.StatusId == 5 // do sprawdzenia
                );

            var wybraneDane = fskp.FiltrujSortuj(dane, sortOrder, searchString);

            int pageSize = 20;
            int pageNumber = (page ?? 1);

            return View(wybraneDane.ToPagedList(pageNumber, pageSize));
        }// Index()

        public virtual ActionResult Edit(int id)
        {
            var dane = pd.PobierzDaneKartyPrzedmiotu();

            return View(dane.FirstOrDefault(d => d.Id == id));
        }// Edit()

        [Authorize(Roles = "Sekretarz")]
        // widok szczegółów karty przedmiotu
        public virtual ActionResult Details(int id)
        {
            var dane = pd.PobierzDaneKartyPrzedmiotu();

            return View(dane.FirstOrDefault(d => d.Id == id));
        }// Details()

        [Authorize(Roles = "Sekretarz")]
        public virtual ActionResult Send(int id)
        {
            using (var db = new ZpiDbCtx())
            {
                var karta = db.KartaPrzedmiotu.First(e => e.KartaPrzedmiotuId == id);
                karta.StatusKartyId = 4;
                db.SaveChanges();
            }

            return RedirectToAction("Index");
        }

    }// KartySekretarzaController
}// namespace