﻿using System.Web.Mvc;
using Wspomaganie_dziekanatu.Models;

namespace Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Controllers
{
    public partial class TabelaPunktowGodzinController : Controller
    {
        private ZpiDbCtx dbo = new ZpiDbCtx();
        private PobieranieDanych pd = new PobieranieDanych();

        protected override void Dispose(bool disposing)
        {
            dbo.Dispose();
            base.Dispose(disposing);
        }

        [ChildActionOnly]
        public virtual ActionResult _Index(int id)
        {
            var dane = pd.PobierzDaneTabeliPunktowGodzin(id);

            return View(dane);
        }

        [ChildActionOnly]
        public virtual ActionResult _Create()
        {
            ViewBag.FormaZaliczenia = new SelectList(dbo.FormaZaliczenia, "Id", "Wartosc");

            return View();
        }
    }
}