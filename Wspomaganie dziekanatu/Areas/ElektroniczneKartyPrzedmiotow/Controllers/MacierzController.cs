﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Models;
using Wspomaganie_dziekanatu.Models;

namespace Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Controllers
{
    public partial class MacierzController : Controller
    {
        public virtual ActionResult _Index(int id)
        {
            using (var dbo = new ZpiDbCtx())
            {
                MacierzVM model = new MacierzVM();
                var kartaPrzedmiotu = dbo.KartaPrzedmiotu.FirstOrDefault(k => k.KartaPrzedmiotuId == id);

                model.NazwaPrzedmiotu = kartaPrzedmiotu.NazwaPl;

                try
                {
                    model.Specjalnosc = dbo.ProgramKierunkuKP.FirstOrDefault
                        (p => p.KartaPrzedmiotuId == kartaPrzedmiotu.KartaPrzedmiotuId).Specjalnosc.Wartosc;
                }
                catch
                {
                    model.Specjalnosc = "";
                }
                var programKierunkuKP = dbo.ProgramKierunkuKP.FirstOrDefault
                    (p => p.KartaPrzedmiotuId == kartaPrzedmiotu.KartaPrzedmiotuId);

                if (programKierunkuKP != null)
                {
                    model.StopienStudiow = programKierunkuKP.StopienStudiow;
                }
                model.KierunekStudiow = dbo.KierunekStudiow.FirstOrDefault
                    (k => k.ProgramKierunkuKP.Any(p => p.KartaPrzedmiotuId == kartaPrzedmiotu.KartaPrzedmiotuId));

                model.TypEfektu = dbo.TypEfektu.ToList();

                if (model.KierunekStudiow == null)
                {
                    model.KierunekStudiow = new KierunekStudiow();
                    model.KierunekStudiow.Wartosc = "";
                    model.KierunekStudiow.Kod = "";
                }

                model.MPkeki = new List<List<KierunkowyEfektKsztalcenia>>();
                model.MPcele = new List<List<CelPrzedmiotu>>();
                model.MPtresci = new List<List<TrescProgramowa>>();
                model.MPnarzedzia = new List<List<Narzedzie>>();

                model.PEKiW = dbo.PrzedmiotowyEfektKsztalcenia.Where
                    (p => p.MacierzPowiazan.Any(m => m.KartaPrzedmiotuId == kartaPrzedmiotu.KartaPrzedmiotuId)
                        && p.TypEfektuId == 1).ToList();
                model.PEKiU = dbo.PrzedmiotowyEfektKsztalcenia.Where
                    (p => p.MacierzPowiazan.Any(m => m.KartaPrzedmiotuId == kartaPrzedmiotu.KartaPrzedmiotuId)
                        && p.TypEfektuId == 2).ToList();
                model.PEKiK = dbo.PrzedmiotowyEfektKsztalcenia.Where
                    (p => p.MacierzPowiazan.Any(m => m.KartaPrzedmiotuId == kartaPrzedmiotu.KartaPrzedmiotuId)
                        && p.TypEfektuId == 3).ToList();
                model.PEKi = new List<PrzedmiotowyEfektKsztalcenia>();
                model.PEKi.AddRange(model.PEKiW);
                model.PEKi.AddRange(model.PEKiU);
                model.PEKi.AddRange(model.PEKiK);

                foreach (var item in model.PEKi)
                {
                    model.MPkeki.Add(dbo.KierunkowyEfektKsztalcenia.Where(k => k.PEKSpelniaKEK.Any(m => m.PekId == item.PekId)).ToList());
                    model.MPcele.Add(dbo.CelPrzedmiotu.Where(c => c.CelMP.Any(m => m.PekId == item.PekId)).ToList());
                    model.MPtresci.Add(dbo.TrescProgramowa.Where(t => t.TrescMP.Any(m => m.PekId == item.PekId)).ToList());
                    model.MPnarzedzia.Add(dbo.Narzedzie.Where(n => n.NarzedzieMP.Any(m => m.PekId == item.PekId)).ToList());
                }
                model.TypPrzedmiotu = dbo.TypPrzedmiotu.ToList();
                return View(model);
            }
        }

        [ChildActionOnly]
        public virtual ActionResult _Create()
        {
            return View();
        }

    }// MacierzController
}// namespace