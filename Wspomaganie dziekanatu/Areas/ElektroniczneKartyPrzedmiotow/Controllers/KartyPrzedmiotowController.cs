﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Models;
using Wspomaganie_dziekanatu.Models;

namespace Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Controllers
{
    public partial class KartyPrzedmiotowController : BaseController
    {
        private ZpiDbCtx dbo = new ZpiDbCtx();
        PobieranieDanych pd = new PobieranieDanych();

        // przeglądanie listy kart przedmiotów
        public virtual ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            FiltrowanieSortowanieKartPrzedmiotow fskp = new FiltrowanieSortowanieKartPrzedmiotow();

            ViewBag.CurrentSort = sortOrder;
            ViewBag.OpiekunSort = sortOrder == "opiekun" ? "opiekun_desc" : "opiekun";
            ViewBag.JezykSort = sortOrder == "jezyk" ? "jezyk_desc" : "jezyk";
            ViewBag.DataSort = sortOrder == "dataZaakceptowania" ? "dataZaakceptowania_desc" : "dataZaakceptowania";
            ViewBag.KodSort = sortOrder == "kod" ? "kod_desc" : "kod";
            ViewBag.NazwaSort = String.IsNullOrEmpty(sortOrder) ? "nazwa_desc" : "";
            ViewBag.StatusSort = sortOrder == "status" ? "status_desc" : "status";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var dane = pd.PobierzDaneKartyPrzedmiotu()
                .Where(d => d.StatusId == 1 // aktywna
                    || d.StatusId == 3      // archiwalna
                );

            var wybraneDane = fskp.FiltrujSortuj(dane, sortOrder, searchString);

            int pageSize = 20;
            int pageNumber = (page ?? 1);

            return View(wybraneDane.ToPagedList(pageNumber, pageSize));
        }// Index()



        // widok szczegółów karty przedmiotu
        public virtual ActionResult Details(int id)
        {
            var dane = pd.PobierzDaneKartyPrzedmiotu();

            return View(dane.FirstOrDefault(d => d.Id == id));
        }// Details()

        // widok tworzenia karty przedmiotów
        [HttpGet]
        [Authorize(Roles = "Dydaktyk")]
        public virtual ActionResult Create()
        {
            ViewBag.WersjaJezykowa = new SelectList(dbo.Jezyk, "id", "Wartosc");
            ViewBag.FormaStudiow = new SelectList(dbo.FormaStudiow, "Id", "Wartosc");
            ViewBag.RodzajPrzedmiotu = new SelectList(dbo.RodzajPrzedmiotu, "Id", "Wartosc");
            ViewBag.StopienStudiow = new SelectList(dbo.StopienStudiow, "Id", "Wartosc");
            ViewBag.Wydzial = new SelectList(dbo.Wydzial, "Id", "Wartosc");
            ViewBag.KierunekStudiow = new SelectList(dbo.KierunekStudiow, "Id", "Wartosc");
            ViewBag.Specjalnosc = new SelectList(dbo.Specjalnosc, "Id", "Wartosc");

            var grupaKursow = new List<GrupaKursowVM>();
            grupaKursow.Add(new GrupaKursowVM() { Id = 0, Wartosc = "nie" });
            grupaKursow.Add(new GrupaKursowVM() { Id = 1, Wartosc = "tak" });
            ViewBag.GrupaKursow = grupaKursow;

            var wyborJezyka = new List<WyborJezykaVM>();
            wyborJezyka.Add(new WyborJezykaVM() { Id = 343, Wartosc = "polska" });
            wyborJezyka.Add(new WyborJezykaVM() { Id = 18, Wartosc = "angielska" });
            ViewBag.WyborJezyka = wyborJezyka;

            return View(new KartaPrzedmiotuVM());
        }// Create()

        [HttpPost]
        public virtual ActionResult Create(KartaPrzedmiotuVM model)
        {
            using (var db = new ZpiDbCtx())
            {
                var kartaPrzedmiotu = new KartaPrzedmiotu
                {
                    KodKursu = model.KodPrzedmiotu,
                    DataUtworzenia = System.DateTime.Today,
                    DataAkceptacji = null,
                    NazwaPl = model.NazwaPrzedmiotuPOL,
                    NazwaAng = model.NazwaPrzedmiotuENG,
                    StatusKartyId = 5,
                    JezykId = 343,
                    PracownikId = Helpers.User.IdDydaktyka(User),
                    CzyGrupaKursow = 0,//model.GrupaKursowId,
                };
                db.KartaPrzedmiotu.Add(kartaPrzedmiotu);
                db.SaveChanges();
                var kartaPrzedmiotuId = db.KartaPrzedmiotu.FirstOrDefault(e => e.KodKursu == model.KodPrzedmiotu).KartaPrzedmiotuId;

                var programKierunkuKP = new ProgramKierunkuKP
                {
                    CzyGrupaKursow = model.GrupaKursowId,
                    KartaPrzedmiotuId = kartaPrzedmiotuId,
                    RodzajPrzedmiotuId = Convert.ToInt32(model.RodzajPrzedmiotu),
                    SpecjanloscId = null,//Convert.ToInt32(model.Specjalnosc),
                    StopienStudiowId = Convert.ToInt32(model.StopienStudiow),
                    FormaStudiowId = Convert.ToInt32(model.FormaStudiow),
                    KierunekStudiowId = Convert.ToInt32(model.KierunekStudiow),
                    WydzialId = Convert.ToInt32(model.Wydzial),
                };
                db.ProgramKierunkuKP.Add(programKierunkuKP);
                db.SaveChanges();

                //foreach (var formaPrzedmiotu in model.)
                //{
                //    var formaPrzedmiotu = new FormaPrzedmiotu
                //    {
                //        TypPrzedmiotuId = model.FormaPrzedmiotu.TypPrzedmiotuId,
                //        ZZU = model.FormaPrzedmiotu.ZZU,
                //        CNPS = model.FormaPrzedmiotu.CNPS,
                //        ECTS = model.FormaPrzedmiotu.ECTS,
                //        CzyKursKoncowy = model.FormaPrzedmiotu.CzyKursKoncowy,
                //        PunktyPrakt = model.FormaPrzedmiotu.PunktyPrakt,
                //        PunktyBK = model.FormaPrzedmiotu.PunktyBK,
                //        FormaZaliczeniaId = model.FormaPrzedmiotu.FormaPrzedmiotuId,
                //        KartaPrzedmiotuId = kartaPrzedmiotuId
                //    };
                //db.FormaPrzedmiotu.Add(formaPrzedmiotu);
                //}
                int nrWymagania = 0;
                foreach (var wymaganieWstepne in model.WymaganiaWstepneList)
                {
                    //var nrWymagania = db.WymaganieWstepne.LastOrDefault(e => e.KartaPrzedmiotuId == kartaPrzedmiotuId).NrWymagania;
                    var wymaganiaWstepne = new WymaganieWstepne
                    {
                        KartaPrzedmiotuId = kartaPrzedmiotuId,
                        NrWymagania = 1 + nrWymagania,
                        OpisWymagania = wymaganieWstepne.Opis,
                        JezykId = 343,
                    };
                    nrWymagania++;
                    db.WymaganieWstepne.Add(wymaganiaWstepne);
                    db.SaveChanges();
                }
                //int nrPek = 1;
                //int nrPekU = 1;
                //int nrPekW = 1;
                //int nrPekK = 1;
                //foreach (var pekW in model.PekiWList)
                //{
                //    var pekW2 = new PrzedmiotowyEfektKsztalcenia
                //    {
                //        Opis = pekW.Opis,
                //        NrPek = Convert.ToString(nrPekW),
                //        JezykId = 343,
                //        TypEfektuId = 1,
                //    };
                //    nrPekW++;
                //    db.PrzedmiotowyEfektKsztalcenia.Add(pekW2);
                //    db.SaveChanges();
                //    var pekWId = db.PrzedmiotowyEfektKsztalcenia.LastOrDefault(e => e.Opis == pekW2.Opis).PekId;
                //    var macierzW = new MacierzPowiazan
                //    {
                //        KartaPrzedmiotuId = kartaPrzedmiotuId,
                //        PekId = pekWId,
                //        NrPEK = Convert.ToString(nrPek)
                //    };
                //    nrPek++;
                //    db.MacierzPowiazan.Add(macierzW);
                //    db.SaveChanges();
                //}
                //foreach (var pekU in model.PekiUList)
                //{
                //    var pekU2 = new PrzedmiotowyEfektKsztalcenia
                //    {
                //        Opis = pekU.Opis,
                //        NrPek = Convert.ToString(nrPekU),
                //        JezykId = 343,
                //        TypEfektuId = 2,
                //    };
                //    nrPekU++;
                //    db.PrzedmiotowyEfektKsztalcenia.Add(pekU2);
                //    db.SaveChanges();
                //    var pekUId = db.PrzedmiotowyEfektKsztalcenia.LastOrDefault(e => e.Opis == pekU2.Opis).PekId;
                //    var macierzU = new MacierzPowiazan
                //    {
                //        KartaPrzedmiotuId = kartaPrzedmiotuId,
                //        PekId = pekUId,
                //        NrPEK = Convert.ToString(nrPek)
                //    };
                //    nrPek++;
                //    db.MacierzPowiazan.Add(macierzU);
                //    db.SaveChanges();
                //}
                //foreach (var pekK in model.PekiKList)
                //{
                //    var pekK2 = new PrzedmiotowyEfektKsztalcenia
                //    {
                //        Opis = pekK.Opis,
                //        NrPek = Convert.ToString(nrPekK),
                //        JezykId = 343,
                //        TypEfektuId = 3,
                //    };
                //    nrPekK++;
                //    db.PrzedmiotowyEfektKsztalcenia.Add(pekK2);
                //    db.SaveChanges();
                //    var pekKId = db.PrzedmiotowyEfektKsztalcenia.LastOrDefault(e => e.Opis == pekK2.Opis).PekId;
                //    var macierzK = new MacierzPowiazan
                //    {
                //        KartaPrzedmiotuId = kartaPrzedmiotuId,
                //        PekId = pekKId,
                //        NrPEK = Convert.ToString(nrPek)
                //    };
                //    nrPek++;
                //    db.MacierzPowiazan.Add(macierzK);
                //    db.SaveChanges();
                //}

                //int nrCelu = 0;
                //foreach (var cel in model.CelePrzedmiotuList)
                //{
                //    //var nrWymagania = db.WymaganieWstepne.LastOrDefault(e => e.KartaPrzedmiotuId == kartaPrzedmiotuId).NrWymagania;
                //    var celePrzedmiotu = new CelPrzedmiotu
                //    {
                //        NrCelu = 1 + nrCelu,
                //        OpisCelu = cel.Opis,
                //        JezykId = 343,
                //    };
                //    db.CelPrzedmiotu.Add(celePrzedmiotu);
                //    db.SaveChanges();
                //    var celPrzedmiotuId = db.CelPrzedmiotu.FirstOrDefault(e => e.OpisCelu == cel.Opis).CelPrzedmiotuId;
                //    var celeMP = new CelMP
                //    {
                //        KartaPrzedmiotuId = kartaPrzedmiotuId,
                //        PekId = 1, //ZMIENIC
                //        CelPrzedmiotuId = celPrzedmiotuId,
                //    };
                //    nrCelu++;
                //    db.CelMP.Add(celeMP);
                //    db.SaveChanges();
                //}
                //int nrNarzedzia = 0;
                //foreach (var narzedzie in model.NarzedziaDydaktyczneList)
                //{
                //    //var nrWymagania = db.WymaganieWstepne.LastOrDefault(e => e.KartaPrzedmiotuId == kartaPrzedmiotuId).NrWymagania;
                //    var narzedzia = new Narzedzie
                //    {
                //        KartaPrzedmiotuId = kartaPrzedmiotuId,
                //        NrNarzedzia = 1 + nrNarzedzia,
                //        OpisNarzedzia = narzedzie.Opis,
                //        JezykId = 343,
                //    };
                //    db.Narzedzie.Add(narzedzia);
                //    db.SaveChanges();
                //    var narzedzieId = db.Narzedzie.FirstOrDefault(e => e.OpisNarzedzia == narzedzie.Opis).NarzedzieId;
                //    var narzedziaMP = new NarzedzieMP
                //    {
                //        KartaPrzedmiotuId = kartaPrzedmiotuId,
                //        PekId = 1, //ZMIENIC
                //        NarzedzieId = narzedzieId,
                //    };
                //    nrNarzedzia++;
                //    db.NarzedzieMP.Add(narzedziaMP);
                //    db.SaveChanges();
                //}
                //Literatura podstawowa
                //foreach (var literatura in model.LiteraturaUList)
                //{
                //    var pozycjaLiteratury = new PozycjaLiteratury
                //    {

                //    };
                //    db.PozycjaLiteratury.Add(pozycjaLiteratury);
                //    db.SaveChanges();
                //    var pozycjaLiteraturyId = db.PozycjaLiteratury.LastOrDefault().PozycjaLiteraturyId;

                //    var wydawca = new Wydawca
                //    {
                //        Wartosc = literatura.Wydawnictwo,
                //    };
                //    db.Wydawca.Add(wydawca);
                //    db.SaveChanges();
                //    var wydawcaId = db.Wydawca.LastOrDefault().Id;

                //    var literatura2 = new Literatura
                //    {
                //        WydawcaId = wydawcaId,
                //        Tytul = literatura.Tytul,
                //        RokWydania = literatura.RokWydania,
                //    };
                //    db.Literatura.Add(literatura2);
                //    db.SaveChanges();
                //    //Literatura uzupełniająca
                //    foreach (var literaturaU in model.LiteraturaPList)
                //    {
                //        var pozycjaLiteraturyU = new PozycjaLiteratury
                //        {

                //        };
                //        db.PozycjaLiteratury.Add(pozycjaLiteratury);
                //        db.SaveChanges();
                //        var pozycjaLiteraturyUId = db.PozycjaLiteratury.LastOrDefault().PozycjaLiteraturyId;

                //        var wydawcaU = new Wydawca
                //        {
                //            Wartosc = literatura.Wydawnictwo,
                //        };
                //        db.Wydawca.Add(wydawca);
                //        db.SaveChanges();
                //        var wydawcaIdU = db.Wydawca.LastOrDefault().Id;

                //        var literaturaU2 = new Literatura
                //        {
                //            WydawcaId = wydawcaId,
                //            Tytul = literatura.Tytul,
                //            RokWydania = literatura.RokWydania,
                //        };
                //        db.Literatura.Add(literaturaU2);
                //        db.SaveChanges();
                    //}
                //}
                return RedirectToAction("Index");
            }
        }



        // dodanie nowego wymagania wstępnego
        
        public virtual ActionResult CreateRowForWymaganieWstepne(string prefix)
        {
            ViewData[NestedPrefix] = prefix;
            return DynamicView(Views._CreateRowWymaganieWstepne, new WymaganieWstepneVM());
        }// CreateRowForWymaganieWstepne()

        // dodanie nowego celu przedmiotu
        
        public virtual ActionResult CreateRowForCelPrzedmiotu(string prefix)
        {
            ViewData[NestedPrefix] = prefix;
            return DynamicView(Views._CreateRowCelPrzedmiotu, new CelPrzedmiotuVM());
        }// CreateRowForCelPrzedmiotu()

        // dodanie nowego narzedzia dydaktycznego
        
        public virtual ActionResult CreateRowForNarzedzieDydaktyczne(string prefix)
        {
            ViewData[NestedPrefix] = prefix;
            return DynamicView(Views._CreateRowNarzedzieDydaktyczne, new NarzedzieDydaktyczneVM());
        }// CreateRowForNarzedzieDydaktyczne()

        // dodanie nowej pozycji literatury
        
        public virtual ActionResult CreateRowForLiteraturaP(string prefix)
        {
            ViewData[NestedPrefix] = prefix;
            return DynamicView(Views._CreateRowLiteraturaP, new LiteraturaVM());
        }// CreateRowForLiteratura()

        public virtual ActionResult CreateRowForLiteraturaU(string prefix)
        {
            ViewData[NestedPrefix] = prefix;
            return DynamicView(Views._CreateRowLiteraturaU, new LiteraturaVM());
        }// CreateRowForLiteratura()

        // dodanie nwej pozycji autora dla pozycji literatury
       
        public virtual ActionResult CreateRowForAutor(string prefix)
        {
            ViewData[NestedPrefix] = prefix;
            return DynamicView(Views._CreateRowAutor, new AutorVM());
        }// CreateRowForAutor()

        public virtual ActionResult CreateRowForPekW(string prefix)
        {
            ViewData[NestedPrefix] = prefix;
            return DynamicView(Views._CreateRowPekW, new PekVM());
        }// CreateRowForPekW()

        public virtual ActionResult CreateRowForPekU(string prefix)
        {
            ViewData[NestedPrefix] = prefix;
            return DynamicView(Views._CreateRowPekU, new PekVM());
        }// CreateRowForPekU()

        public virtual ActionResult CreateRowForPekK(string prefix)
        {
            ViewData[NestedPrefix] = prefix;
            return DynamicView(Views._CreateRowPekK, new PekVM());
        }// CreateRowForPekK()

        public virtual ActionResult CreateRowForTrescW(string prefix)
        {
            ViewData[NestedPrefix] = prefix;
            return DynamicView(Views._CreateRowTrescW, new TrescProgramowaVM());
        }// CreateRowForTrescW()

        public virtual ActionResult CreateRowForTrescC(string prefix)
        {
            ViewData[NestedPrefix] = prefix;
            return DynamicView(Views._CreateRowTrescC, new TrescProgramowaVM());
        }// CreateRowForTrescC()

        public virtual ActionResult CreateRowForTrescL(string prefix)
        {
            ViewData[NestedPrefix] = prefix;
            return DynamicView(Views._CreateRowTrescL, new TrescProgramowaVM());
        }// CreateRowForTrescL()

        public virtual ActionResult CreateRowForTrescP(string prefix)
        {
            ViewData[NestedPrefix] = prefix;
            return DynamicView(Views._CreateRowTrescP, new TrescProgramowaVM());
        }// CreateRowForTrescP()

        public virtual ActionResult CreateRowForTrescS(string prefix)
        {
            ViewData[NestedPrefix] = prefix;
            return DynamicView(Views._CreateRowTrescS, new TrescProgramowaVM());
        }// CreateRowForTrescS()
    }// KartyPrzedmiotowController
}// namespace
