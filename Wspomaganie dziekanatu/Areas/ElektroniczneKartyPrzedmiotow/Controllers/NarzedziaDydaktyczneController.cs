﻿using System.Web.Mvc;
using Wspomaganie_dziekanatu.Models;

namespace Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Controllers
{
    public partial class NarzedziaDydaktyczneController : Controller
    {
        private ZpiDbCtx dbo = new ZpiDbCtx();
        private PobieranieDanych pd = new PobieranieDanych();
        protected override void Dispose(bool disposing)
        {
            dbo.Dispose();
            base.Dispose(disposing);
        }

        [ChildActionOnly]
        public virtual ActionResult _Index(int id)
        {
            var dane = pd.PobierzDaneNarzedzi(id);

            return View(dane);
        }
    }
}