﻿using Rotativa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Models;
using Wspomaganie_dziekanatu.Models;

namespace Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Controllers
{
    public partial class KartaDoPdfController : Controller
    {
        // GET: ElektroniczneKartyPrzedmiotow/KartaDoPdf
        public virtual ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }

            using (var db = new ZpiDbCtx())
            {
                /*SEKCJA NR 1 - KARTA PRZEDMIOTU*/
                var kartaPrzedmiotu = db.KartaPrzedmiotu.FirstOrDefault(k => k.KartaPrzedmiotuId == id);
                if (kartaPrzedmiotu == null)
                {
                    return HttpNotFound();
                }
                KartaDoPdfModel model = new KartaDoPdfModel();

                model.KodPrzedmiotu = kartaPrzedmiotu.KodKursu;
                model.NazwaPrzedmiotuPL = kartaPrzedmiotu.NazwaPl;
                model.NazwaPrzedmiotuEN = kartaPrzedmiotu.NazwaAng;
                model.GrupaKursow = kartaPrzedmiotu.CzyGrupaKursow;

                try
                {
                    model.Wydzial = db.ProgramKierunkuKP.FirstOrDefault
                        (p => p.KartaPrzedmiotuId == kartaPrzedmiotu.KartaPrzedmiotuId).Wydzial.Wartosc;
                }
                catch
                {
                    model.Wydzial = "";
                }

                try
                {
                    model.Specjalnosc = db.ProgramKierunkuKP.FirstOrDefault
                        (p => p.KartaPrzedmiotuId == kartaPrzedmiotu.KartaPrzedmiotuId).Specjalnosc.Wartosc;
                }
                catch
                {
                    model.Specjalnosc = "";
                }

                model.KierunekStudiow = db.KierunekStudiow.FirstOrDefault
                    (k => k.ProgramKierunkuKP.Any(p => p.KartaPrzedmiotuId == kartaPrzedmiotu.KartaPrzedmiotuId));

                if (model.KierunekStudiow == null)
                {
                    model.KierunekStudiow = new KierunekStudiow();
                    model.KierunekStudiow.Wartosc = "";
                    model.KierunekStudiow.Kod = "";
                }

                var programKierunkuKP = db.ProgramKierunkuKP.FirstOrDefault
                    (p => p.KartaPrzedmiotuId == kartaPrzedmiotu.KartaPrzedmiotuId);

                if (programKierunkuKP != null)
                {
                    model.RodzajPrzedmiotu = programKierunkuKP.RodzajPrzedmiotu.Wartosc;
                    model.StopienStudiow = programKierunkuKP.StopienStudiow;
                    model.FormaStudiow = programKierunkuKP.FormaStudiow.Wartosc;
                }

                /*SEKCJA NR 2*/
                /*FORMA PRZEDMIOTU W*/
                var wformaPrzedmiotu = db.FormaPrzedmiotu.FirstOrDefault
                    (f => f.KartaPrzedmiotuId == kartaPrzedmiotu.KartaPrzedmiotuId && f.TypPrzedmiotuId == 1);
                if (wformaPrzedmiotu != null)
                {
                    model.wZZU = wformaPrzedmiotu.ZZU.ToString();
                    model.wCNPS = wformaPrzedmiotu.CNPS.ToString();
                    model.wECTS = wformaPrzedmiotu.ECTS.ToString();
                    if (wformaPrzedmiotu.CzyKursKoncowy != null)
                    {
                        model.wKoncowy = Convert.ToBoolean(wformaPrzedmiotu.CzyKursKoncowy.Value);
                    }
                    model.wP = wformaPrzedmiotu.PunktyPrakt;
                    model.wBK = wformaPrzedmiotu.PunktyBK;
                    model.wZaliczenie = wformaPrzedmiotu.FormaZaliczenia.Wartosc;
                }

                /*FORMA PRZEDMIOTU C*/
                var cformaPrzedmiotu = db.FormaPrzedmiotu.FirstOrDefault
                    (f => f.KartaPrzedmiotuId == kartaPrzedmiotu.KartaPrzedmiotuId && f.TypPrzedmiotuId == 2);
                if (cformaPrzedmiotu != null)
                {
                    model.cZZU = cformaPrzedmiotu.ZZU.ToString();
                    model.cCNPS = cformaPrzedmiotu.CNPS.ToString();
                    model.cECTS = cformaPrzedmiotu.ECTS.ToString();
                    if (cformaPrzedmiotu.CzyKursKoncowy != null)
                    {
                        model.cKoncowy = Convert.ToBoolean(cformaPrzedmiotu.CzyKursKoncowy.Value);
                    }
                    model.cP = cformaPrzedmiotu.PunktyPrakt;
                    model.cBK = cformaPrzedmiotu.PunktyBK;
                    model.cZaliczenie = cformaPrzedmiotu.FormaZaliczenia.Wartosc;
                }

                /*FORMA PRZEDMIOTU L*/
                var lformaPrzedmiotu = db.FormaPrzedmiotu.FirstOrDefault
                    (f => f.KartaPrzedmiotuId == kartaPrzedmiotu.KartaPrzedmiotuId && f.TypPrzedmiotuId == 3);
                if (lformaPrzedmiotu != null)
                {
                    model.lZZU = lformaPrzedmiotu.ZZU.ToString();
                    model.lCNPS = lformaPrzedmiotu.CNPS.ToString();
                    model.lECTS = lformaPrzedmiotu.ECTS.ToString();
                    if (lformaPrzedmiotu.CzyKursKoncowy != null)
                    {
                        model.lKoncowy = Convert.ToBoolean(lformaPrzedmiotu.CzyKursKoncowy.Value);
                    }
                    model.lP = lformaPrzedmiotu.PunktyPrakt;
                    model.lBK = lformaPrzedmiotu.PunktyBK;
                    model.lZaliczenie = lformaPrzedmiotu.FormaZaliczenia.Wartosc;
                }

                /*FORMA PRZEDMIOTU P*/
                var pformaPrzedmiotu = db.FormaPrzedmiotu.FirstOrDefault
                    (f => f.KartaPrzedmiotuId == kartaPrzedmiotu.KartaPrzedmiotuId && f.TypPrzedmiotuId == 4);
                if (pformaPrzedmiotu != null)
                {
                    model.pZZU = pformaPrzedmiotu.ZZU.ToString();
                    model.pCNPS = pformaPrzedmiotu.CNPS.ToString();
                    model.pECTS = pformaPrzedmiotu.ECTS.ToString();
                    if (pformaPrzedmiotu.CzyKursKoncowy != null)
                    {
                        model.pKoncowy = Convert.ToBoolean(pformaPrzedmiotu.CzyKursKoncowy.Value);
                    }
                    model.pP = pformaPrzedmiotu.PunktyPrakt;
                    model.pBK = pformaPrzedmiotu.PunktyBK;
                    model.pZaliczenie = pformaPrzedmiotu.FormaZaliczenia.Wartosc;
                }

                /*FORMA PRZEDMIOTU S*/
                var sformaPrzedmiotu = db.FormaPrzedmiotu.FirstOrDefault
                    (f => f.KartaPrzedmiotuId == kartaPrzedmiotu.KartaPrzedmiotuId && f.TypPrzedmiotuId == 5);
                if (sformaPrzedmiotu != null)
                {
                    model.sZZU = sformaPrzedmiotu.ZZU.ToString();
                    model.sCNPS = sformaPrzedmiotu.CNPS.ToString();
                    model.sECTS = sformaPrzedmiotu.ECTS.ToString();
                    if (sformaPrzedmiotu.CzyKursKoncowy != null)
                    {
                        model.sKoncowy = Convert.ToBoolean(sformaPrzedmiotu.CzyKursKoncowy.Value);
                    }
                    model.sP = sformaPrzedmiotu.PunktyPrakt;
                    model.sBK = sformaPrzedmiotu.PunktyBK;
                    model.sZaliczenie = sformaPrzedmiotu.FormaZaliczenia.Wartosc;
                }

                /*SEKCJA NR 3 - WYMAGANIA WSTĘPNE*/
                model.WymaganiaWstepne = (from WymaganieWstepne in db.WymaganieWstepne
                                          where WymaganieWstepne.KartaPrzedmiotuId == kartaPrzedmiotu.KartaPrzedmiotuId
                                          select WymaganieWstepne).ToList();

                /*SEKCJA NR 4 - CELE*/
                model.CelePrzedmiotu = db.CelPrzedmiotu.Where
                    (c => c.CelMP.Any(m => m.KartaPrzedmiotuId == kartaPrzedmiotu.KartaPrzedmiotuId)).ToList();

                /*SEKCJA NR 5 - PEKI*/
                model.PEKiW = db.PrzedmiotowyEfektKsztalcenia.Where
                    (p => p.MacierzPowiazan.Any(m => m.KartaPrzedmiotuId == kartaPrzedmiotu.KartaPrzedmiotuId)
                        && p.TypEfektuId == 1).ToList();
                model.PEKiU = db.PrzedmiotowyEfektKsztalcenia.Where
                    (p => p.MacierzPowiazan.Any(m => m.KartaPrzedmiotuId == kartaPrzedmiotu.KartaPrzedmiotuId)
                        && p.TypEfektuId == 2).ToList();
                model.PEKiK = db.PrzedmiotowyEfektKsztalcenia.Where
                    (p => p.MacierzPowiazan.Any(m => m.KartaPrzedmiotuId == kartaPrzedmiotu.KartaPrzedmiotuId)
                        && p.TypEfektuId == 3).ToList();
                model.PEKi = new List<PrzedmiotowyEfektKsztalcenia>();
                model.PEKi.AddRange(model.PEKiW);
                model.PEKi.AddRange(model.PEKiU);
                model.PEKi.AddRange(model.PEKiK);

                /*SEKCJA NR 6 - TREŚCI PROGRAMOWE*/
                model.TrescWyklady = db.TrescProgramowa.Where
                    (t => t.TrescMP.Any(m => m.KartaPrzedmiotuId ==
                        kartaPrzedmiotu.KartaPrzedmiotuId) && t.TypPrzedmiotuId == 1).ToList();
                model.TrescCwiczenia = db.TrescProgramowa.Where
                    (t => t.TrescMP.Any(m => m.KartaPrzedmiotuId ==
                        kartaPrzedmiotu.KartaPrzedmiotuId) && t.TypPrzedmiotuId == 2).ToList();
                model.TrescLaboratorium = db.TrescProgramowa.Where
                    (t => t.TrescMP.Any(m => m.KartaPrzedmiotuId ==
                        kartaPrzedmiotu.KartaPrzedmiotuId) && t.TypPrzedmiotuId == 3).ToList();
                model.TrescProjekt = db.TrescProgramowa.Where
                    (t => t.TrescMP.Any(m => m.KartaPrzedmiotuId ==
                        kartaPrzedmiotu.KartaPrzedmiotuId) && t.TypPrzedmiotuId == 4).ToList();
                model.TrescSeminarium = db.TrescProgramowa.Where
                    (t => t.TrescMP.Any(m => m.KartaPrzedmiotuId ==
                        kartaPrzedmiotu.KartaPrzedmiotuId) && t.TypPrzedmiotuId == 5).ToList();

                /*SEKCJA NR 7 - NARZĘDZIA DYDAKTYCZNE*/
                model.Narzedzia = db.Narzedzie.Where
                    (n => n.KartaPrzedmiotuId == kartaPrzedmiotu.KartaPrzedmiotuId).ToList();

                /*SEKCJA NR 8 - OCENA OSIĄGNIĘCIA PRZEDMIOTOWYCH EFEKTÓW KSZTAŁCENIA*/
                model.OcenyPEKF = db.OcenaOsiagnieciaPekow.Where(o => o.KartaPrzedmiotuId ==
                    kartaPrzedmiotu.KartaPrzedmiotuId && o.FormaOcenyId == 1).ToList();
                model.OcenyPEKP = db.OcenaOsiagnieciaPekow.Where(o => o.KartaPrzedmiotuId ==
                    kartaPrzedmiotu.KartaPrzedmiotuId && o.FormaOcenyId == 2).ToList();

                var pekOcenyF = db.PrzedmiotowyEfektKsztalcenia.Where
                    (p => p.OcenaOsiagnieciaPekow.Any(o => o.KartaPrzedmiotuId ==
                        kartaPrzedmiotu.KartaPrzedmiotuId && o.FormaOcenyId == 1));
                var pekOcenyP = db.PrzedmiotowyEfektKsztalcenia.Where
                    (p => p.OcenaOsiagnieciaPekow.Any(o => o.KartaPrzedmiotuId ==
                        kartaPrzedmiotu.KartaPrzedmiotuId && o.FormaOcenyId == 2));

                List<List<PrzedmiotowyEfektKsztalcenia>> ocenyPekiF = new List<List<PrzedmiotowyEfektKsztalcenia>>();
                List<List<PrzedmiotowyEfektKsztalcenia>> ocenyPekiP = new List<List<PrzedmiotowyEfektKsztalcenia>>();

                model.TypEfektu = db.TypEfektu.ToList();

                int i = 1;
                foreach (var item in pekOcenyF)
                {
                    ocenyPekiF.Add(db.PrzedmiotowyEfektKsztalcenia.Where
                        (p => p.OcenaOsiagnieciaPekow.Any
                            (o => o.KartaPrzedmiotuId == kartaPrzedmiotu.KartaPrzedmiotuId
                                && o.FormaOcenyId == 1 && o.NrOceny == i)).ToList());
                    i++;
                }

                i = 1;

                foreach (var item in pekOcenyP)
                {
                    ocenyPekiP.Add(db.PrzedmiotowyEfektKsztalcenia.Where
                        (p => p.OcenaOsiagnieciaPekow.Any
                            (o => o.KartaPrzedmiotuId == kartaPrzedmiotu.KartaPrzedmiotuId
                                && o.FormaOcenyId == 2 && o.NrOceny == i)).ToList());
                    i++;
                }

                model.PEKOcenyF = ocenyPekiF;
                model.PEKOcenyP = ocenyPekiP;

                /*SEKCJA NR 9 - LITERATURA*/
                model.LiteraturaPodst = db.Literatura.Where(l => l.PozycjaLiteratury.PozycjaLiteraturyKartaPrzedmiotu.Any
                    (p => p.KartaPrzedmiotuId == kartaPrzedmiotu.KartaPrzedmiotuId && p.TypLiteraturyId == 1)).ToList();
                model.LiteraturaUzup = db.Literatura.Where(l => l.PozycjaLiteratury.PozycjaLiteraturyKartaPrzedmiotu.Any
                    (p => p.KartaPrzedmiotuId == kartaPrzedmiotu.KartaPrzedmiotuId && p.TypLiteraturyId == 2)).ToList();

                model.LiteraturaPodstWydawca = new List<string>();
                model.LiteraturaUzupWydawca = new List<string>();
                model.LiteraturaPodstAutorzy = new List<List<Osoba>>();
                model.LiteraturaUzupAutorzy = new List<List<Osoba>>();

                foreach (var item in model.LiteraturaPodst)
                {
                    model.LiteraturaPodstWydawca.Add(item.Wydawca.Wartosc);
                }
                foreach (var item in model.LiteraturaUzup)
                {
                    model.LiteraturaUzupWydawca.Add(item.Wydawca.Wartosc);
                }

                foreach (var item in model.LiteraturaPodst)
                {
                    model.LiteraturaPodstAutorzy.Add(item.Osoba.ToList());
                }
                foreach (var item in model.LiteraturaUzup)
                {
                    model.LiteraturaUzupAutorzy.Add(item.Osoba.ToList());
                }

                model.AutorImie = kartaPrzedmiotu.Pracownik.Dydaktyk.Osoba.Imie;
                model.AutorNazwisko = kartaPrzedmiotu.Pracownik.Dydaktyk.Osoba.Nazwisko;
                model.AutorEmail = kartaPrzedmiotu.Pracownik.Dydaktyk.Email;

                /*SEKCJA NR 10 - MACIERZ POWIĄZAŃ*/
                model.MPkeki = new List<List<KierunkowyEfektKsztalcenia>>();
                model.MPcele = new List<List<CelPrzedmiotu>>();
                model.MPtresci = new List<List<TrescProgramowa>>();
                model.MPnarzedzia = new List<List<Narzedzie>>();

                foreach (var item in model.PEKi)
                {
                    model.MPkeki.Add(db.KierunkowyEfektKsztalcenia.Where(k => k.PEKSpelniaKEK.Any(m => m.PekId == item.PekId)).ToList());
                    model.MPcele.Add(db.CelPrzedmiotu.Where(c => c.CelMP.Any(m => m.PekId == item.PekId)).ToList());
                    model.MPtresci.Add(db.TrescProgramowa.Where(t => t.TrescMP.Any(m => m.PekId == item.PekId)).ToList());
                    model.MPnarzedzia.Add(db.Narzedzie.Where(n => n.NarzedzieMP.Any(m => m.PekId == item.PekId)).ToList());
                }

                model.TypPrzedmiotu = db.TypPrzedmiotu.ToList();

                return View(model);
            }
        }

        public virtual ActionResult GetPDF(int? id)
        {
            if (id != null)
            {
                return new ActionAsPdf("Details", new { id = id })
                {
                    FileName = "karta_przedmiotu.pdf",
                    CustomSwitches = "--footer-center [page]"
                };
            }
            else
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
        }
    }
}