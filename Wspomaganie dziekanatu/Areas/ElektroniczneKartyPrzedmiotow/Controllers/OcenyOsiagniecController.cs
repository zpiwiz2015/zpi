﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Models;
using Wspomaganie_dziekanatu.Models;

namespace Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Controllers
{
    public partial class OcenyOsiagniecController : Controller
    {
        public virtual ActionResult _Index(int id)
        {
            using (var dbo = new ZpiDbCtx())
            {
                OcenaOsiagniecVM model = new OcenaOsiagniecVM();
                var kartaPrzedmiotu = dbo.KartaPrzedmiotu.FirstOrDefault(k => k.KartaPrzedmiotuId == id);

                model.OcenyPEKF = dbo.OcenaOsiagnieciaPekow.Where(o => o.KartaPrzedmiotuId ==
                    kartaPrzedmiotu.KartaPrzedmiotuId && o.FormaOcenyId == 1).ToList();
                model.OcenyPEKP = dbo.OcenaOsiagnieciaPekow.Where(o => o.KartaPrzedmiotuId ==
                    kartaPrzedmiotu.KartaPrzedmiotuId && o.FormaOcenyId == 2).ToList();

                var pekOcenyF = dbo.PrzedmiotowyEfektKsztalcenia.Where
                    (p => p.OcenaOsiagnieciaPekow.Any(o => o.KartaPrzedmiotuId ==
                        kartaPrzedmiotu.KartaPrzedmiotuId && o.FormaOcenyId == 1));
                var pekOcenyP = dbo.PrzedmiotowyEfektKsztalcenia.Where
                    (p => p.OcenaOsiagnieciaPekow.Any(o => o.KartaPrzedmiotuId ==
                        kartaPrzedmiotu.KartaPrzedmiotuId && o.FormaOcenyId == 2));

                List<List<PrzedmiotowyEfektKsztalcenia>> ocenyPekiF = new List<List<PrzedmiotowyEfektKsztalcenia>>();
                List<List<PrzedmiotowyEfektKsztalcenia>> ocenyPekiP = new List<List<PrzedmiotowyEfektKsztalcenia>>();

                model.TypEfektu = dbo.TypEfektu.ToList();

                int i = 1;
                foreach (var item in pekOcenyF)
                {
                    ocenyPekiF.Add(dbo.PrzedmiotowyEfektKsztalcenia.Where
                        (p => p.OcenaOsiagnieciaPekow.Any
                            (o => o.KartaPrzedmiotuId == kartaPrzedmiotu.KartaPrzedmiotuId
                                && o.FormaOcenyId == 1 && o.NrOceny == i)).ToList());
                    i++;
                }

                i = 1;

                foreach (var item in pekOcenyP)
                {
                    ocenyPekiP.Add(dbo.PrzedmiotowyEfektKsztalcenia.Where
                        (p => p.OcenaOsiagnieciaPekow.Any
                            (o => o.KartaPrzedmiotuId == kartaPrzedmiotu.KartaPrzedmiotuId
                                && o.FormaOcenyId == 2 && o.NrOceny == i)).ToList());
                    i++;
                }

                model.PEKOcenyF = ocenyPekiF;
                model.PEKOcenyP = ocenyPekiP;

                return View(model);
            }
        }

        [ChildActionOnly]
        public virtual ActionResult _Create()
        {
            return View();
        }
    }
}