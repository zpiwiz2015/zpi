﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Controllers
{
    // Na podstawie treści udostępnionych przez Jakuba Gutkowskiego, na blogu:
    // http://blog.gutek.pl/2011/05/15/edycja-zagniezdzonych-list-w-mvc-3-czesc-2/

    public partial class BaseController : Controller
    {
        protected readonly string NestedPrefix = "__prefix";

        protected DynamicViewResult DynamicView(string viewName, object model)
        {
            return DynamicView(null, viewName, model);
        }// DynamicView()

        protected DynamicViewResult DynamicView(string formId, string viewName, object model)
        {
            if (model != null)
            {
                ViewData.Model = model;
            }

            DynamicViewResult result = new DynamicViewResult();
            result.FormId = formId;
            result.ViewName = viewName;
            result.ViewData = ViewData;
            result.TempData = TempData;
            return result;
        }// DynamicView()

    }// BaseControler
}// namespace