﻿using System.Collections.Generic;
using System.Linq;
using Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Models;
using Wspomaganie_dziekanatu.Models;

namespace Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow
{
    public class PobieranieDanych
    {
        private ZpiDbCtx dbo = new ZpiDbCtx();

        public List<KartaPrzedmiotuVM> PobierzDaneKartyPrzedmiotu()
        {
            var dane = (from kp in dbo.KartaPrzedmiotu
                        join pkkp in dbo.ProgramKierunkuKP on kp.KartaPrzedmiotuId equals pkkp.KartaPrzedmiotuId
                        select new KartaPrzedmiotuVM
                        {
                            Id = kp.KartaPrzedmiotuId,
                            DataUtworzenia = kp.DataUtworzenia,
                            DataZaakceptowania = kp.DataAkceptacji,
                            Status = kp.StatusKarty.Wartosc,
                            StatusId = kp.StatusKartyId,
                            WersjaJezykowa = kp.Jezyk.Wartosc,
                            WersjaJezykowaId = kp.JezykId,

                            OpiekunId = kp.Pracownik.Dydaktyk.OsobaId,
                            ImieOpiekuna = kp.Pracownik.Dydaktyk.Osoba.Imie,
                            NazwiskoOpiekuna = kp.Pracownik.Dydaktyk.Osoba.Nazwisko,
                            TytulOpiekuna = kp.Pracownik.Dydaktyk.TytulNaukowy.Wartosc,
                            EmailOpiekuna = kp.Pracownik.Dydaktyk.Email,

                            NazwaPrzedmiotuPOL = kp.NazwaPl,
                            NazwaPrzedmiotuENG = kp.NazwaAng,
                            KodPrzedmiotu = kp.KodKursu,
                            FormaStudiow = pkkp.FormaStudiow.Wartosc,
                            GrupaKursowId = pkkp.CzyGrupaKursow,
                            KierunekStudiow = pkkp.KierunekStudiow.Wartosc,
                            RodzajPrzedmiotu = pkkp.RodzajPrzedmiotu.Wartosc,
                            Specjalnosc = pkkp.Specjalnosc.Wartosc,
                            StopienStudiow = pkkp.StopienStudiow.Wartosc,
                            //Studium = ?
                            Wydzial = pkkp.Wydzial.Wartosc,
                        })
                         .Distinct()
                         .ToList();

            return dane;
        }// PobierzDaneKartyPrzedmiotu()

        public List<TabelaPunktowGodzinVM> PobierzDaneTabeliPunktowGodzin(int id)
        {
            var dane = (from fp in dbo.FormaPrzedmiotu
                       where fp.KartaPrzedmiotuId == id
                       select new TabelaPunktowGodzinVM
                        {
                            FormaZaliczenia = fp.FormaZaliczenia.Wartosc,
                            FormaZaliczeniaId = fp.FormaZaliczeniaId,
                            GodzinyCNPS = fp.CNPS,
                            GodzinyZZU = fp.ZZU,
                            KursKoncowyWartosc = fp.CzyKursKoncowy,
                            PunktyBK = fp.PunktyBK,
                            PunktyECTS = fp.ECTS,
                            PunktyP = fp.PunktyPrakt,
                            TypPrzedmiotu = fp.TypPrzedmiotu.Wartosc,
                            TypPrzedmiotuId = fp.TypPrzedmiotuId,
                        })
                       .Distinct()
                       .OrderBy(d => d.TypPrzedmiotuId)
                       .ToList();

            return dane;
        }// PobierzDaneTabeliPunktowGodzin()


        public List<WymaganieWstepneVM> PobierzDaneWymagan(int id)
        {
            var dane = (from ww in dbo.WymaganieWstepne
                        where ww.KartaPrzedmiotuId == id
                        select new WymaganieWstepneVM
                        {
                            Numer = ww.NrWymagania,
                            Opis = ww.OpisWymagania,
                            JezykId = ww.JezykId
                        })
                       .Distinct()
                       .OrderBy(d => d.Numer)
                       .ToList();

            return dane;
        }// PobierzDaneWymagan()

        public List<CelPrzedmiotuVM> PobierzDaneCelow(int id)
        {
            var dane = (from cp in dbo.CelPrzedmiotu
                        join cmp in dbo.CelMP on cp.CelPrzedmiotuId equals cmp.CelPrzedmiotuId
                        where cmp.KartaPrzedmiotuId == id
                        select new CelPrzedmiotuVM
                        {
                            Numer = cp.NrCelu,
                            Opis = cp.OpisCelu,
                            JezykId = cp.JezykId
                        })
                       .Distinct()
                       .OrderBy(d => d.Numer)
                       .ToList();

            return dane;
        }// PobierzDaneCelow()

        public List<PekVM> PobierzDanePek(int id)
        {
            var dane = (from pek in dbo.PrzedmiotowyEfektKsztalcenia
                        join te in dbo.TypEfektu on pek.TypEfektuId equals te.Id
                        join mp in dbo.MacierzPowiazan on pek.PekId equals mp.PekId
                        where mp.KartaPrzedmiotuId == id
                        select new PekVM
                        {
                            Numer = pek.NrPek,
                            Opis = pek.Opis,
                            TypPek = te.Kod,
                            TypPekId = te.Id,
                            JezykId = pek.JezykId
                        })
                       .Distinct()
                       .OrderBy(d => d.TypPekId)
                       .ThenBy(d => d.Numer)
                       .ToList();

            return dane;
        }// PobierzDanePek()

        public List<TrescProgramowaVM> PobierzDaneTresci(int id)
        {
            var dane = (from tp in dbo.TrescProgramowa
                        join tmp in dbo.TrescMP on tp.TrescProgrId equals tmp.TrescProgrId
                        where tmp.KartaPrzedmiotuId == id
                        select new TrescProgramowaVM
                        {
                            LiczbaGodzin = tp.LiczbaGodz,
                            Numer = tp.NrTresci,
                            Opis = tp.OpisTresci,
                            TypPrzedmiotu = tp.TypPrzedmiotu.Wartosc,
                            TypPrzedmiotuId = tp.TypPrzedmiotuId,
                            JezykId = tp.JezykId
                        })
                       .Distinct()
                       .OrderBy(d => d.TypPrzedmiotuId)
                       .ThenBy(d => d.Numer)
                       .ToList();

            return dane;
        }// PobierzDaneTresci()

        public List<NarzedzieDydaktyczneVM> PobierzDaneNarzedzi(int id)
        {
            var dane = (from n in dbo.Narzedzie
                        where n.KartaPrzedmiotuId == id
                        select new NarzedzieDydaktyczneVM
                        {
                            Numer = n.NrNarzedzia,
                            Opis = n.OpisNarzedzia,
                            JezykId = n.JezykId
                        })
                       .Distinct()
                       .OrderBy(d => d.Numer)
                       .ToList();

            return dane;
        }// PobierzDaneNarzedzi()

        public List<LiteraturaVM> PobierzDaneLiteratury(int id)
        {
            var dane = (from l in dbo.Literatura
                        join plkp in dbo.PozycjaLiteraturyKartaPrzedmiotu on l.PozycjaLiteraturyId equals plkp.PozycjaLiteraturyId
                        join tl in dbo.TypLiteratury on plkp.TypLiteraturyId equals tl.Id
                        where plkp.KartaPrzedmiotuId == id
                        select new LiteraturaVM
                        {
                            RokWydania = l.RokWydania,
                            TypId = tl.Id,
                            Tytul = l.Tytul,
                            Wydawnictwo = l.Wydawca.Wartosc
                        })
                       .Distinct()
                       .OrderBy(d => d.Tytul)
                       .ToList();

            return dane;
        }// PobierzDaneLiteratury()

    }// PobieranieDanychKartyPrzedmiotu
}// namespace