﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Models
{
    public class TrescProgramowaVM
    {
        [DisplayFormat(DataFormatString = "{0:D2}")]
        public int Numer { get; set; }

        public string Opis { get; set; }

        public string TypPrzedmiotu { get; set; }
        public int TypPrzedmiotuId { get; set; }

        [Display(Name = "liczba godzin:")]
        public int LiczbaGodzin { get; set; }

        public Nullable<int> JezykId { get; set; }

        public string Nr
        {
            get
            {
                if (TypPrzedmiotuId == 1)
                {
                    return "Wy" + Numer;
                }
                else if (TypPrzedmiotuId == 2)
                {
                    return "Ćw" + Numer;
                }
                else if (TypPrzedmiotuId == 3)
                {
                    return "La" + Numer;
                }
                else if (TypPrzedmiotuId == 4)
                {
                    return "Pr" + Numer;
                }
                else if (TypPrzedmiotuId == 5)
                {
                    return "Se" + Numer;
                }
                else { return "Nr" + Numer; }
            }
        }
    }
}