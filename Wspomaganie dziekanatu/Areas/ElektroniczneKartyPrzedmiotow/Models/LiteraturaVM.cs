﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Models
{
    public class LiteraturaVM
    {
        [Required]
        [Display(Name = "Tytuł:")]
        public string Tytul { get; set; }

        [Required]
        [Display(Name = "Rok wydania:")]
        public string RokWydania { get; set; }

        [Required]
        [Display(Name = "Wydawnictwo:")]
        public string Wydawnictwo { get; set; }

        [Required]
        public int TypId { get; set; }

        public IEnumerable<AutorVM> AutorzyList { get; set; }

        public LiteraturaVM() {
            AutorzyList = new List<AutorVM>();
        }
    }// LiteraturaVM

    public class AutorVM
    {
        [Required]
        [Display(Name = "Imię:")]
        public string Imie { get; set; }

        [Required]
        [Display(Name = "Nazwisko:")]
        public string Nazwisko { get; set; }
    }// AutorVM

}// namespace