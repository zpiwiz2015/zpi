﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Wspomaganie_dziekanatu.Models;

namespace Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Models
{
    public class OcenaOsiagniecVM
    {


        public List<OcenaOsiagnieciaPekow> OcenyPEKF { get; set; }
        public List<OcenaOsiagnieciaPekow> OcenyPEKP { get; set; }

        public List<List<PrzedmiotowyEfektKsztalcenia>> PEKOcenyF { get; set; }
        public List<List<PrzedmiotowyEfektKsztalcenia>> PEKOcenyP { get; set; }

        public List<TypEfektu> TypEfektu { get; set; }
    }
}