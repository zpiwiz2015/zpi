﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Models
{
    public class TabelaPunktowGodzinVM
    {
        public int GodzinyZZU { get; set; }

        public int GodzinyCNPS { get; set; }

        public string FormaZaliczenia { get; set; }
        public int FormaZaliczeniaId { get; set; }

        public byte? KursKoncowyWartosc { get; set; }
        public string KursKoncowy
        {
            get
            {
                if (KursKoncowyWartosc != null)
                {
                    return "tak";
                }
                else
                {
                    return null;
                }
            }
        }

        public int PunktyECTS { get; set; }

        public string PunktyP { get; set; }

        public string PunktyBK { get; set; }

        public string TypPrzedmiotu { get; set; }

        public int TypPrzedmiotuId { get; set; }

    }// TabelaPunktowGodzinVM
}// namespace