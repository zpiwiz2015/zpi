﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Wspomaganie_dziekanatu.Models;

namespace Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Models
{
    public class MacierzVM
    {
        public string NazwaPrzedmiotu { get; set; }

        public KierunekStudiow KierunekStudiow { get; set; }

        public string Specjalnosc { get; set; }
        public List<TypPrzedmiotu> TypPrzedmiotu { get; set; }
        public List<TypEfektu> TypEfektu { get; set; }
        public StopienStudiow StopienStudiow { get; set; }
        public List<PrzedmiotowyEfektKsztalcenia> PEKi { get; set; }
        public List<PrzedmiotowyEfektKsztalcenia> PEKiW { get; set; }
        public List<PrzedmiotowyEfektKsztalcenia> PEKiU { get; set; }
        public List<PrzedmiotowyEfektKsztalcenia> PEKiK { get; set; }

        public List<List<KierunkowyEfektKsztalcenia>> MPkeki { get; set; }
        public List<List<CelPrzedmiotu>> MPcele { get; set; }
        public List<List<TrescProgramowa>> MPtresci { get; set; }
        public List<List<Narzedzie>> MPnarzedzia { get; set; }
    }

}