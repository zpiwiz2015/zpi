﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Wspomaganie_dziekanatu.Models;

namespace Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Models
{
    public class KartaDoPdfModel
    {

        public int KartaPrzedmiotuId { get; set; }

        [Display(Name = "Wydział*:")]
        public string Wydzial { get; set; }

        [Display(Name = "Studium*:")]
        public string Studium { get; set; }

        [Display(Name = "Nazwa w języku polskim:")]
        public string NazwaPrzedmiotuPL { get; set; }

        [Display(Name = "Nazwa w języku angielskim:")]
        public string NazwaPrzedmiotuEN { get; set; }

        [Display(Name = "Kierunek studiów*:")]
        public KierunekStudiow KierunekStudiow { get; set; }

        [Display(Name = "Specjalność*:")]
        public string Specjalnosc { get; set; }

        [Display(Name = "Stopień studiów:")]
        public StopienStudiow StopienStudiow { get; set; }

        [Display(Name = "Forma studiów:")]
        public string FormaStudiow { get; set; }

        [Display(Name = "Rodzaj przedmiotu:")]
        public string RodzajPrzedmiotu { get; set; }

        [Display(Name = "Kod przedmiotu:")]
        public string KodPrzedmiotu { get; set; }

        [Display(Name = "Grupa kursów:")]
        public byte GrupaKursow { get; set; }

        [Display(Name = "")]
        public string wZZU { get; set; }
        [Display(Name = "")]
        public string cZZU { get; set; }
        [Display(Name = "")]
        public string lZZU { get; set; }
        [Display(Name = "")]
        public string pZZU { get; set; }
        [Display(Name = "")]
        public string sZZU { get; set; }

        [Display(Name = "")]
        public string wCNPS { get; set; }
        [Display(Name = "")]
        public string cCNPS { get; set; }
        [Display(Name = "")]
        public string lCNPS { get; set; }
        [Display(Name = "")]
        public string pCNPS { get; set; }
        [Display(Name = "")]
        public string sCNPS { get; set; }

        [Display(Name = "")]
        public string wZaliczenie { get; set; }
        [Display(Name = "")]
        public string cZaliczenie { get; set; }
        [Display(Name = "")]
        public string lZaliczenie { get; set; }
        [Display(Name = "")]
        public string pZaliczenie { get; set; }
        [Display(Name = "")]
        public string sZaliczenie { get; set; }

        [Display(Name = "")]
        public bool wKoncowy { get; set; }
        [Display(Name = "")]
        public bool cKoncowy { get; set; }
        [Display(Name = "")]
        public bool lKoncowy { get; set; }
        [Display(Name = "")]
        public bool pKoncowy { get; set; }
        [Display(Name = "")]
        public bool sKoncowy { get; set; }

        [Display(Name = "")]
        public string wECTS { get; set; }
        [Display(Name = "")]
        public string cECTS { get; set; }
        [Display(Name = "")]
        public string lECTS { get; set; }
        [Display(Name = "")]
        public string pECTS { get; set; }
        [Display(Name = "")]
        public string sECTS { get; set; }


        [Display(Name = "")]
        public string wP { get; set; }
        [Display(Name = "")]
        public string cP { get; set; }
        [Display(Name = "")]
        public string lP { get; set; }
        [Display(Name = "")]
        public string pP { get; set; }
        [Display(Name = "")]
        public string sP { get; set; }

        [Display(Name = "")]
        public string wBK { get; set; }
        [Display(Name = "")]
        public string cBK { get; set; }
        [Display(Name = "")]
        public string lBK { get; set; }
        [Display(Name = "")]
        public string pBK { get; set; }
        [Display(Name = "")]
        public string sBK { get; set; }

        public List<WymaganieWstepne> WymaganiaWstepne { get; set; }

        public List<CelPrzedmiotu> CelePrzedmiotu { get; set; }

        public List<PrzedmiotowyEfektKsztalcenia> PEKi { get; set; }
        public List<PrzedmiotowyEfektKsztalcenia> PEKiW { get; set; }
        public List<PrzedmiotowyEfektKsztalcenia> PEKiU { get; set; }
        public List<PrzedmiotowyEfektKsztalcenia> PEKiK { get; set; }

        public List<TrescProgramowa> TrescWyklady { get; set; }
        public List<TrescProgramowa> TrescCwiczenia { get; set; }
        public List<TrescProgramowa> TrescLaboratorium { get; set; }
        public List<TrescProgramowa> TrescProjekt { get; set; }
        public List<TrescProgramowa> TrescSeminarium { get; set; }

        public List<Narzedzie> Narzedzia { get; set; }

        public List<TypPrzedmiotu> TypPrzedmiotu { get; set; }

        public List<OcenaOsiagnieciaPekow> OcenyPEKF { get; set; }
        public List<OcenaOsiagnieciaPekow> OcenyPEKP { get; set; }

        public List<List<PrzedmiotowyEfektKsztalcenia>> PEKOcenyF { get; set; }
        public List<List<PrzedmiotowyEfektKsztalcenia>> PEKOcenyP { get; set; }

        public List<TypEfektu> TypEfektu { get; set; }

        public List<Literatura> LiteraturaPodst { get; set; }
        public List<Literatura> LiteraturaUzup { get; set; }
        public List<string> LiteraturaPodstWydawca { get; set; }
        public List<string> LiteraturaUzupWydawca { get; set; }
        public List<List<Osoba>> LiteraturaPodstAutorzy { get; set; }
        public List<List<Osoba>> LiteraturaUzupAutorzy { get; set; }

        public string AutorImie { get; set; }
        public string AutorNazwisko { get; set; }
        public string AutorEmail { get; set; }

        public List<List<KierunkowyEfektKsztalcenia>> MPkeki { get; set; }
        public List<List<CelPrzedmiotu>> MPcele { get; set; }
        public List<List<TrescProgramowa>> MPtresci { get; set; }
        public List<List<Narzedzie>> MPnarzedzia { get; set; }
    }
}