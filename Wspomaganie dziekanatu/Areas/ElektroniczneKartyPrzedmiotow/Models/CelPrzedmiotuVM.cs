﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Models
{
    public class CelPrzedmiotuVM
    {
        [DisplayFormat(DataFormatString = "C{0:D}.")]
        public int Numer { get; set; }

        [Required]
        public string Opis { get; set; }

        public Nullable<int> JezykId { get; set; }

    }// CelPrzedmiotuVM
}// namespace