﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Models
{
    public class PekVM
    {
        [DisplayFormat(DataFormatString = "{0:D2}")]
        public string Numer { get; set; }

        public string Opis { get; set; }

        public string TypPek { get; set; }
        public int TypPekId { get; set; }

        public Nullable<int> JezykId { get; set; }

        public string Nr
        {
            get
            {
                return "PEK_" + TypPek + Numer;
            }
        }

    }
}