﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Models
{
    public class KartaPrzedmiotuVM
    {
        // dane karty przedmiotu

        public int Id { get; set; }

        [Display(Name = "Data utworzenia:")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        [DataType(DataType.Date)]
        public Nullable<DateTime> DataUtworzenia { get; set; }

        [Display(Name = "Data zaakceptowania:")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        [DataType(DataType.Date)]
        public Nullable<DateTime> DataZaakceptowania { get; set; }

        [Display(Name = "Status karty:")]
        public string Status { get; set; }

        public int StatusId { get; set; }

        [Display(Name = "Wersja językowa:")]
        public string WersjaJezykowa { get; set; }

        public Nullable<int> WersjaJezykowaId { get; set; }


        // dane opiekuna

        public int OpiekunId { get; set; }

        [Display(Name = "Imię:")]
        public string ImieOpiekuna { get; set; }

        [Display(Name = "Nazwisko:")]
        public string NazwiskoOpiekuna { get; set; }

        public string TytulOpiekuna { get; set; }

        [Display(Name = "E-mail:")]
        public string EmailOpiekuna { get; set; }

        [Display(Name = "Opiekun przedmiotu:")]
        public string OpiekunPrzedmiotu
        {
            get
            {
                if (string.IsNullOrEmpty(TytulOpiekuna))
                {
                    return ImieOpiekuna + " " + NazwiskoOpiekuna;
                }
                else
                {
                    return TytulOpiekuna + " " + ImieOpiekuna + " " + NazwiskoOpiekuna;
                }
            }
        }


        // dane przedmiotu

        [Display(Name = "Kod przedmiotu:")]
        public string KodPrzedmiotu { get; set; }

        [Display(Name = "Nazwa w języku angielskim:")]
        public string NazwaPrzedmiotuENG { get; set; }

        [Display(Name = "Nazwa w języku polskim:")]
        public string NazwaPrzedmiotuPOL { get; set; }

        [Display(Name = "Rodzaj przedmiotu:")]
        public string RodzajPrzedmiotu { get; set; }

        [Display(Name = "Wydział:")]
        [DisplayFormat(ConvertEmptyStringToNull = true)]
        public string Wydzial { get; set; }

        [DisplayFormat(ConvertEmptyStringToNull = true)]
        public string Studium { get; set; }

        [Display(Name = "Stopien studiów")]
        public string StopienStudiow { get; set; }

        [Display(Name = "Forma studiów:")]
        public string FormaStudiow { get; set; }

        [Display(Name = "Kierunek studiów:")]
        public string KierunekStudiow { get; set; }

        [Display(Name = "Specjalność:")]
        public string Specjalnosc { get; set; }

        [Display(Name = "Grupa kursów:")]
        public string GrupaKursow
        {
            get
            {
                if (GrupaKursowId == 1)
                {
                    return "tak";
                }
                else
                {
                    return "nie";
                }
            }
        }
        public byte GrupaKursowId { get; set; }


        // pozostałe segmenty karty przedmiotu

        public List<WymaganieWstepneVM> WymaganiaWstepneList { get; set; }
        public List<CelPrzedmiotuVM> CelePrzedmiotuList { get; set; }
        public List<NarzedzieDydaktyczneVM> NarzedziaDydaktyczneList { get; set; }

        public List<LiteraturaVM> LiteraturaPList { get; set; }
        public List<LiteraturaVM> LiteraturaUList { get; set; }
        
        public List<PekVM> PekiWList { get; set; }
        public List<PekVM> PekiUList { get; set; }
        public List<PekVM> PekiKList { get; set; }

        public List<TrescProgramowaVM> TresciWList { get; set; }
        public List<TrescProgramowaVM> TresciCList { get; set; }
        public List<TrescProgramowaVM> TresciLList { get; set; }
        public List<TrescProgramowaVM> TresciPList { get; set; }
        public List<TrescProgramowaVM> TresciSList { get; set; }

        public KartaPrzedmiotuVM()
        {
            WymaganiaWstepneList = new List<WymaganieWstepneVM>();
            CelePrzedmiotuList = new List<CelPrzedmiotuVM>();
            NarzedziaDydaktyczneList = new List<NarzedzieDydaktyczneVM>();
            
            LiteraturaPList = new List<LiteraturaVM>();
            LiteraturaUList = new List<LiteraturaVM>();
            
            PekiWList = new List<PekVM>();
            PekiUList = new List<PekVM>();
            PekiKList = new List<PekVM>();

            TresciWList = new List<TrescProgramowaVM>();
            TresciCList = new List<TrescProgramowaVM>();
            TresciLList = new List<TrescProgramowaVM>();
            TresciPList = new List<TrescProgramowaVM>();
            TresciSList = new List<TrescProgramowaVM>();
        }// KartaPrzedmiotuVM()

    }// KartaprzedmiotuVM

    public class GrupaKursowVM
    {
        public int Id { get; set; }
        public string Wartosc { get; set; }
    }// GrupaKursowVM

    public class WyborJezykaVM
    {
        public int Id { get; set; }
        public string Wartosc { get; set; }
    }//WyborJezykaVM
}// namespace
