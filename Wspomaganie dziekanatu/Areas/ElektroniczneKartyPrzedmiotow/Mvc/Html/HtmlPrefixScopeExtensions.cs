﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq.Expressions;
using System.Reflection;
using Wspomaganie_dziekanatu.Areas.ElektroniczneKartyPrzedmiotow.Extensions;

namespace System.Web.Mvc.Html
{
    // Na podstawie treści udostępnionych przez Jakuba Gutkowskiego, na blogu:
    // http://blog.gutek.pl/2011/05/12/edycja-zagniezdzonych-list-w-mvc-3-czesc-1/

    public static class HtmlPrefixScopeExtensions
    {
        private const string IdsToReuseKey = "__htmlPrefixScopeExtensions_IdsToReuse_";
        private const string ViewDataPrefixKey = "__prefix";
        private const string IndexNameWithDot = ".index";

        public static MvcHtmlString AddDynamicRowLink(this HtmlHelper html, string linktTitle, string actionName, string selector, object htmlAttributes = null)
        {
            var dictHtmlAttrs = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
            var dictDefaults = new Dictionary<string, object>
            {
                { "data-prefix", html.ViewData.TemplateInfo.HtmlFieldPrefix },
                { "data-placeholder", selector },
                { "title", linktTitle },
                { "class", "addRow" }
            };// dictDefaults

            return html.ActionLink(
                linktTitle,
                actionName,
                null,
                dictDefaults.MergeAttributes(dictHtmlAttrs)
                );
        }// AddDynamicRowLink()

        public static IDisposable BeginCollectionItemFor<TModel>(this HtmlHelper html, Expression<Func<TModel, IEnumerable>> expression)
        {
            Type type = typeof(TModel);

            var member = expression.Body as MemberExpression;
            if (member == null)
            {
                throw new ArgumentException(string.Format("Expression '{0}' refers to a method, not a property.", expression));
            }

            var propInfo = member.Member as PropertyInfo;
            if (propInfo == null)
            {
                throw new ArgumentException(string.Format("Expression '{0}' refers to a field, not a property.", expression));
            }

            return html.BeginCollectionItem(member.Member.Name);
        }// BeginCollectionItemFor()

        public static IDisposable BeginCollectionItem(this HtmlHelper html, string collectionName)
        {
            var idsToReuse = GetIdsToReuse(html.ViewContext.HttpContext, collectionName);

            string itemIndex = idsToReuse.Count > 0 ? idsToReuse.Dequeue() : Guid.NewGuid().GetHashCode().ToString();

            var templatePrefix = html.ViewData.TemplateInfo.HtmlFieldPrefix;

            string viewDataPrefix = html.ViewData.ContainsKey(ViewDataPrefixKey) ? html.ViewData[ViewDataPrefixKey] as string : string.Empty;

            var currentPrefix = !string.IsNullOrEmpty(templatePrefix) ? templatePrefix : viewDataPrefix;

            string dot = string.IsNullOrEmpty(currentPrefix) ? string.Empty : ".";

            string fullPrefix = string.Format("{0}{1}{2}", currentPrefix, dot, collectionName);

            html.ViewContext.Writer.WriteLine(
                string.Format(CultureInfo.InvariantCulture, "<input type=\"hidden\" name=\"{0}{1}\" autocomplete=\"off\" value=\"{2}\" />", fullPrefix, IndexNameWithDot, html.Encode(itemIndex))
                );

            var fieldPrefix = string.Format(CultureInfo.InvariantCulture, "{0}[{1}]", fullPrefix, itemIndex);

            return html.BeginHtmlFieldPrefixScope(fieldPrefix);
        }// BeginCollectionItem()

        static IDisposable BeginHtmlFieldPrefixScope(this HtmlHelper html, string htmlFieldPrefix)
        {
            return new HtmlFieldPrefixScope(html.ViewData.TemplateInfo, htmlFieldPrefix);
        }// BeginHtmlFieldPrefixScope()

        private static Queue<string> GetIdsToReuse(HttpContextBase httpContext, string collectionName)
        {
            string key = IdsToReuseKey + collectionName;
            var queue = (Queue<string>)httpContext.Items[key];

            if (queue != null)
            {
                return queue;
            }

            httpContext.Items[key] = queue = new Queue<string>();
            var previouslyUsedIds = httpContext.Request[collectionName + IndexNameWithDot];

            if (!string.IsNullOrEmpty(previouslyUsedIds))
            {
                foreach (var previouslyUsedId in previouslyUsedIds.Split(','))
                {
                    queue.Enqueue(previouslyUsedId);
                }
            }

            return queue;
        }// GetIdsToReuse()

        private class HtmlFieldPrefixScope : IDisposable
        {
            private readonly TemplateInfo _templateInfo;
            private readonly string _previousHtmlFieldPrefix;

            public HtmlFieldPrefixScope(TemplateInfo templateInfo, string htmlFieldPrefix)
            {
                _templateInfo = templateInfo;
                _previousHtmlFieldPrefix = templateInfo.HtmlFieldPrefix;
                templateInfo.HtmlFieldPrefix = htmlFieldPrefix;
            }// HtmlFieldPrefixScope()

            public void Dispose()
            {
                _templateInfo.HtmlFieldPrefix = _previousHtmlFieldPrefix;
            }// Dispose()

        }// HtmlFieldPrefixScope

    }// HtmlPrefixScopeExtensions
}// namespace