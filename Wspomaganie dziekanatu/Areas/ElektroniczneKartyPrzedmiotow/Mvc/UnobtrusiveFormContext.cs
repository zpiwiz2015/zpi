﻿
namespace System.Web.Mvc
{
    // Na podstawie treści udostępnionych przez Jakuba Gutkowskiego, na blogu:
    // http://blog.gutek.pl/2011/05/15/edycja-zagniezdzonych-list-w-mvc-3-czesc-2/

    public class UnobtrusiveFormContext : IDisposable
    {
        private readonly ViewContext _context;

        public UnobtrusiveFormContext(ViewContext context, string formId)
        {
            _context = context;
            _context.ClientValidationEnabled = true;
            _context.UnobtrusiveJavaScriptEnabled = true;
            _context.FormContext = new FormContext { FormId = formId };
        }// UnobtrusiveFormContext()

        public void Dispose()
        {
            _context.OutputClientValidation();
        }// Dispose()

    }// UnobtrusiveFormContext
}// namespace