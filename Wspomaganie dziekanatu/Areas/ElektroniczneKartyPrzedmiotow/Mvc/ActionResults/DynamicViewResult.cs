﻿using System.Globalization;
using System.Text;

namespace System.Web.Mvc
{
    // Na podstawie treści udostępnionych przez Jakuba Gutkowskiego, na blogu:
    // http://blog.gutek.pl/2011/05/15/edycja-zagniezdzonych-list-w-mvc-3-czesc-2/

    public class DynamicViewResult : ViewResultBase
    {
        public string FormId { get; set; }

        protected override ViewEngineResult FindView(ControllerContext context)
        {
            ViewEngineResult result = ViewEngineCollection.FindPartialView(context, ViewName);
            ViewContext viewContext = new ViewContext(
                context,
                result.View,
                ViewData,
                TempData,
                context.HttpContext.Response.Output
                );

            using (new UnobtrusiveFormContext(viewContext, FormId))
            {
                if (result.View != null)
                {
                    return result;
                }

                StringBuilder bulider = new StringBuilder();

                foreach (string str in result.SearchedLocations)
                {
                    bulider.AppendLine();
                    bulider.Append(str);
                }

                throw new InvalidOperationException(
                    string.Format(
                    CultureInfo.CurrentCulture,
                    "The [partial] view '{0}' was not found or no view engine supports the searched locations. The following locations were searched:{1}",
                    ViewName,
                    bulider
                    ));
            }// using
        }// FindView()

    }// DynamicViewResult
}// namespace