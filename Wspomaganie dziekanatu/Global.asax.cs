﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using Wspomaganie_dziekanatu.Models;
using System.Security.Principal;

namespace Wspomaganie_dziekanatu
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            if (FormsAuthentication.CookiesSupported == true)
            {
                if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
                {
                    try
                    {
                        string userName = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
                        string roles = string.Empty;

                        using (var db = new ZpiDbCtx())
                        {
                            var user = db.Uzytkownik.SingleOrDefault(u => u.Login == userName);
                            roles = user.RolaUzytkownika.Aggregate(roles, (current, rola) => current + (rola.Nazwa + ';'));
                        }

                        HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(userName, "Forms"), roles.Split(';'));
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
        }
    }
}
