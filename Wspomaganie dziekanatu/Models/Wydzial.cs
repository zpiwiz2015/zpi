//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Wspomaganie_dziekanatu.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Wydzial
    {
        public Wydzial()
        {
            this.ProgramKierunkuKP = new HashSet<ProgramKierunkuKP>();
            this.Publikacja = new HashSet<Publikacja>();
        }
    
        public int Id { get; set; }
        public string Wartosc { get; set; }
        public string Kod { get; set; }
        public byte CzyAktywny { get; set; }
    
        public virtual ICollection<ProgramKierunkuKP> ProgramKierunkuKP { get; set; }
        public virtual ICollection<Publikacja> Publikacja { get; set; }
    }
}
