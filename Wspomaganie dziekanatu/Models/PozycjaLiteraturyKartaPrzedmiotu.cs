//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Wspomaganie_dziekanatu.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PozycjaLiteraturyKartaPrzedmiotu
    {
        public int PozycjaLiteraturyId { get; set; }
        public int TypLiteraturyId { get; set; }
        public int KartaPrzedmiotuId { get; set; }
    
        public virtual KartaPrzedmiotu KartaPrzedmiotu { get; set; }
        public virtual PozycjaLiteratury PozycjaLiteratury { get; set; }
        public virtual TypLiteratury TypLiteratury { get; set; }
    }
}
