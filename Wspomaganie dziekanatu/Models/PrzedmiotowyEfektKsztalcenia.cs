//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Wspomaganie_dziekanatu.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PrzedmiotowyEfektKsztalcenia
    {
        public PrzedmiotowyEfektKsztalcenia()
        {
            this.MacierzPowiazan = new HashSet<MacierzPowiazan>();
            this.PEKSpelniaKEK = new HashSet<PEKSpelniaKEK>();
            this.StudentBrakZaliczeniaPEK = new HashSet<StudentBrakZaliczeniaPEK>();
            this.OcenaOsiagnieciaPekow = new HashSet<OcenaOsiagnieciaPekow>();
        }
    
        public int PekId { get; set; }
        public string Opis { get; set; }
        public string NrPek { get; set; }
        public Nullable<int> JezykId { get; set; }
        public int TypEfektuId { get; set; }
    
        public virtual Jezyk Jezyk { get; set; }
        public virtual ICollection<MacierzPowiazan> MacierzPowiazan { get; set; }
        public virtual ICollection<PEKSpelniaKEK> PEKSpelniaKEK { get; set; }
        public virtual ICollection<StudentBrakZaliczeniaPEK> StudentBrakZaliczeniaPEK { get; set; }
        public virtual TypEfektu TypEfektu { get; set; }
        public virtual ICollection<OcenaOsiagnieciaPekow> OcenaOsiagnieciaPekow { get; set; }
    }
}
