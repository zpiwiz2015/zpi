//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Wspomaganie_dziekanatu.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class WymaganieWstepne
    {
        public int WymaganieId { get; set; }
        public int KartaPrzedmiotuId { get; set; }
        public int NrWymagania { get; set; }
        public string OpisWymagania { get; set; }
        public Nullable<int> JezykId { get; set; }
    
        public virtual Jezyk Jezyk { get; set; }
        public virtual KartaPrzedmiotu KartaPrzedmiotu { get; set; }
    }
}
