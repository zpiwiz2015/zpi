//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Wspomaganie_dziekanatu.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MacierzPowiazan
    {
        public MacierzPowiazan()
        {
            this.CelMP = new HashSet<CelMP>();
            this.NarzedzieMP = new HashSet<NarzedzieMP>();
            this.TrescMP = new HashSet<TrescMP>();
        }
    
        public int KartaPrzedmiotuId { get; set; }
        public int PekId { get; set; }
        public string NrPEK { get; set; }
    
        public virtual ICollection<CelMP> CelMP { get; set; }
        public virtual KartaPrzedmiotu KartaPrzedmiotu { get; set; }
        public virtual ICollection<NarzedzieMP> NarzedzieMP { get; set; }
        public virtual ICollection<TrescMP> TrescMP { get; set; }
        public virtual PrzedmiotowyEfektKsztalcenia PrzedmiotowyEfektKsztalcenia { get; set; }
    }
}
