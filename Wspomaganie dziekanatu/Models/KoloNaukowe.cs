//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Wspomaganie_dziekanatu.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class KoloNaukowe
    {
        public KoloNaukowe()
        {
            this.Publikacja = new HashSet<Publikacja>();
            this.StudentKoloNaukowe = new HashSet<StudentKoloNaukowe>();
        }
    
        public int KoloNaukoweId { get; set; }
        public int OpiekunId { get; set; }
        public int DyscyplinaNaukowaId { get; set; }
        public Nullable<int> ZasiegKolaNaukowegoId { get; set; }
        public Nullable<int> DrugiOpiekunId { get; set; }
        public string Nazwa { get; set; }
        public System.DateTime DataUtworzenia { get; set; }
        public Nullable<System.DateTime> DataZakonczenia { get; set; }
        public string Charakterystyka { get; set; }
        public Nullable<System.DateTime> DataUsuniecia { get; set; }
        public Nullable<int> UsuwajacyId { get; set; }
    
        public virtual Dydaktyk Dydaktyk { get; set; }
        public virtual Dydaktyk Dydaktyk1 { get; set; }
        public virtual DyscyplinaNaukowa DyscyplinaNaukowa { get; set; }
        public virtual ICollection<Publikacja> Publikacja { get; set; }
        public virtual ICollection<StudentKoloNaukowe> StudentKoloNaukowe { get; set; }
        public virtual ZasiegKolaNaukowego ZasiegKolaNaukowego { get; set; }
        public virtual Osoba Osoba { get; set; }
    }
}
