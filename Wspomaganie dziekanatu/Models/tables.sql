﻿use master;
WHILE EXISTS(select NULL from sys.databases where name='zpi')
BEGIN
    DECLARE @SQL varchar(max)
    SELECT @SQL = COALESCE(@SQL,'') + 'Kill ' + Convert(varchar, SPId) + ';'
    FROM MASTER..SysProcesses
    WHERE DBId = DB_ID(N'zpi') AND SPId <> @@SPId
    EXEC(@SQL)
    DROP DATABASE [zpi]
END
GO

CREATE DATABASE zpi
GO
use  zpi;

--slowniki

CREATE TABLE Obywatelstwo (
	ObywatelstwoId int IDENTITY(1,1) PRIMARY KEY
	,Wartosc NVARCHAR(100) NOT NULL UNIQUE
	,Kod NVARCHAR(15) UNIQUE
	,CzyAktywny tinyint NOT NULL
)


CREATE TABLE Jezyk (
	JezykId int IDENTITY(1,1) PRIMARY KEY
	,Wartosc NVARCHAR(100) NOT NULL UNIQUE
	,Kod NVARCHAR(15) UNIQUE
	,CzyAktywny tinyint NOT NULL
)

CREATE TABLE Katedra (
	KatedraId int IDENTITY(1,1) PRIMARY KEY
	,Wartosc NVARCHAR(100) NOT NULL UNIQUE
	,Kod NVARCHAR(15) UNIQUE
	,CzyAktywny tinyint NOT NULL
)

CREATE TABLE RolaPracownikaWKatedrze (
	RolaPracownikaWKatedrzeId int IDENTITY(1,1) PRIMARY KEY
	,Wartosc NVARCHAR(100) NOT NULL UNIQUE
	,Kod NVARCHAR(15) UNIQUE
	,CzyAktywny tinyint NOT NULL
)

CREATE TABLE KategoriaPracownika (
	KategoriaPracownikaId int IDENTITY(1,1) PRIMARY KEY
	,Wartosc NVARCHAR(100) NOT NULL UNIQUE
	,Kod NVARCHAR(15) UNIQUE
	,CzyAktywny tinyint NOT NULL
)

CREATE TABLE StanowiskoPracownika (
	StanowiskoPracownikaId int IDENTITY(1,1) PRIMARY KEY
	,Wartosc NVARCHAR(100) NOT NULL UNIQUE
	,Kod NVARCHAR(15) UNIQUE
	,CzyAktywny tinyint NOT NULL
)
CREATE TABLE RodzajUmowyPracownika (
	RodzajUmowyPracownikaId int IDENTITY(1,1) PRIMARY KEY
	,Wartosc NVARCHAR(100) NOT NULL UNIQUE
	,Kod NVARCHAR(15) UNIQUE
	,CzyAktywny tinyint NOT NULL
)


CREATE TABLE TytulNaukowy (
	TytulNaukowyId int IDENTITY(1,1) PRIMARY KEY
	,Wartosc NVARCHAR(100) NOT NULL UNIQUE
	,Kod NVARCHAR(15) UNIQUE
	,CzyAktywny tinyint NOT NULL
)

CREATE TABLE TypPrzedmiotu (
	TypPrzedmiotuId int IDENTITY(1,1) PRIMARY KEY
	,Wartosc NVARCHAR(100) NOT NULL UNIQUE
	,Kod NVARCHAR(15) UNIQUE
	,CzyAktywny tinyint NOT NULL
)

CREATE TABLE FormaZaliczenia (
	FormaZaliczeniaId int IDENTITY(1,1) PRIMARY KEY
	,Wartosc NVARCHAR(100) NOT NULL UNIQUE
	,Kod NVARCHAR(15) UNIQUE
	,CzyAktywny tinyint NOT NULL
)

CREATE TABLE FormaStudiow (
	FormaStudiowId int IDENTITY(1,1) PRIMARY KEY
	,Wartosc NVARCHAR(100) NOT NULL UNIQUE
	,Kod NVARCHAR(15) UNIQUE
	,CzyAktywny tinyint NOT NULL
)

CREATE TABLE KierunekStudiow (
	KierunekStudiowId int IDENTITY(1,1) PRIMARY KEY
	,Wartosc NVARCHAR(100) NOT NULL UNIQUE
	,Kod NVARCHAR(15) UNIQUE
	,CzyAktywny tinyint NOT NULL
)

CREATE TABLE TypProramu (
	TypProgramuId int IDENTITY(1,1) PRIMARY KEY
	,Wartosc NVARCHAR(100) NOT NULL UNIQUE
	,Kod NVARCHAR(15) UNIQUE
	,CzyAktywny tinyint NOT NULL
)
CREATE TABLE StatusRaportu (
	StatusRaportuId int IDENTITY(1,1) PRIMARY KEY
	,Wartosc NVARCHAR(100) NOT NULL UNIQUE
	,Kod NVARCHAR(15) UNIQUE
	,CzyAktywny tinyint NOT NULL
)
--
CREATE TABLE Osoba (
	OsobaId int IDENTITY(1,1) PRIMARY KEY CLUSTERED (OsobaId ASC)
	,Imie NVARCHAR(50) NOT NULL
	,Nazwisko NVARCHAR(70) NOT NULL
	,StanAktualnyId int 
	,ObywatelstwoId int REFERENCES Obywatelstwo(ObywatelstwoId)
	,UzytkownikId int
	,NumerEwidencyjny NVARCHAR(15)
	,Plec CHAR(1)
	,CzyStanAktualny tinyint
);

CREATE TABLE Student (
	StudentId int IDENTITY(1,1) PRIMARY KEY CLUSTERED (StudentId ASC)
	,OsobaId int NOT NULL UNIQUE REFERENCES Osoba(OsobaId) ON DELETE NO ACTION ON UPDATE NO ACTION
	,Email NVARCHAR (70) NOT NULL 
	,NrAlbumu CHAR(10) NOT NULL UNIQUE
	,DataWydania DATE NOT NULL
);
CREATE INDEX Student_email_idx ON Student(Email);

CREATE TABLE Dydaktyk (
	DydaktykId int IDENTITY(1,1) PRIMARY KEY CLUSTERED (DydaktykId ASC)
	,OsobaId int NOT NULL UNIQUE REFERENCES Osoba(OsobaId) ON DELETE NO ACTION ON UPDATE NO ACTION
	,Email NVARCHAR (70) NOT NULL 
	,TytulNaukowyId int NOT NULL REFERENCES TytulNaukowy(TytulNaukowyId)
);
CREATE INDEX Dydaktyk_email_idx ON Dydaktyk(Email);


CREATE TABLE Pracownik (
	PracownikId int IDENTITY(1,1) PRIMARY KEY CLUSTERED (PracownikId ASC)
	,DydaktykId int NOT NULL UNIQUE REFERENCES Dydaktyk(DydaktykId) ON DELETE NO ACTION ON UPDATE NO ACTION
	,StanowiskoPracownikaId int NOT NULL REFERENCES StanowiskoPracownika(StanowiskoPracownikaId)
	,RodzajUmowyPracownikaId int NOT NULL REFERENCES RodzajUmowyPracownika(RodzajUmowyPracownikaId)
	,KatedraId int NOT NULL REFERENCES Katedra(KatedraId)
	,RolaPracownikaWKatedrzeId int NOT NULL REFERENCES RolaPracownikaWKatedrze(RolaPracownikaWKatedrzeId)
	,ZatrudnionyOd DATE NOT NULL
	,ZatrudnionyDo DATE --NULL oznacza bezterminowo
);

CREATE TABLE PracownikKategoriaPracownika (
	PracownikId int NOT NULL REFERENCES Pracownik(PracownikId)
	,KategoriaPracownikaId int NOT NULL REFERENCES RolaPracownikaWKatedrze(RolaPracownikaWKatedrzeId)
	,ZakwalifikowanyOdRoku date NOT NULL
	CONSTRAINT PracownikKategoriaPracownika_PK PRIMARY KEY CLUSTERED (PracownikId, KategoriaPracownikaId, ZakwalifikowanyOdRoku ASC)
);


CREATE TABLE Gosc (
	GoscId int IDENTITY(1,1) PRIMARY KEY CLUSTERED (GoscId ASC)
	,DydaktykId int NOT NULL UNIQUE REFERENCES Dydaktyk(DydaktykId) ON DELETE NO ACTION ON UPDATE NO ACTION
);

CREATE TABLE Doktorant (
	DoktorantId int IDENTITY(1,1) PRIMARY KEY CLUSTERED (DoktorantId ASC)
	,DydaktykId int NOT NULL UNIQUE REFERENCES Dydaktyk(DydaktykId) ON DELETE NO ACTION ON UPDATE NO ACTION
	,PromotorId int REFERENCES Pracownik(PracownikId) ON DELETE SET NULL
	,PromotorZastepcaId int REFERENCES Pracownik(PracownikId) --ON DELETE SET NULL
);
CREATE INDEX Doktorant_promotor_idx ON Doktorant(PromotorId);

CREATE TABLE KartaPrzedmiotu (
	KartaPrzedmiotuId int IDENTITY(1,1) PRIMARY KEY CLUSTERED (KartaPrzedmiotuId)
	--Kasia musi wstawic swoje rzeczy
);

CREATE TABLE PrzedmiotowyEfektKsztalcenia (
	PekId INT IDENTITY (1,1)			PRIMARY KEY CLUSTERED(PekId)
	,TypEfektu CHAR(1) NOT NULL  CHECK (TypEfektu in ('W', 'K', 'U'))
	,Opis NVARCHAR (100) NOT NULL
	,JezykId int NOT NULL				REFERENCES  Jezyk(JezykId)
);


CREATE TABLE KierunkowyEfektKsztalcenia (
	KekId INT IDENTITY (1,1)			PRIMARY KEY CLUSTERED(KekId)
	,TypEfektu CHAR(1) NOT NULL CHECK (TypEfektu in ('W', 'K', 'U'))
	,Opis NVARCHAR (100) NOT NULL
	,JezykId int NOT NULL				REFERENCES  Jezyk(JezykId)
)

CREATE TABLE ObszarowyEfektKsztalcenia (
	OekId INT IDENTITY (1,1)			PRIMARY KEY CLUSTERED(OekId)
	,Opis NVARCHAR (100) NOT NULL
);

CREATE TABLE KEKSpelniaOEK (
	OekId INT NOT NULL					REFERENCES ObszarowyEfektKsztalcenia (OekId)
	,KekId INT NOT NULL					REFERENCES KierunkowyEfektKsztalcenia (KekId)
	CONSTRAINT KEKSpelniaOEK_PK			PRIMARY KEY (KekId, OekId)
)

CREATE TABLE PEKSpelniaKEK (
	PracownikId INT NOT NULL			REFERENCES Pracownik (PracownikId)
	,PekId INT NOT NULL					REFERENCES PrzedmiotowyEfektKsztalcenia (PekId)
	,KekId INT NOT NULL					REFERENCES KierunkowyEfektKsztalcenia (KekId)
	CONSTRAINT PEKSpelniaKEK_PK			PRIMARY KEY (PracownikId, PekId, KekId)
)
--

CREATE TABLE FormaPrzedmiotu (
	FormaPrzedmiotuId int IDENTITY(1,1) PRIMARY KEY CLUSTERED (FormaPrzedmiotuId ASC) --tutaj mam spore watpliwosci czy nie uzyc klucza zlozonego
	,KartaPrzedmiotuId int NOT NULL 	REFERENCES KartaPrzedmiotu(KartaPrzedmiotuId)
	,TypPrzedmiotuId int NOT NULL		REFERENCES TypPrzedmiotu(TypPrzedmiotuId)
	,FormaZaliczeniaId int NOT NULL 	REFERENCES FormaZaliczenia(FormaZaliczeniaId)
	,ZZU int NOT NULL
	,CNPS int NOT NULL
	,CzyKursKoncowy tinyint
	,ECTS int NOT NULL
	,PunktyPrakt CHAR(10)
	,PunktyBK CHAR (10)

);
CREATE UNIQUE INDEX FormaPrzedmiotu_alt_uidx ON Formaprzedmiotu(kartaPrzedmiotuId, TypPrzedmiotuId);

CREATE TABLE MacierzPowiazan (
	KartaPrzedmiotuId int NOT NULL		REFERENCES KartaPrzedmiotu(KartaPrzedmiotuId)
	,PekId INT NOT NULL					REFERENCES PrzedmiotowyEfektKsztalcenia (PekId)
	,NrPek smallint NOT NULL
	,CONSTRAINT MaciezPowiazan_PK		PRIMARY KEY (KartaPrzedmiotuId, PekId)
);

CREATE TABLE Grupa (
	Grupaid INT IDENTITY(1,1)			PRIMARY KEY CLUSTERED (GrupaId ASC)
	,Kod CHAR(7) NOT NULL
	,Rok INT NOT NULL
	,Semestr CHAR(1) NOT NULL
	,FormaPrzedmiotuId INT NOT NULL		REFERENCES FormaPrzedmiotu(FormaPrzedmiotuId)
	,DydaktykId INT NOT NULL			REFERENCES Dydaktyk(DydaktykId)
);
CREATE UNIQUE INDEX Grupa_alt_uidx ON Grupa(Kod, Rok, Semestr);
CREATE INDEX Grupa_FormaPrzedmiotu_idx ON Grupa(FormaPrzedmiotuId, Rok, Semestr);

CREATE TABLE DydaktyktycyPomocniczy (
	DydaktykId INT NOT NULL				REFERENCES Dydaktyk(DydaktykId)
	,GrupaId INT NOT NULL				REFERENCES Grupa(GrupaId)
	,CONSTRAINT DydaktyktycyPomocniczy_PK PRIMARY KEY CLUSTERED (GrupaId, DydaktykId)
)

CREATE TABLE DydaktykZastępstwoWGrupie (
	DydaktykId INT NOT NULL					REFERENCES Dydaktyk(DydaktykId)
	,GrupaId INT NOT NULL				REFERENCES Grupa(GrupaId)
	,DataOd DATE NOT NULL
	,DataDo DATE 
	,CONSTRAINT DydaktykZastępstwoWGrupie_PK PRIMARY KEY CLUSTERED (DydaktykId, GrupaId)
)

CREATE TABLE StudentZapisanyDoGrupy (
	GrupaId INT NOT NULL				REFERENCES Grupa(GrupaId)
	,StudentId INT NOT NULL				REFERENCES Student(StudentId)
	,ocena1 INT CHECK (ocena1 IN (NULL, 2, 3, 3.5, 4, 4.5, 5, 5.5))
	,data1 DATE 
	,ocena2 INT CHECK (ocena2 IN (NULL, 2, 3, 3.5, 4, 4.5, 5, 5.5))
	,data2 DATE
	,uwagi NVARCHAR(200)
	,CzyZaliczenie tinyint NOT NULL
	--pytanie czy nie podac referencji do FRP mozliwe ,ze bedzie to konieczne
	,CONSTRAINT StudentZapisanyDoGrupy_PK PRIMARY KEY CLUSTERED (GrupaId, StudentId)
)

CREATE TABLE StudentBrakZaliczeniaPEK (
	GrupaId INT NOT NULL				REFERENCES Grupa(GrupaId)
	,StudentId INT NOT NULL				REFERENCES Student(StudentId)
	,PekId INT NOT NULL					REFERENCES PrzedmiotowyEfektKsztalcenia(PekId)
	,Uwagi NVARCHAR(200)
	,CONSTRAINT StudentZapisanyDoGrupy_FK FOREIGN KEY (GrupaId, StudentId) REFERENCES  StudentZapisanyDoGrupy (GrupaId, StudentId) 
	,CONSTRAINT StudentBrakZaliczeniaPEK_PK PRIMARY KEY CLUSTERED (GrupaId, StudentId, PekId)
)



CREATE TABLE StudiaNaKierunku (
	StudiaNaKierunkuId int IDENTITY(1,1) PRIMARY KEY CLUSTERED (StudiaNaKierunkuId ASC)
	,PoczatekCykluKsztalcenia int --powinno byc chyba not null ale co w momencie definiowania
	,FormaStudiowId int NOT NULL REFERENCES FormaStudiow(FormaStudiowId)
	,Stopien int CHECK (stopien in (1,2)) 
	,KierunekStudiowId  int NOT NULL REFERENCES KierunekStudiow(KierunekStudiowId)
);

CREATE TABLE ProgramKierunku (
	StudiaNaKierunkuId int NOT NULL REFERENCES StudiaNaKierunku(StudiaNaKierunkuId)
	,KartaPrzedmiotuId int NOT NULL REFERENCES KartaPrzedmiotu(KartaPrzedmiotuId)
	,TypProgramu CHAR(1) NOT NULL CHECK (TypProgramu in('O','K','S'))
	,Semestr tinyint NOT NULL
	,CONSTRAINT ProgramKierunku_PK PRIMARY KEY (StudiaNaKierunkuId, KartaPrzedmiotuId) 
);

CREATE TABLE StudiaNaKierunkuRealizujaKEK (
	StudiaNaKierunkuId int NOT NULL REFERENCES StudiaNaKierunku(StudiaNaKierunkuId)
	,KekId int NOT NULL REFERENCES KierunkowyEfektKsztalcenia(KekId)
	,CONSTRAINT StudiaNaKierunkuRealizujaKEK_PK PRIMARY KEY (StudiaNaKierunkuId, KekId)
);

CREATE TABLE StudentStudiuje (
	StudentId int NOT NULL REFERENCES  Student(StudentId)
	,StudiaNaKierunkuId int NOT NULL REFERENCES  StudiaNaKierunku(StudiaNaKierunkuId)
	CONSTRAINT StudentStudiuje_PK PRIMARY KEY (StudentId, StudiaNaKierunkuId)
);

CREATE TABLE FRP (
	FormaPrzedmiotuId INT NOT NULL REFERENCES FormaPrzedmiotu(FormaPrzedmiotuId)
	,DydaktykId INT NOT NULL REFERENCES Dydaktyk(DydaktykId)
	,StudiaNaKierunkuId int NOT NULL REFERENCES StudiaNaKierunku(StudiaNaKierunkuId)
	,Rok INT NOT NULL
	,Semestr CHAR(1) NOT NULL
	,Termin1 DATE --pytanie o checka w przypadku kiedy przedmiot zakonczony egzaminem 
	,Termin2 DATE
	,KartaPrzedmiotuId INT NOT NULL REFERENCES KartaPrzedmiotu (KartaPrzedmiotuId) --zbedne ale uwalnia nas od jednego joina
	,DataUtworzenia DATE NOT NULL
	,DataModyfikacji DATE
	,DataZatwierdzenia DATE
	,Sekcja3 NVARCHAR(MAX)
	,Sekcja4 NVARCHAR(MAX)
	,Sekcja5 NVARCHAR(MAX)
	,StatusRaportuId int NOT NULL REFERENCES StatusRaportu(StatusRaportuId)
	--,status CHAR(1) CHECK (status in ('U','M','Z')) --U- utworzony, M- zmodyfikowany, Z-zatweirdzony
	,CONSTRAINT FRP_PK PRIMARY KEY CLUSTERED (FormaPrzedmiotuId, Rok, Semestr, DydaktykId, StudiaNaKierunkuId) 
)
CREATE INDEX FRP_Dydaktyk_idx ON FRP(DydaktykId, Rok, Semestr);

CREATE TABLE SROP (
	KartaPrzedmiotuId INT NOT NULL REFERENCES KartaPrzedmiotu(KartaPrzedmiotuId)
	,PracownikId INT NOT NULL REFERENCES Pracownik(PracownikId)
	,StudiaNaKierunkuId int NOT NULL REFERENCES StudiaNaKierunku(StudiaNaKierunkuId)
	,Rok INT NOT NULL
	,Semestr CHAR(1) NOT NULL
	,DataUtworzenia DATE NOT NULL
	,DataModyfikacji DATE
	,DataZatwierdzenia DATE

	,Sekcja1 NVARCHAR(MAX)
	,Sekcja2 NVARCHAR(MAX)
	,Sekcja3 NVARCHAR(MAX)
	,Sekcja4 NVARCHAR(MAX)
	,Sekcja5 NVARCHAR(MAX)
	,StatusRaportuId int NOT NULL REFERENCES StatusRaportu(StatusRaportuId)
	,CONSTRAINT SROP_PK PRIMARY KEY CLUSTERED (KartaprzedmiotuId, Rok, Semestr, StudiaNaKierunkuId) 
);
CREATE INDEX SROP_Opiekun_idx ON SROP(PracownikId, Rok, Semestr);
