//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Wspomaganie_dziekanatu.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Wydawca
    {
        public Wydawca()
        {
            this.CytataWydawnicza = new HashSet<CytataWydawnicza>();
            this.Literatura = new HashSet<Literatura>();
        }
    
        public int Id { get; set; }
        public string Wartosc { get; set; }
        public string Kod { get; set; }
        public byte CzyAktywny { get; set; }
    
        public virtual ICollection<CytataWydawnicza> CytataWydawnicza { get; set; }
        public virtual ICollection<Literatura> Literatura { get; set; }
    }
}
