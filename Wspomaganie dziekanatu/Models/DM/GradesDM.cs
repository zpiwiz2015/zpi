﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Wspomaganie_dziekanatu.Models.VM;

namespace Wspomaganie_dziekanatu.Models.DM
{
    public class GradesDM
    {
        public G_Zaliczenie Oceny { get; set; }
        public List<Obecnosc> Obecnosci { get; set; }
    }

}