﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wspomaganie_dziekanatu.Models.VM
{
    public class frpVM
    {
        public FRP frp { get; set; }
        public bool Zaliczenie { get; set; }
        public string KodKursu { get; set; }
        public string NazwaKursu { get; set; }

        public List<RealizacjaPEK> RealizacjaPEK_T1 { get; set; }
        public List<RealizacjaPEK> RealizacjaPEK_T2 { get; set; }
        public List<FrpOceny> Oceny { get; set; }
        public List<FrpOceny> Oceny_T1 { get; set; }
        public List<FrpOceny> Oceny_T2 { get; set; }
        public List<DaneStatystyczne> DaneStatystyczne { get; set; }
    }

    public class FormyPrzedmiotuFRP
    {
        
    }

    public class FrpOceny
    {
        public int IdGrupy { get; set; }
        public string KodGrupy { get; set; }
        public string NazwaKursu { get; set; }
        public int ZapisanychNaKurs { get; set; }
        public int UczestniczacychWEgzaminie { get; set; }
        public int NieuczestniczacychWEgzaminie { get; set; }
        public int L6 { get; set; }
        public int L5 { get; set; }
        public int L4 { get; set; }
        public int L3 { get; set; }
        public int L2 { get; set; }
        public int L1 { get; set; }
        public int L0 { get; set; }
        public int L { get; set; }
    }

    public class RealizacjaPEK
    {
        public int GrupaId { get; set; }
        public string KodGrupy { get; set; }
        public int PEKId { get; set; }
        public string NazwaPEK { get; set; }
        public int Osiagneli { get; set; }
        public int Nieosiagneli { get; set; }
    }

    public class DaneStatystyczne
    {
        public int GrupaId { get; set; }
        public string KodGrupy { get; set; }
        public string NazwaKursu { get; set; }
        public int TerminZajec { get; set; }
        public double OcenaSredniaEgzamin { get; set; }
        public double OdchylenieStandardowe { get; set; }
        public double OcenaSredniaZapisanych { get; set; }
        public double RzeczywistyWskaznikZawartosci { get; set; }
        public double WskaznikZdawalnosci { get; set; }
    }
}
