﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wspomaganie_dziekanatu.Models.VM
{
    public class PowiadomienieProsteVM
    {
        public int PowiadomienieId { get; set; }
        public String URL { get; set; }

        public int Status { get; set; }
        public String Tresc { get; set; }
    }

}