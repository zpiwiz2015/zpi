﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wspomaganie_dziekanatu.Models.VM
{
    public class ListaRaportowVM
    {
        public string NazwaKursu { get; set; }
        public int IdKursu { get; set; }
        public List<ListaRaportow_grupy> ListaGrup { get; set; }
        public bool IstniejeSROP { get; set; }
        public bool PrzekazanySROP { get; set; }
        public bool OdpowiedzialnyZaSROP { get; set; }
    }

    public class ListaRaportow_grupy
    {
        public string TypPrzedmiotu { get; set; }
        public int DzienTygodnia { get; set; }
        public string Godzina { get; set; }
        public string KodGrupy { get; set; }
        public bool IstniejeFRP { get; set; }
        public bool PrzekazaneFRP { get; set; }
    }
}