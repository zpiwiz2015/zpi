﻿using Wspomaganie_dziekanatu.Helpers.Enums;

namespace Wspomaganie_dziekanatu.Models.VM
{
    public class ListaGrupVM
    {
        public string TypPrzedmiotu { get; set; }
        public string DzienTygodnia { get; set; }
        public string Godzina { get; set; }
        public string KodGrupy { get; set; }
        public int IdKursu { get; set; }
        public string NazwaKursu { get; set; }
        public int IdGrupy { get; set; }

        public int FormaPrzedmiotuId { get; set; }
    }
}