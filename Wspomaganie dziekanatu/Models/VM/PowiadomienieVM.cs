﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wspomaganie_dziekanatu.Models.VM
{
    public class PowiadomienieVM
    {
        public int PowiadomienieId { get; set; }
        public int NadawcaId { get; set; }
        public int OdbiorcaId { get; set; }
        public DateTime DataWyslania { get; set; }
        public String DataGodzina { get; set; }
        public String DataDzien { get; set; }
        public String DataMiesiac { get; set; }
        public String DataRok { get; set; }

        public String URL { get; set; }

        public int Status { get; set; }
        public String Tresc { get; set; }
    }

}