﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Wspomaganie_dziekanatu.Helpers;

namespace Wspomaganie_dziekanatu.Models.VM
{
    public class GradesVM
    {
        public G_Student Student { get; set; }
        public G_Zaliczenie Zaliczenie { get; set; }
        public List<Obecnosc> Obecnosci { get; set; }

        public List<String> Terminy { get; set; }
    }


    public class G_Student
    {
        public String StudentId { get; set; }
        public String NrAlbumu { get; set; }
        public String Nazwisko { get; set; }
        public String Imie { get; set; }
    }

    public class G_Zaliczenie
    {
        [CheckGrade]
        public Nullable<decimal> Ocena1 { get; set; }

        [CheckGrade]
        public Nullable<decimal> Ocena2 { get; set; }

        public int GrupaId { get; set; }
        public int StudentId { get; set; }
    }

    public class G_Obecnosc
    {
        public bool CzyObecny { get; set; }
    }
}