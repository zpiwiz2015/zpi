﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Wspomaganie_dziekanatu.Helpers.Enums;

namespace Wspomaganie_dziekanatu.Models.VM
{
    public class DashboardVM
    {
        public List<D_Obecnosci> Obecnosci { get; set; }
        public List<D_Raporty> Raporty { get; set; }
        public List<ListaGrupVM> ListaGrup { get; set; }
    }

    public class D_Obecnosci
    {
        public string NazwaKursu { get; set; }
        public string TypKursu { get; set; }
        public string Godzina { get; set; }
        public int IdKursu { get; set; }
        public int IdGrupy { get; set; }
    }

    public class D_Raporty
    {
        public string NazwaKursu { get; set; }
        public string KodKursu { get; set; }
        public int IdKursu { get; set; }
    }
}