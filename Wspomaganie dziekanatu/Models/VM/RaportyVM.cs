﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wspomaganie_dziekanatu.Models.VM
{
    public class RaportyVM
    {
        public bool IstniejeSROP { get; set; }
        public bool PrzekazanySROP { get; set; }
        public bool CzyOdpowiedzialnyZaSROP { get; set; }
        public int KursId { get; set; }
        public string NazwaKursu { get; set; }
        public bool PrzekazaneWszystkieFRP { get; set; }
        public List<RaportyVM_formyPrzedmiotu> ListaFormPrzedmiotu { get; set; }

    }

    public class RaportyVM_formyPrzedmiotu
    {
        public int IdFormyPrzedmiotu { get; set; }
        public string NazwaFormyPrzedmiotu { get; set; }
        public string DzienTygodnia { get; set; }
        public string Godzina { get; set; }
        public string KodGrupy { get; set; }
        public bool PrzekazanyFRP { get; set; }
        public bool IstniejeFRP { get; set; }
    }
}