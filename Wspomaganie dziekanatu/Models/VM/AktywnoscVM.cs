﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wspomaganie_dziekanatu.Models.VM
{
    public class AktywnoscVM
    {
        public String URL { get; set; }
        public String Opis { get; set; }
        public DateTime Data { get; set; }
        public String DataGodzina { get; set; }
        public String DataDzien { get; set; }
        public String DataMiesiac { get; set; }
        public String DataRok { get; set; }
        public int UzytkownikId { get; set; }
    }

}