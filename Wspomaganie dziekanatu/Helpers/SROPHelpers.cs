﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Wspomaganie_dziekanatu.Models;

namespace Wspomaganie_dziekanatu.Helpers
{
    public static class SROPHelpers
    {
        public static void DodajRaportSROP(int pracownikId, int kartaPrzedmiotu, int rok, int semestr, string sekcja1, string skcja2, string sekcja3, string sekcja4, string sekcja5, int studiaNaKierunku)
        {
            using (var db = new ZpiDbCtx())
            {
                var srop = new SROP
                {
                    PracownikId = pracownikId,
                    KartaPrzedmiotuId = kartaPrzedmiotu,
                    Rok = rok,
                    Semestr = semestr,
                    Sekcja1 = sekcja1,
                    Sekcja2 = skcja2,
                    Sekcja3 = sekcja3,
                    Sekcja4 = sekcja4,
                    Sekcja5 = sekcja5,
                    StudiaNaKierunkuId = studiaNaKierunku,
                    StatusRaportuId = (int)StatusRaportuEnum.nieoddane,
                    DataUtworzenia = DateTime.Now,
                };
                db.SROP.Add(srop);
                db.SaveChanges();
            }
        }

        public static void ZaktualizujSROP(SROP srop)
        {
            using (var db = new ZpiDbCtx())
            {
                srop.DataModyfikacji = DateTime.Now;
                db.SROP.Where(
                    e =>
                        e.KartaPrzedmiotuId == srop.KartaPrzedmiotuId && e.Rok == srop.Rok && e.Semestr == srop.Semestr &&
                        e.StudiaNaKierunkuId == srop.StudiaNaKierunkuId).FirstOrDefault();// = srop;
                db.SaveChanges();
            }
        }

        public static void ZatwierdzSROP(SROP srop, StatusRaportuEnum status)
        {
            using (var db = new ZpiDbCtx())
            {
                var sropEntity = db.SROP.Where(e =>
                    e.KartaPrzedmiotuId == srop.KartaPrzedmiotuId && e.Rok == srop.Rok && e.Semestr == srop.Semestr &&
                    e.StudiaNaKierunkuId == srop.StudiaNaKierunkuId).FirstOrDefault();
                sropEntity.StatusRaportuId = (int) StatusRaportuEnum.oddane;
                sropEntity.DataZatwierdzenia = DateTime.Now;
                db.SaveChanges();
            }
        }

        public static SROP WybierzSROP(int kartaPrzedmiotuId, int rok, int semestr, int studiaNakierunkuId)
        {
            using (var db = new ZpiDbCtx())
            {
                return
                    db.SROP.FirstOrDefault(
                        e =>
                            e.KartaPrzedmiotuId == kartaPrzedmiotuId && e.Rok == rok && e.Semestr == semestr &&
                            e.StudiaNaKierunkuId == studiaNakierunkuId);
            }
        }
    }
}