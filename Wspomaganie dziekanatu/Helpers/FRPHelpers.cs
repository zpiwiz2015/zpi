﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Wspomaganie_dziekanatu.Models;

namespace Wspomaganie_dziekanatu.Helpers
{
    public static class FRPHelpers
    {
        public static void DodajNowyRaportFRP(int formaPrzedmiotu, int rok, int semestr, DateTime? termin1, DateTime? termin2, string sekscja3, string sekcja4, string sekcja5, int studiaNaKierunku, int dydaktykId, StatusRaportuEnum statusRaportu)
        {
            using (var db = new ZpiDbCtx())
            {
                var frp = new FRP
                {
                    FormaPrzedmiotuId = formaPrzedmiotu,
                    Rok = rok,
                    Semestr = semestr,
                    Termin1 = termin1,
                    Termin2 = termin2,
                    Sekcja3 = sekscja3,
                    Sekcja4 = sekcja4,
                    Sekcja5 = sekcja5,
                    StudiaNaKierunkuId = studiaNaKierunku,
                    DydaktykykId = dydaktykId,
                    StatusRaportuId = (int)statusRaportu,
                    DataUtworzenia = DateTime.Now
                };
                db.FRP.Add(frp);
                db.SaveChanges();
            }
        }

        public static void ZaktualizujFRP(FRP frp)
        {
            frp.DataModyfikacji = DateTime.Now;
            using (var db = new ZpiDbCtx())
            {
                db.FRP.Where(
                    e =>
                        e.FormaPrzedmiotuId == frp.FormaPrzedmiotuId && e.Rok == frp.Rok && e.Semestr == frp.Semestr &&
                        e.StudiaNaKierunkuId == frp.StudiaNaKierunkuId).FirstOrDefault();// = frp;
                db.SaveChanges();
            }
        }

        public static void ZatwierdzSROP(FRP frp)
        {
            using (var db = new ZpiDbCtx())
            {
                var frpEntity = db.FRP.Where(e =>
                    e.FormaPrzedmiotuId == frp.FormaPrzedmiotuId && e.Rok == frp.Rok && e.Semestr == frp.Semestr &&
                    e.FormaPrzedmiotuId == frp.FormaPrzedmiotuId).FirstOrDefault();
                frpEntity.StatusRaportuId = (int) StatusRaportuEnum.oddane;
                frpEntity.DataZatwierdzenia = DateTime.Now;
                db.SaveChanges();
            }
        }

        public static FRP WybierzFRP(int formaPrzedmiotuId, int rok, int semestr, int studiaNaKierunkuId)
        {
            using (var db = new ZpiDbCtx())
            {
                return
                    db.FRP.FirstOrDefault(
                        e =>
                            e.FormaPrzedmiotuId == formaPrzedmiotuId && e.Rok == rok && e.Semestr == semestr &&
                            e.StudiaNaKierunkuId == studiaNaKierunkuId);
            }
        }
    }
}