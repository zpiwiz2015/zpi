﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Wspomaganie_dziekanatu.Helpers
{
    public class CheckGradeAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid
	    (object value, ValidationContext validationContext)
        {
	        decimal[] grades = {5.5M, 5.0M, 4.5M, 4.0M, 3.5M, 3.0M, 2.0M};
	        string errorMessage = "Nieprawidłowa ocena.";

	        if (value != null && !grades.Contains((decimal)value)) {
                return new ValidationResult(errorMessage);
            }
	        return ValidationResult.Success;
        }
    }
}