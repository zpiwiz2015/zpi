﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Web;
using Wspomaganie_dziekanatu.Models;
using Wspomaganie_dziekanatu.Models.VM;

namespace Wspomaganie_dziekanatu.Helpers
{
    public static class AktywnoscHelpers
    {
        public static List<AktywnoscVM> getUserActivities(System.Security.Principal.IPrincipal user)
        {
            return getUserActivities(user, true);
        }
        public static List<AktywnoscVM> getUserActivities(System.Security.Principal.IPrincipal user, bool all) {
            List<AktywnoscVM> l = new List<AktywnoscVM>();
            using (var db = new ZpiDbCtx())
            {
                var userId = Helpers.User.ZalogowanyId(user);

                var activities = db.Aktywnosc.Where(e => e.UzytkownikId == userId);
                if (!all)
                {
                    activities.Take(10);
                }

                foreach (var activity in activities)
                {
                    var date = (DateTime)activity.DataPowstania;
                    l.Add(new AktywnoscVM()
                    {
                        UzytkownikId = activity.UzytkownikId,
                        Data = date,
                        DataMiesiac = CultureInfo.CurrentCulture.DateTimeFormat.MonthNames[(int)date.Month - 1],
                        DataRok = date.Year.ToString(),
                        DataDzien = date.Day.ToString(),
                        DataGodzina = String.Format("{0:t}", date),
                        URL = activity.URLAkcji,
                        Opis = activity.Opis
                    });
                }
            }
            l.Reverse();
            return l;
        }

        public static void addActivity(AktywnoscVM aktywnosc)
        {
            using (var db = new ZpiDbCtx())
            {
                Aktywnosc a = new Aktywnosc();
                a.DataPowstania = aktywnosc.Data;
                a.URLAkcji = aktywnosc.URL;
                a.Opis = aktywnosc.Opis;
                a.UzytkownikId = aktywnosc.UzytkownikId;
                a.IdentyfikatorZrodla = Guid.NewGuid();

                db.Aktywnosc.Add(a);
                db.SaveChanges();
            }
        }

    }
}