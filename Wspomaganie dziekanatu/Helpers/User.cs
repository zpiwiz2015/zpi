﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Wspomaganie_dziekanatu.Helpers.Enums;
using Wspomaganie_dziekanatu.Models;

namespace Wspomaganie_dziekanatu.Helpers
{
    //w kontrolerze wystarczy wpisać: var nazwaUzytkownika = ZalogowanyName(Request);

    public static class User
    {
        public static string ZalogowanyName(System.Security.Principal.IPrincipal user)
        {
            return user.Identity.GetUserName();
        }

        public static int ZalogowanyId(System.Security.Principal.IPrincipal user)
        {
            var userName = user.Identity.GetUserName();
            using (var db = new ZpiDbCtx())
            {
                return db.Uzytkownik.First(e => e.Login == userName).UzytkownikId;
            }
        }

        public static Osoba ZalogowanaOsoba(System.Security.Principal.IPrincipal user)
        {
            var userName = user.Identity.GetUserName();
            using (var db = new ZpiDbCtx())
            {
                return db.Osoba.First(e => e.Uzytkownik.Login == userName);
            }
        }

        public static bool PosiadaRole(RoleEnum rola, System.Security.Principal.IPrincipal user)
        {
            int id = ZalogowanyId(user);
            using (var db = new ZpiDbCtx())
                return
                    db.Uzytkownik.First(e => e.UzytkownikId == id)
                        .RolaUzytkownika.Any(e => e.RolaUzytkownikaId == (int) rola);
        }

        public static int IdDydaktyka(System.Security.Principal.IPrincipal user)
        {
            int userId = ZalogowanyId(user);
            using (var db = new ZpiDbCtx())
            {
                var o = db.Osoba.First(e => e.UzytkownikId == userId);
                return db.Dydaktyk.First(e => e.OsobaId == o.OsobaId).DydaktykId;
            }
        }

    }
}