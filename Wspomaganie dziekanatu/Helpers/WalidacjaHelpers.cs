﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wspomaganie_dziekanatu.Helpers
{
    public static class WalidacjaHelpers
    {
        public static bool ToInt(string napis, out int liczba)
        {
            liczba = 0;
            try
            {
                liczba = Convert.ToInt32(napis);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool CzyDouble(string napis, out double liczba)
        {
            liczba = 0;
            try
            {
                liczba = Convert.ToDouble(liczba);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}