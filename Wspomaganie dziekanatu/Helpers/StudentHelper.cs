﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Wspomaganie_dziekanatu.Models;

namespace Wspomaganie_dziekanatu.Helpers
{
    public static class StudentHelper
    {
        public static void DodajKomentarz(int studentId, int kartaPrzedmiotuId, string tresc)
        {
            using (var db = new ZpiDbCtx())
            {
                db.Komentarz.Add(new Komentarz
                {
                    DataWprowadzenia = DateTime.Now,
                    KartaPrzedmiotuId = kartaPrzedmiotuId,
                    StudentId = studentId,
                    Tresc = tresc
                });
                db.SaveChanges();
            }
        }

        public static List<Komentarz> WyswietlKomentarzeDoKarty(int kartaPrzedmiotuId)
        {
            using (var db = new ZpiDbCtx())
            {
                return db.Komentarz.Where(e => e.KartaPrzedmiotuId==kartaPrzedmiotuId).ToList();
            }
        }

        public static Student WybierzStudenta(int studentId)
        {
            using (var db = new ZpiDbCtx())
            {
                return db.Student.FirstOrDefault(e => e.StudentId == studentId);
            }
        }
    }
}