﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Web;
using Wspomaganie_dziekanatu.Models;
using Wspomaganie_dziekanatu.Models.VM;

namespace Wspomaganie_dziekanatu.Helpers
{
    public static class PowiadomienieHelpers
    {
        public static List<PowiadomienieVM> getUserNotifications(System.Security.Principal.IPrincipal user) {
            List<PowiadomienieVM> l = new List<PowiadomienieVM>();
            using (var db = new ZpiDbCtx())
            {
                var userId = Helpers.User.ZalogowanyId(user);
                foreach (var notification in db.Wiadomosc.Where(e => e.OdbiorcaId == userId))
                {
                    var date = (DateTime)notification.DataNaWyslania;
                    l.Add(new PowiadomienieVM()
                    {
                        PowiadomienieId = notification.WiadomoscId,
                        OdbiorcaId = notification.OdbiorcaId,
                        NadawcaId = notification.NadawcaId,
                        DataWyslania = date,
                        DataMiesiac = CultureInfo.CurrentCulture.DateTimeFormat.MonthNames[(int)date.Month - 1],
                        DataRok = date.Year.ToString(),
                        DataDzien = date.Day.ToString(),
                        DataGodzina = String.Format("{0:t}", date),
                        URL = notification.URLAkcji,
                        Tresc = notification.Wiadomosc1,
                        Status = notification.StatusWiadomosciId
                    });
                }
            }
            l.Reverse();
            return l;
        }

        public static List<PowiadomienieProsteVM> getUserSimpleNotifications(System.Security.Principal.IPrincipal user)
        {
            List<PowiadomienieProsteVM> l = new List<PowiadomienieProsteVM>();
            using (var db = new ZpiDbCtx())
            {
                var userId = Helpers.User.ZalogowanyId(user);
                foreach (var notification in db.Wiadomosc.Where(e => e.OdbiorcaId == userId).OrderByDescending(e => e.StatusWiadomosciId).ThenBy(e => e.DataNaWyslania).Take(5))
                {
                    var date = (DateTime)notification.DataNaWyslania;
                    l.Add(new PowiadomienieProsteVM()
                    {
                        URL = notification.URLAkcji,
                        Tresc = notification.Wiadomosc1,
                        Status = notification.StatusWiadomosciId
                    });
                }
            }
            l.Reverse();
            return l;
        }

        public static int getUserNewNotificationsNumber(System.Security.Principal.IPrincipal user)
        {
            int num = 0;
            using (var db = new ZpiDbCtx())
            {
                var userId = Helpers.User.ZalogowanyId(user);
                num = db.Wiadomosc.Where(e => e.OdbiorcaId == userId && e.StatusWiadomosciId != 4).Count(); /* 4 - "Odczytana" */
            }
            return num;
        }

        public static void markAsRead(System.Security.Principal.IPrincipal user, int id)
        {
            using (var db = new ZpiDbCtx())
            {
                var userId = Helpers.User.ZalogowanyId(user);
                foreach (var notification in db.Wiadomosc.Where(e => e.OdbiorcaId == userId && e.WiadomoscId == id))
                {
                    notification.StatusWiadomosciId = 4; /* 4 - "Odczytana" */
                }
                db.SaveChanges();
            }
        }

        public static void markAllAsRead(System.Security.Principal.IPrincipal user)
        {
            using (var db = new ZpiDbCtx())
            {
                var userId = Helpers.User.ZalogowanyId(user);
                foreach (var notification in db.Wiadomosc.Where(e => e.OdbiorcaId == userId))
                {
                    notification.StatusWiadomosciId = 4; /* 4 - "Odczytana" */
                }
                db.SaveChanges();
            }
        }

        public static void addNotification(PowiadomienieVM notification)
        {
            using (var db = new ZpiDbCtx())
            {
                Wiadomosc w = new Wiadomosc()
                {
                    OdbiorcaId = notification.OdbiorcaId,
                    NadawcaId = notification.NadawcaId, /* 3 - admin */
                    DataNaWyslania = DateTime.Now,
                    URLAkcji = notification.URL,
                    Wiadomosc1 = notification.Tresc,
                    StatusWiadomosciId = 2, /* 2 - "Wysłana"; 4 - "Odczytana" */
                    IdentyfikatorZrodla = Guid.NewGuid()
                };

                db.Wiadomosc.Add(w);
                db.SaveChanges();
                
            }
        }

    }
}