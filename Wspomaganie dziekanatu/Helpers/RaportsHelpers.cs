﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.Model;
using System.Globalization;
using System.Linq;
using System.Web;
using Wspomaganie_dziekanatu.Models;
using Wspomaganie_dziekanatu.Models.VM;

namespace Wspomaganie_dziekanatu.Helpers
{
    public static class RaportsHelpers
    {
        #region raport FRP
        public static bool IstniejeFRP(int kursId, int dydaktykId, int formaPrzedmiotuId)
        {
            using (var db = new ZpiDbCtx())
            {
                return db.KartaPrzedmiotu.FirstOrDefault(e => e.KartaPrzedmiotuId == kursId)
                    .FormaPrzedmiotu.FirstOrDefault().FRP.Any(e => e.DydaktykykId == dydaktykId && e.Semestr==DashboardHelpers.Semestr() && e.Rok==DashboardHelpers.Rok() && e.FormaPrzedmiotuId == formaPrzedmiotuId);
            }
        }

        public static bool PrzekazanyFRP(int kursId, int dydaktykId)
        {
            using (var db = new ZpiDbCtx())
            {
                var karta = db.KartaPrzedmiotu.FirstOrDefault(e => e.KartaPrzedmiotuId == kursId);
                if (karta == null)
                    return false;
                if (!karta.FormaPrzedmiotu.Any())
                    return false;
                var frp = karta.FormaPrzedmiotu.FirstOrDefault().FRP.FirstOrDefault(e=>e.DydaktykykId==dydaktykId);
                if (frp == null)
                    return false;
                return frp.DataZatwierdzenia != null;
            }
        }

        public static frpVM PobierzRaportyFRP(int dydaktykId, int kursId, int formaPrzedmiotuId)
        {
            var wynik = new List<frpVM>();
            using (var db = new ZpiDbCtx())
            {
                var fp = db.FormaPrzedmiotu.First(e => e.FormaPrzedmiotuId == formaPrzedmiotuId);
                if (!IstniejeFRP(kursId, dydaktykId, formaPrzedmiotuId))
                {
                        var grupa =
                            fp.Grupa.FirstOrDefault(
                                e =>
                                    e.DydaktykId == dydaktykId && e.CyklKsztalcenia==DashboardHelpers.CyklKsztalcenia());
                        if (grupa != null)
                        {
                            db.FRP.Add(new FRP
                            {
                                DataUtworzenia = DateTime.Now,
                                DataModyfikacji = DateTime.Now,
                                DydaktykykId = dydaktykId,
                                Rok = DashboardHelpers.Rok(),
                                Semestr = DashboardHelpers.Semestr(),
                                Sekcja3 = "",
                                Sekcja4 = "",
                                Sekcja5 = "",
                                FormaPrzedmiotuId = fp.FormaPrzedmiotuId,
                                StatusRaportuId = 1,
                                StudiaNaKierunkuId = grupa.StudiaNaKierunkuId
                            });
                        }
                    db.SaveChanges();
                }
                var frp = new frpVM
                {
                    NazwaKursu = NazwaKursu_kurs(kursId),
                    KodKursu = fp.KartaPrzedmiotu.KodKursu,
                    frp = fp.FRP.First(e => e.DydaktykykId == dydaktykId),
                    DaneStatystyczne = new List<DaneStatystyczne>(),
                    Oceny = new List<FrpOceny>(),
                    Oceny_T1 = new List<FrpOceny>(),
                    Oceny_T2 = new List<FrpOceny>(),
                    RealizacjaPEK_T1 = new List<RealizacjaPEK>(),
                    RealizacjaPEK_T2 = new List<RealizacjaPEK>(),
                    Zaliczenie = fp.FormaZaliczenia.Wartosc[0] != 'E'
                };
                int iloscOsob = 0;
                foreach (Grupa g in fp.Grupa.Where(e =>
                    e.DydaktykId == dydaktykId && e.CyklKsztalcenia==DashboardHelpers.CyklKsztalcenia()))
                {
                    var studentZapisanyDoGrupy = db.StudentZapisanyDoGrupy.Where(e => e.GrupaId == g.GrupaId);

                    var daneStatystyczne = new DaneStatystyczne();
                    var t1 = frpOceny_T1(kursId, g, frp, studentZapisanyDoGrupy);
                    frp.Oceny_T1.Add(t1);
                    iloscOsob += studentZapisanyDoGrupy.Count();
                    if (!frp.Zaliczenie)
                    {
                        frp.Oceny.Add(PrzeliczFRPOceny(kursId, g, frp, studentZapisanyDoGrupy));
                        var t2 = frpOceny_T2(kursId, g, frp, studentZapisanyDoGrupy);
                        frp.Oceny_T2.Add(t2);
                        double sredniaUczestniczacych = (t2.L6*5.5 + t2.L5*5 + t2.L4*4.5 + t2.L3*4 + t2.L2*3.5 +
                                                            t2.L1*3 +
                                                            t2.L0*2)/t2.UczestniczacychWEgzaminie;
                        frp.DaneStatystyczne.Add(new DaneStatystyczne
                        {
                            GrupaId = g.GrupaId,
                            NazwaKursu = NazwaKursu_kurs(kursId),
                            KodGrupy = g.Kod,
                            OcenaSredniaEgzamin = Math.Round(sredniaUczestniczacych,2),
                            OcenaSredniaZapisanych = Math.Round(
                                (t2.L6*5.5 + t2.L5*5 + t2.L4*4.5 + t2.L3*4 + t2.L2*3.5 + t2.L1*3 + t2.L0*2 +
                                 t2.NieuczestniczacychWEgzaminie*2)/studentZapisanyDoGrupy.Count(),2),
                            OdchylenieStandardowe = Math.Round(
                                Math.Sqrt((Math.Pow(t2.L6*(5.5 - sredniaUczestniczacych), 2) +
                                           Math.Pow(t2.L5*(5 - sredniaUczestniczacych), 2) +
                                           Math.Pow(t2.L4*(4.5 - sredniaUczestniczacych), 2) +
                                           Math.Pow(t2.L3*(4 - sredniaUczestniczacych), 2) +
                                           Math.Pow(t2.L2*(3.5 - sredniaUczestniczacych), 2) +
                                           Math.Pow(t2.L1*(3 - sredniaUczestniczacych), 2) +
                                           Math.Pow(t2.L0*(2 - sredniaUczestniczacych), 2))/
                                          t2.UczestniczacychWEgzaminie),2),
                            RzeczywistyWskaznikZawartosci = Math.Round(((t2.L * 100.0) / t2.UczestniczacychWEgzaminie), 1),
                            WskaznikZdawalnosci = Math.Round(((t2.L * 100.0) / studentZapisanyDoGrupy.Count()), 1)
                        });
                    }
                    else
                    {
                        int zapisanych = studentZapisanyDoGrupy.Count();
                        double sredniaUczestniczacych = Math.Round((t1.L6*5.5 + t1.L5*5 + t1.L4*4.5 + t1.L3*4 + t1.L2*3.5 + t1.L1*3 +
                                                         t1.L0*2)/t1.UczestniczacychWEgzaminie,2);
                        double ocenaSredniaZapisanych = zapisanych == 0
                            ? 0
                            : Math.Round((t1.L6*5.5 + t1.L5*5 + t1.L4*4.5 + t1.L3*4 + t1.L2*3.5 + t1.L1*3 + t1.L0*2 +
                               t1.NieuczestniczacychWEgzaminie*2)/zapisanych,2);
                        double odchylenieStandardowe = t1.UczestniczacychWEgzaminie == 0
                            ? 0
                            : Math.Round(Math.Sqrt((Math.Pow(t1.L6*(5.5 - sredniaUczestniczacych), 2) +
                                         Math.Pow(t1.L5*(5 - sredniaUczestniczacych), 2) +
                                         Math.Pow(t1.L4*(4.5 - sredniaUczestniczacych), 2) +
                                         Math.Pow(t1.L3*(4 - sredniaUczestniczacych), 2) +
                                         Math.Pow(t1.L2*(3.5 - sredniaUczestniczacych), 2) +
                                         Math.Pow(t1.L1*(3 - sredniaUczestniczacych), 2) +
                                         Math.Pow(t1.L0*(2 - sredniaUczestniczacych), 2))/
                                        t1.UczestniczacychWEgzaminie),2);
                        double zWskaznikZdawalnosci = t1.UczestniczacychWEgzaminie == 0
                            ? 0
                            : Math.Round(((t1.L*100.0)/t1.UczestniczacychWEgzaminie), 1);
                        double wskaznikZdawalnosci = zapisanych == 0
                            ? 0
                            : Math.Round(((t1.L * 100.0 )/ zapisanych), 1);

                        frp.DaneStatystyczne.Add(new DaneStatystyczne
                        {
                            GrupaId = g.GrupaId,
                            NazwaKursu = NazwaKursu_kurs(kursId),
                            KodGrupy = g.Kod,
                            OcenaSredniaEgzamin = sredniaUczestniczacych,
                            OcenaSredniaZapisanych = ocenaSredniaZapisanych,
                            OdchylenieStandardowe = odchylenieStandardowe,
                            RzeczywistyWskaznikZawartosci = zWskaznikZdawalnosci,
                            WskaznikZdawalnosci = wskaznikZdawalnosci

                        });
                    }
                }
                foreach (var pek in db.KartaPrzedmiotu.First(e => e.KartaPrzedmiotuId == kursId).MacierzPowiazan)
                {
                    
                    int pekId = pek.PrzedmiotowyEfektKsztalcenia.PekId;
                    string pekNazwa = pek.PrzedmiotowyEfektKsztalcenia.Opis;
                    int nieosiagneli = NieosiagneliPEK(fp.FormaPrzedmiotuId, dydaktykId, pekId);
                    if (!frp.Zaliczenie)
                    {
                        frp.RealizacjaPEK_T1.Add(new RealizacjaPEK
                        {
                            NazwaPEK = pekNazwa,
                            PEKId = pekId,
                            Nieosiagneli = frp.Oceny_T1.Sum(e => e.L),
                            Osiagneli = iloscOsob - frp.Oceny_T1.Sum(e => e.L)
                        });
                        frp.RealizacjaPEK_T2.Add(new RealizacjaPEK
                        {
                            NazwaPEK = pekNazwa,
                            PEKId = pekId,
                            Nieosiagneli = nieosiagneli,
                            Osiagneli = iloscOsob - nieosiagneli
                        });
                    }
                    else
                    {
                        frp.RealizacjaPEK_T1.Add(new RealizacjaPEK
                        {
                            NazwaPEK = pekNazwa,
                            PEKId = pekId,
                            Nieosiagneli = nieosiagneli,
                            Osiagneli = iloscOsob - nieosiagneli
                        });
                    }
                }

                return frp;
            }
        }

        private static int NieosiagneliPEK(int idFP, int idD, int idPEK)
        {
            using (var db = new ZpiDbCtx())
            {
                int nieosiagneli = 0;
                foreach (Grupa g in db.Grupa.Where(e => e.DydaktykId == idD && e.FormaPrzedmiotuId == idFP)) // && e.CyklKsztalcenia == DashboardHelpers.CyklKsztalcenia()))
                {
                    nieosiagneli +=
                        g.StudentZapisanyDoGrupy.Sum(
                            e => e.StudentBrakZaliczeniaPEK.Count(ee => ee.PekId == idPEK));
                }
                return nieosiagneli;
            }

        }

        private static FrpOceny frpOceny_T2(int kursId, Grupa g, frpVM frp, IQueryable<StudentZapisanyDoGrupy> studentZapisanyDoGrupy)
        {
            var frpOceny = new FrpOceny
            {
                NazwaKursu = NazwaKursu_kurs(kursId),
                KodGrupy = g.Kod,
                IdGrupy = g.GrupaId,
                UczestniczacychWEgzaminie =
                    frp.Zaliczenie ? 0 : studentZapisanyDoGrupy.Count(e => e.Ocena2 != null),
                ZapisanychNaKurs = studentZapisanyDoGrupy.Count(),
                NieuczestniczacychWEgzaminie = studentZapisanyDoGrupy.Count() -
                                               studentZapisanyDoGrupy.Count(e => e.Ocena2 != null),
                L = 0,
                L0 = 0,
                L1 = 0,
                L2 = 0,
                L3 = 0,
                L4 = 0,
                L5 = 0,
                L6 = 0
            };
            foreach (var s in studentZapisanyDoGrupy)
            {
                if (s.Ocena2 == 5.5m)
                {
                    frpOceny.L6++;
                    frpOceny.L++;
                }
                else if (s.Ocena2 == 5.0m)
                {
                    frpOceny.L5++;
                    frpOceny.L++;
                }
                else if (s.Ocena2 == 4.5m)
                {
                    frpOceny.L4++;
                    frpOceny.L++;
                }
                else if (s.Ocena2 == 4.0m)
                {
                    frpOceny.L3++;
                    frpOceny.L++;
                }
                else if (s.Ocena2 == 3.5m)
                {
                    frpOceny.L2++;
                    frpOceny.L++;
                }
                else if (s.Ocena2 == 3.0m)
                {
                    frpOceny.L1++;
                    frpOceny.L++;
                }
                else if (s.Ocena2 == 2.0m)
                {
                    frpOceny.L0++;
                }
            }
            return frpOceny;
        }

        private static FrpOceny frpOceny_T1(int kursId, Grupa g, frpVM frp, IQueryable<StudentZapisanyDoGrupy> studentZapisanyDoGrupy)
        {
            var frpOceny = new FrpOceny
            {
                NazwaKursu = NazwaKursu_kurs(kursId),
                KodGrupy = g.Kod,
                IdGrupy = g.GrupaId,
                UczestniczacychWEgzaminie =
                    studentZapisanyDoGrupy.Count(e => e.Ocena1 != null),
                ZapisanychNaKurs = studentZapisanyDoGrupy.Count(),
                NieuczestniczacychWEgzaminie = studentZapisanyDoGrupy.Count() -
                                               studentZapisanyDoGrupy.Count(e => e.Ocena1 != null),
                L = 0,
                L0 = 0,
                L1 = 0,
                L2 = 0,
                L3 = 0,
                L4 = 0,
                L5 = 0,
                L6 = 0
            };
            foreach (var s in studentZapisanyDoGrupy)
            {
                if (s.Ocena1 == 5.5m)
                {
                    frpOceny.L6++;
                    frpOceny.L++;
                }
                else if (s.Ocena1 == 5.0m)
                {
                    frpOceny.L5++;
                    frpOceny.L++;
                }
                else if (s.Ocena1 == 4.5m)
                {
                    frpOceny.L4++;
                    frpOceny.L++;
                }
                else if (s.Ocena1 == 4.0m)
                {
                    frpOceny.L3++;
                    frpOceny.L++;
                }
                else if (s.Ocena1 == 3.5m)
                {
                    frpOceny.L2++;
                    frpOceny.L++;
                }
                else if (s.Ocena1 == 3.0m)
                {
                    frpOceny.L1++;
                    frpOceny.L++;
                }
                else if (s.Ocena1 == 2.0m)
                {
                    frpOceny.L0++;
                }
            }
            return frpOceny;
        }

        private static FrpOceny PrzeliczFRPOceny(int kursId, Grupa g, frpVM frp, IQueryable<StudentZapisanyDoGrupy> studentZapisanyDoGrupy)
        {
            var frpOceny = new FrpOceny
            {
                NazwaKursu = NazwaKursu_kurs(kursId),
                KodGrupy = g.Kod,
                IdGrupy = g.GrupaId,
                UczestniczacychWEgzaminie =
                    frp.Zaliczenie ? 0 : studentZapisanyDoGrupy.Count(e => e.Ocena1 != null || e.Ocena2 != null),
                ZapisanychNaKurs = studentZapisanyDoGrupy.Count(),
                NieuczestniczacychWEgzaminie =
                    frp.Zaliczenie
                        ? 0
                        : studentZapisanyDoGrupy.Count() -
                          studentZapisanyDoGrupy.Count(e => e.Ocena1 != null || e.Ocena2 != null),
                L = 0,
                L0 = 0,
                L1 = 0,
                L2 = 0,
                L3 = 0,
                L4 = 0,
                L5 = 0,
                L6 = 0
            };
            foreach (var s in studentZapisanyDoGrupy)
            {
                if (s.Ocena1 == 5.5m || s.Ocena2 == 5.5m)
                {
                    frpOceny.L6++;
                    frpOceny.L++;
                }
                else if (s.Ocena1 == 5.0m || s.Ocena2 == 5.0m)
                {
                    frpOceny.L5++;
                    frpOceny.L++;
                }
                else if (s.Ocena1 == 4.5m || s.Ocena2 == 4.5m)
                {
                    frpOceny.L4++;
                    frpOceny.L++;
                }
                else if (s.Ocena1 == 4.0m || s.Ocena2 == 4.0m)
                {
                    frpOceny.L3++;
                    frpOceny.L++;
                }
                else if (s.Ocena1 == 3.5m || s.Ocena2 == 3.5m)
                {
                    frpOceny.L2++;
                    frpOceny.L++;
                }
                else if (s.Ocena1 == 3.0m || s.Ocena2 == 3.0m)
                {
                    frpOceny.L1++;
                    frpOceny.L++;
                }
                else if (s.Ocena1 == 2.0m || s.Ocena2 == 2.0m)
                {
                    frpOceny.L0++;
                }
            }
            return frpOceny;
        }

        public static void PrzekazFRP(int dydaktykId, int kursId)
        {
            using (var db = new ZpiDbCtx())
            {
                foreach (FormaPrzedmiotu fp in db.KartaPrzedmiotu.First(e => e.KartaPrzedmiotuId==kursId).FormaPrzedmiotu)
                {
                    foreach (FRP frp in fp.FRP.Where(e=>e.DydaktykykId==dydaktykId && e.Semestr==DashboardHelpers.Semestr() && e.Rok == DashboardHelpers.Rok()))
                    {
                        frp.DataZatwierdzenia = DateTime.Now;
                    }
                }
                db.SaveChanges();
            }
        }

        public static void EdytujFRP(frpVM raportFRP)
        {
            using (var db = new ZpiDbCtx())
            {
                var frp =
                    db.FRP.First(
                        e =>
                            e.FormaPrzedmiotuId == raportFRP.frp.FormaPrzedmiotuId && e.Rok == raportFRP.frp.Rok &&
                            e.Semestr == raportFRP.frp.Semestr &&
                            e.StudiaNaKierunkuId == raportFRP.frp.StudiaNaKierunkuId);
                frp.Termin1 = raportFRP.frp.Termin1;
                frp.Termin2 = raportFRP.frp.Termin2;
                frp.DataModyfikacji = DateTime.Now;
                frp.Sekcja3 = raportFRP.frp.Sekcja3;
                frp.Sekcja4 = raportFRP.frp.Sekcja4;
                frp.Sekcja5 = raportFRP.frp.Sekcja5;
                db.SaveChanges();
            }
        }

        public static bool CzyWszystkieFRPPrzekazane(int kursId)
        {
            using (var db = new ZpiDbCtx())
            {
                foreach (var fp in db.KartaPrzedmiotu.First(e=>e.KartaPrzedmiotuId==kursId).FormaPrzedmiotu)
                {
                    foreach (var g in fp.Grupa)
                    {
                        if (!PrzekazanyFRP(kursId, g.DydaktykId))
                            return false;
                    }
                }
                return true;
            }
        }

        #endregion raport FRP

        #region raport SROP
        public static bool CzyOdpowiedzialnyZaSROP(int dydaktykId, int idKursu)
        {
            using (var db = new ZpiDbCtx())
            {
                return db.KartaPrzedmiotu.First(e => e.KartaPrzedmiotuId==idKursu).PracownikId == dydaktykId;
            }
        }

        public static bool IstniejeSROP(int kursId)
        {
            using (var db = new ZpiDbCtx())
            {
                return db.KartaPrzedmiotu.First(e => e.KartaPrzedmiotuId == kursId).SROP.Any(
                    e => e.Rok == DashboardHelpers.Rok() && e.Semestr == DashboardHelpers.Semestr());
            }
        }

        public static bool PrzekazanySROP(int kursId)
        {
            using (var db = new ZpiDbCtx())
            {
                var karta = db.KartaPrzedmiotu.FirstOrDefault(e => e.KartaPrzedmiotuId == kursId);
                if (karta == null)
                    return false;
                if (!karta.SROP.Any(e => e.Rok == DashboardHelpers.Rok() && e.Semestr == DashboardHelpers.Semestr()))
                    return false;

                return
                    karta.SROP.First(e => e.Rok == DashboardHelpers.Rok() && e.Semestr == DashboardHelpers.Semestr())
                        .DataZatwierdzenia != null;
            }
        }

        public static SROP PobierzRaportSROP(int dydaktykId, int kursId)
        {
            using (var db = new ZpiDbCtx())
            {
                var studiaNaKierunku =
                    db.KartaPrzedmiotu.First(e => e.KartaPrzedmiotuId == kursId)
                        .FormaPrzedmiotu.FirstOrDefault()
                        .Grupa.FirstOrDefault()
                        .StudiaNaKierunkuId;
                if (!IstniejeSROP(kursId))
                {
                    db.KartaPrzedmiotu.First(e=> e.KartaPrzedmiotuId==kursId).SROP.Add(new SROP
                    {
                        DataUtworzenia = DateTime.Now,
                        DataModyfikacji = DateTime.Now,
                        Semestr = DashboardHelpers.Semestr(),
                        Rok = DashboardHelpers.Rok(),
                        KartaPrzedmiotuId = kursId,
                        PracownikId = dydaktykId,
                        Sekcja1 = "",
                        Sekcja2 = "",
                        Sekcja3 = "",
                        Sekcja4 = "",
                        Sekcja5 = "",
                        StudiaNaKierunkuId = studiaNaKierunku,
                        StatusRaportuId = 1
                    });
                    db.SaveChanges();
                }
                return db.KartaPrzedmiotu.First(e => e.KartaPrzedmiotuId == kursId).SROP.First(e=>e.Rok==DashboardHelpers.Rok() && e.Semestr==DashboardHelpers.Semestr());
            }
        }

        public static void PrzekazSROP(int dydaktykId, int kursId)
        {
            using (var db = new ZpiDbCtx())
            {
                db.KartaPrzedmiotu.First(e => e.KartaPrzedmiotuId == kursId)
                    .SROP.First(e => e.Rok == DashboardHelpers.Rok() & e.Semestr == DashboardHelpers.Semestr()).DataZatwierdzenia = DateTime.Now;
            }
        }

        public static void EdytujSROP(SROP srop)
        {
            using (var db = new ZpiDbCtx())
            {
                var edytowanySROP =
                    db.SROP.First(
                        e =>
                            e.KartaPrzedmiotuId == srop.KartaPrzedmiotuId && e.Rok == srop.Rok &&
                            e.Semestr == srop.Semestr && e.StudiaNaKierunkuId == srop.StudiaNaKierunkuId);
                edytowanySROP.DataModyfikacji = DateTime.Now;
                edytowanySROP.Sekcja1 = srop.Sekcja1;
                edytowanySROP.Sekcja2 = srop.Sekcja2;
                edytowanySROP.Sekcja3 = srop.Sekcja3;
                edytowanySROP.Sekcja4 = srop.Sekcja4;
                edytowanySROP.Sekcja5 = srop.Sekcja5;
                db.SaveChanges();
            }
        }

        #endregion raport SROP

        public static string NazwaKursu_kurs(int idKursu)
        {
            using (var db = new ZpiDbCtx())
            {
                return db.KartaPrzedmiotu.First(e => e.KartaPrzedmiotuId == idKursu).NazwaPl;
            }
        }

        public static string NazwaKursu_grupa(int idGrupy)
        {
            using (var db = new ZpiDbCtx())
            {
                return db.Grupa.First(e => e.GrupaId == idGrupy).FormaPrzedmiotu.KartaPrzedmiotu.NazwaPl;
            }
        }

        public static RaportyVM WidokRaportow(int kursId, int dydaktykId)
        {
            RaportyVM raportVM = new RaportyVM
            {
                CzyOdpowiedzialnyZaSROP = CzyOdpowiedzialnyZaSROP(dydaktykId, kursId),
                KursId = kursId,
                NazwaKursu = NazwaKursu_kurs(kursId),
                IstniejeSROP = IstniejeSROP(kursId),
                PrzekazanySROP = PrzekazanySROP(kursId),
                PrzekazaneWszystkieFRP = CzyWszystkieFRPPrzekazane(kursId),
                ListaFormPrzedmiotu = new List<RaportyVM_formyPrzedmiotu>()
            };
            using (var db = new ZpiDbCtx())
            {
                foreach (FormaPrzedmiotu fp in db.FormaPrzedmiotu.Where(e=>e.KartaPrzedmiotuId==kursId))
                {
                    if (fp.Grupa.Any(e => e.DydaktykId == dydaktykId))
                    {
                        TerminZajec termin = fp.Grupa.First().TerminZajec.First();
                        raportVM.ListaFormPrzedmiotu.Add(new RaportyVM_formyPrzedmiotu
                        {
                            IdFormyPrzedmiotu = fp.FormaPrzedmiotuId,
                            IstniejeFRP = IstniejeFRP(kursId, dydaktykId, fp.FormaPrzedmiotuId),
                            NazwaFormyPrzedmiotu = fp.TypPrzedmiotu.Wartosc,
                            DzienTygodnia = CultureInfo.CurrentCulture.DateTimeFormat.DayNames[(int)termin.Data.DayOfWeek],
                            Godzina = DashboardHelpers.GodzinyZajec(termin.GodzinaOd, termin.GodzinaDo),
                            KodGrupy = fp.Grupa.First().Kod,
                            PrzekazanyFRP = PrzekazanyFRP(kursId, dydaktykId)
                        });
                    }
                }
            }
            return raportVM;
        }

        public static List<KartaPrzedmiotu> KursyDydaktyka(int dydaktykId)
        {
            var kartyPrzedmiotu = new List<KartaPrzedmiotu>();
            using (var db = new ZpiDbCtx())
            {
                var grupy = db.Dydaktyk.First(e => e.DydaktykId == dydaktykId).Grupa.ToList();

                kartyPrzedmiotu.AddRange(grupy.Select(g => g.FormaPrzedmiotu.KartaPrzedmiotu));
            }
            return kartyPrzedmiotu.Distinct().ToList();
        }

        public static List<ListaRaportowVM> ListaRaportow(int dydaktykId)
        {
            var lista = new List<ListaRaportowVM>();
            var listaKursow = KursyDydaktyka(dydaktykId);

            using (var db = new ZpiDbCtx())
            {
                foreach (KartaPrzedmiotu kurs in listaKursow)
                {
                    var k = db.KartaPrzedmiotu.First(e => e.KartaPrzedmiotuId == kurs.KartaPrzedmiotuId);
                    var grupy = new List<ListaRaportow_grupy>();

                    foreach (var f in k.FormaPrzedmiotu)
                    {
                        foreach (Grupa g in f.Grupa.Where(e => e.DydaktykId == dydaktykId))
                        {
                            var termin = g.TerminZajec.First();
                            grupy.Add(new ListaRaportow_grupy
                            {
                                DzienTygodnia = (int)termin.Data.DayOfWeek,
                                Godzina = DashboardHelpers.GodzinyZajec(termin.GodzinaOd,termin.GodzinaDo),
                                IstniejeFRP = IstniejeFRP(k.KartaPrzedmiotuId, dydaktykId, g.FormaPrzedmiotuId),
                                KodGrupy = g.Kod,
                                PrzekazaneFRP = PrzekazanyFRP(k.KartaPrzedmiotuId, dydaktykId),
                                TypPrzedmiotu = g.FormaPrzedmiotu.TypPrzedmiotu.Wartosc
                            });
                        }
                    }

                    var lr = new ListaRaportowVM
                    {
                        IdKursu = k.KartaPrzedmiotuId,
                        IstniejeSROP = IstniejeSROP(k.KartaPrzedmiotuId),
                        OdpowiedzialnyZaSROP = CzyOdpowiedzialnyZaSROP(dydaktykId, k.KartaPrzedmiotuId),
                        PrzekazanySROP = PrzekazanySROP(k.KartaPrzedmiotuId),
                        NazwaKursu = NazwaKursu_kurs(k.KartaPrzedmiotuId),
                        ListaGrup = grupy
                    };
                    lista.Add(lr);
                }
            }
            return lista;
        }
    }
}