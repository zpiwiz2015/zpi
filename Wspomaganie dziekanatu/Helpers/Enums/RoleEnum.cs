﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wspomaganie_dziekanatu.Helpers.Enums
{
    public enum RoleEnum
    {
        Administrator = 1,
        Dydaktyk = 3
    }

    public static class Role
    {
        public static string GetRola(this RoleEnum rola)
        {
            switch (rola)
            {
                case RoleEnum.Administrator:
                    return "Administrator";
                //case RoleEnum.Dydaktyk:
                default:
                    return "Dydaktyk";
            }
        }
    }
}