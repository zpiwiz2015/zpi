﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wspomaganie_dziekanatu.Helpers.Enums
{
    public enum DzienTygodniaEnum
    {
        poniedzialek = 1,
        wtorek,
        sroda,
        czwartek,
        piatek,
        sobota,
        niedziela
    }

    public static class DzienTygodnia
    {
        public static string GetString(this DzienTygodniaEnum dzienTygodnia)
        {
            switch (dzienTygodnia)
            {
                case DzienTygodniaEnum.poniedzialek:
                    return "Poniedziałek";
                case DzienTygodniaEnum.wtorek:
                    return "Wtorek";
                case DzienTygodniaEnum.sroda:
                    return "Środa";
                case DzienTygodniaEnum.czwartek:
                    return "Czwartek";
                case DzienTygodniaEnum.piatek:
                    return "Piątek";
                case DzienTygodniaEnum.sobota:
                    return "Sobota";
                default:
                    return "Niedziela";
            }
        }

        public static DzienTygodniaEnum GetEnum(string dzienTygodnia)
        {
            dzienTygodnia = dzienTygodnia.ToLower().Substring(1, 3);
            switch (dzienTygodnia)
            {
                case "pon":
                    return DzienTygodniaEnum.poniedzialek;
                case "wto":
                    return DzienTygodniaEnum.wtorek;
                case "sro":
                case "śro":
                    return DzienTygodniaEnum.sroda;
                case "czw":
                    return DzienTygodniaEnum.czwartek;
                case "pia":
                case "pią":
                    return DzienTygodniaEnum.piatek;
                case "sob":
                    return DzienTygodniaEnum.sobota;
                default:
                    return DzienTygodniaEnum.niedziela;
            }
        }
    }
}