﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Wspomaganie_dziekanatu.Helpers
{
    public class RolesHelper
    {
        public bool isRoleProper(String role)
        {
            if (role == "Student" || role == "Dydaktyk")
            {
                return true;
            }
            return false;
        }
    }
}