﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Web;
using Wspomaganie_dziekanatu.Helpers.Enums;
using Wspomaganie_dziekanatu.Models;
using Wspomaganie_dziekanatu.Models.VM;

namespace Wspomaganie_dziekanatu.Helpers
{
    public static class DashboardHelpers
    {
        public static string GodzinyZajec(TimeSpan godzinaOd, TimeSpan godzinaDo)
        {
            return String.Format("{0,2}:{1,2} - {2,2}:{3,2}", godzinaOd.Hours, godzinaOd.Minutes.ToString("D2"), godzinaDo.Hours,
                godzinaDo.Minutes.ToString("D2"));
        }

        public static ListaGrupVM Grupa(Grupa grupa)
        {
            var zajecie = grupa.TerminZajec.First();
            return new ListaGrupVM
            {
                DzienTygodnia = CultureInfo.CurrentCulture.DateTimeFormat.DayNames[(int)zajecie.Data.DayOfWeek],
                Godzina = DashboardHelpers.GodzinyZajec(zajecie.GodzinaOd, zajecie.GodzinaDo),
                IdGrupy = grupa.GrupaId,
                KodGrupy = grupa.Kod,
                IdKursu = grupa.FormaPrzedmiotu.KartaPrzedmiotu.KartaPrzedmiotuId,
                NazwaKursu = grupa.FormaPrzedmiotu.KartaPrzedmiotu.NazwaPl,
                TypPrzedmiotu = grupa.FormaPrzedmiotu.TypPrzedmiotu.Wartosc,
                FormaPrzedmiotuId = grupa.FormaPrzedmiotuId
            };
        }

        public static List<D_Raporty> Raporty(int idDydaktyka)
        {
            var raportyLista = new List<D_Raporty>();
            using (var db = new ZpiDbCtx())
            {
                var grupy = db.Grupa.Where(e => e.DydaktykId == idDydaktyka);
                foreach (var grupa in grupy)
                {
                    string nazwaKursu = grupa.FormaPrzedmiotu.KartaPrzedmiotu.NazwaPl;
                    //czy nie istnieje raport lub nie został zatwierdzony
                    if (!grupa.FormaPrzedmiotu.FRP.Any() || grupa.FormaPrzedmiotu.FRP.First(e => e.FormaPrzedmiotuId == grupa.FormaPrzedmiotuId)
                                .DataZatwierdzenia == null)
                    {


                        if (!raportyLista.Any(e => e.NazwaKursu == nazwaKursu))
                            raportyLista.Add(new D_Raporty
                            {
                                IdKursu = grupa.FormaPrzedmiotu.KartaPrzedmiotuId,
                                KodKursu = grupa.Kod,
                                NazwaKursu = nazwaKursu
                            });
                    }
                    //czy istnieje SROP dla KartyPrzedmiotu w aktualnym roku i semestrze lub SROP nie został zatwierdzony
                    if (
                        !grupa.FormaPrzedmiotu.KartaPrzedmiotu.SROP.Any(e => e.Semestr == Semestr() && e.Rok == Rok()) ||
                        grupa.FormaPrzedmiotu.KartaPrzedmiotu.SROP.First(e => e.Semestr == Semestr() && e.Rok == Rok())
                            .DataZatwierdzenia == null)
                    {
                        if (!raportyLista.Any(e => e.NazwaKursu == nazwaKursu))
                            raportyLista.Add(new D_Raporty
                            {
                                IdKursu = grupa.FormaPrzedmiotu.KartaPrzedmiotuId,
                                KodKursu = grupa.Kod,
                                NazwaKursu = nazwaKursu
                            });
                    }
                }
            }
            return raportyLista;
        }

        public static int Semestr()
        {
            int aktualnyMiesiac = DateTime.Now.Month;
            return aktualnyMiesiac < 3 && aktualnyMiesiac >= 10 ? 1 : 2;
        }

        public static int Rok()
        {
            int aktualnyMiesiac = DateTime.Now.Month;
            int aktualnyRok = DateTime.Now.Year;
            return aktualnyMiesiac < 10 ? aktualnyRok - 1 : aktualnyRok;
        }

        public static string CyklKsztalcenia()
        {
            int rok = DateTime.Now.Year;
            if (DateTime.Now.Month > 9)
                return string.Format("{0}/{1}",rok,rok+1);
            return string.Format("{0}/{1}", rok-1, rok);
        }
    }
}